import ast


def parse_and_generate_code(init_file_path: str) -> str:
    """
    Parse an __init__.py file and generate import statements and __init__ function lines.

    Args:
        init_file_path: Path to the __init__.py file.

    Returns:
        A tuple containing two strings:
        - The import statements
        - The lines to be added to the __init__ function of the generated class.
    """
    with open(init_file_path, "r") as f:
        # Read the file contents
        file_contents = f.read()

    # Parse the Python code into an AST
    tree = ast.parse(file_contents)

    # Initialize result strings
    import_str = ""
    init_str = ""

    # Loop through the AST nodes to find imported API classes
    for node in ast.walk(tree):
        # Filter out ImportFrom nodes
        if isinstance(node, ast.ImportFrom):
            for name in node.names:
                # Create import statements
                import_str += f"from {node.module} import {name.name}\n"
                # Create the line for the __init__ function
                init_str += f"self.{name.name.lower()[:-3]} = {name.name}(api_client)\n"

    return import_str, init_str


# Provide the path to your __init__.py file here
init_file_path = "nomad-openapi/openapi_client/api/__init__.py"

# Generate the import statements and __init__ function lines
import_statements, init_function_lines = parse_and_generate_code(init_file_path)

print("Import Statements:")
print(import_statements)

print("Init Function Lines:")
print(init_function_lines)
