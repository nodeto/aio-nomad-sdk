from urllib.parse import urljoin

from nomad_generated.api.acl_api import ACLApi
from nomad_generated.api.allocations_api import AllocationsApi
from nomad_generated.api.deployments_api import DeploymentsApi
from nomad_generated.api.enterprise_api import EnterpriseApi
from nomad_generated.api.evaluations_api import EvaluationsApi
from nomad_generated.api.jobs_api import JobsApi
from nomad_generated.api.metrics_api import MetricsApi
from nomad_generated.api.namespaces_api import NamespacesApi
from nomad_generated.api.nodes_api import NodesApi
from nomad_generated.api.operator_api import OperatorApi
from nomad_generated.api.plugins_api import PluginsApi
from nomad_generated.api.regions_api import RegionsApi
from nomad_generated.api.scaling_api import ScalingApi
from nomad_generated.api.search_api import SearchApi
from nomad_generated.api.status_api import StatusApi
from nomad_generated.api.system_api import SystemApi
from nomad_generated.api.variables_api import VariablesApi
from nomad_generated.api.volumes_api import VolumesApi
from nomad_generated.api_client import ApiClient
from nomad_generated.configuration import Configuration


class NomadApi:
    def __init__(
        self,
        url: str | None = None,
        token: str | None = None,
        client_cert: str | None = None,
        client_key: str | None = None,
        ca_cert: str | None = None,
        client_side_validation: bool = False,
    ):
        if url is None:
            url = "http://localhost:4646"
        configuration = Configuration(urljoin(url, "/v1"))
        if token is not None:
            configuration.api_key["X-Nomad-Token"] = token

        if client_cert is not None:
            configuration.cert_file = client_cert  # type: ignore
        if client_key is not None:
            configuration.key_file = client_key  # type: ignore
        if ca_cert is not None:
            configuration.ssl_ca_cert = ca_cert

        configuration.client_side_validation = client_side_validation

        self._api_client = ApiClient(configuration=configuration)

        self.acl = ACLApi(self._api_client)
        self.allocations = AllocationsApi(self._api_client)
        self.deployments = DeploymentsApi(self._api_client)
        self.enterprise = EnterpriseApi(self._api_client)
        self.evaluations = EvaluationsApi(self._api_client)
        self.jobs = JobsApi(self._api_client)
        self.metrics = MetricsApi(self._api_client)
        self.namespaces = NamespacesApi(self._api_client)
        self.nodes = NodesApi(self._api_client)
        self.operator = OperatorApi(self._api_client)
        self.plugins = PluginsApi(self._api_client)
        self.regions = RegionsApi(self._api_client)
        self.scaling = ScalingApi(self._api_client)
        self.search = SearchApi(self._api_client)
        self.status = StatusApi(self._api_client)
        self.system = SystemApi(self._api_client)
        self.variables = VariablesApi(self._api_client)
        self.volumes = VolumesApi(self._api_client)

    async def __aenter__(self):
        return self

    async def __aexit__(self, exc_type, exc_value, traceback):
        await self._api_client.close()

    async def close(self):
        await self._api_client.close()
