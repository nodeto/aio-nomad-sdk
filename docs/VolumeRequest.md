# VolumeRequest


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**access_mode** | **str** |  | [optional] 
**attachment_mode** | **str** |  | [optional] 
**mount_options** | [**CSIMountOptions**](CSIMountOptions.md) |  | [optional] 
**name** | **str** |  | [optional] 
**per_alloc** | **bool** |  | [optional] 
**read_only** | **bool** |  | [optional] 
**source** | **str** |  | [optional] 
**type** | **str** |  | [optional] 

## Example

```python
from nomad_generated.models.volume_request import VolumeRequest

# TODO update the JSON string below
json = "{}"
# create an instance of VolumeRequest from a JSON string
volume_request_instance = VolumeRequest.from_json(json)
# print the JSON string representation of the object
print VolumeRequest.to_json()

# convert the object into a dict
volume_request_dict = volume_request_instance.to_dict()
# create an instance of VolumeRequest from a dict
volume_request_form_dict = volume_request.from_dict(volume_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


