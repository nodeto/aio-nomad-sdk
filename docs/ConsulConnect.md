# ConsulConnect


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**gateway** | [**ConsulGateway**](ConsulGateway.md) |  | [optional] 
**native** | **bool** |  | [optional] 
**sidecar_service** | [**ConsulSidecarService**](ConsulSidecarService.md) |  | [optional] 
**sidecar_task** | [**SidecarTask**](SidecarTask.md) |  | [optional] 

## Example

```python
from nomad_generated.models.consul_connect import ConsulConnect

# TODO update the JSON string below
json = "{}"
# create an instance of ConsulConnect from a JSON string
consul_connect_instance = ConsulConnect.from_json(json)
# print the JSON string representation of the object
print ConsulConnect.to_json()

# convert the object into a dict
consul_connect_dict = consul_connect_instance.to_dict()
# create an instance of ConsulConnect from a dict
consul_connect_form_dict = consul_connect.from_dict(consul_connect_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


