# QuotaSpec


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**create_index** | **int** |  | [optional] 
**description** | **str** |  | [optional] 
**limits** | [**List[QuotaLimit]**](QuotaLimit.md) |  | [optional] 
**modify_index** | **int** |  | [optional] 
**name** | **str** |  | [optional] 

## Example

```python
from nomad_generated.models.quota_spec import QuotaSpec

# TODO update the JSON string below
json = "{}"
# create an instance of QuotaSpec from a JSON string
quota_spec_instance = QuotaSpec.from_json(json)
# print the JSON string representation of the object
print QuotaSpec.to_json()

# convert the object into a dict
quota_spec_dict = quota_spec_instance.to_dict()
# create an instance of QuotaSpec from a dict
quota_spec_form_dict = quota_spec.from_dict(quota_spec_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


