# CSIControllerInfo


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**supports_attach_detach** | **bool** |  | [optional] 
**supports_clone** | **bool** |  | [optional] 
**supports_condition** | **bool** |  | [optional] 
**supports_create_delete** | **bool** |  | [optional] 
**supports_create_delete_snapshot** | **bool** |  | [optional] 
**supports_expand** | **bool** |  | [optional] 
**supports_get** | **bool** |  | [optional] 
**supports_get_capacity** | **bool** |  | [optional] 
**supports_list_snapshots** | **bool** |  | [optional] 
**supports_list_volumes** | **bool** |  | [optional] 
**supports_list_volumes_attached_nodes** | **bool** |  | [optional] 
**supports_read_only_attach** | **bool** |  | [optional] 

## Example

```python
from nomad_generated.models.csi_controller_info import CSIControllerInfo

# TODO update the JSON string below
json = "{}"
# create an instance of CSIControllerInfo from a JSON string
csi_controller_info_instance = CSIControllerInfo.from_json(json)
# print the JSON string representation of the object
print CSIControllerInfo.to_json()

# convert the object into a dict
csi_controller_info_dict = csi_controller_info_instance.to_dict()
# create an instance of CSIControllerInfo from a dict
csi_controller_info_form_dict = csi_controller_info.from_dict(csi_controller_info_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


