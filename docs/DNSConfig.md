# DNSConfig


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**options** | **List[str]** |  | [optional] 
**searches** | **List[str]** |  | [optional] 
**servers** | **List[str]** |  | [optional] 

## Example

```python
from nomad_generated.models.dns_config import DNSConfig

# TODO update the JSON string below
json = "{}"
# create an instance of DNSConfig from a JSON string
dns_config_instance = DNSConfig.from_json(json)
# print the JSON string representation of the object
print DNSConfig.to_json()

# convert the object into a dict
dns_config_dict = dns_config_instance.to_dict()
# create an instance of DNSConfig from a dict
dns_config_form_dict = dns_config.from_dict(dns_config_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


