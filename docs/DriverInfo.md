# DriverInfo


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**attributes** | **Dict[str, str]** |  | [optional] 
**detected** | **bool** |  | [optional] 
**health_description** | **str** |  | [optional] 
**healthy** | **bool** |  | [optional] 
**update_time** | **datetime** |  | [optional] 

## Example

```python
from nomad_generated.models.driver_info import DriverInfo

# TODO update the JSON string below
json = "{}"
# create an instance of DriverInfo from a JSON string
driver_info_instance = DriverInfo.from_json(json)
# print the JSON string representation of the object
print DriverInfo.to_json()

# convert the object into a dict
driver_info_dict = driver_info_instance.to_dict()
# create an instance of DriverInfo from a dict
driver_info_form_dict = driver_info.from_dict(driver_info_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


