# JobChildrenSummary


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dead** | **int** |  | [optional] 
**pending** | **int** |  | [optional] 
**running** | **int** |  | [optional] 

## Example

```python
from nomad_generated.models.job_children_summary import JobChildrenSummary

# TODO update the JSON string below
json = "{}"
# create an instance of JobChildrenSummary from a JSON string
job_children_summary_instance = JobChildrenSummary.from_json(json)
# print the JSON string representation of the object
print JobChildrenSummary.to_json()

# convert the object into a dict
job_children_summary_dict = job_children_summary_instance.to_dict()
# create an instance of JobChildrenSummary from a dict
job_children_summary_form_dict = job_children_summary.from_dict(job_children_summary_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


