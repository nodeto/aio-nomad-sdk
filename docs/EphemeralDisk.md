# EphemeralDisk


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**migrate** | **bool** |  | [optional] 
**size_mb** | **int** |  | [optional] 
**sticky** | **bool** |  | [optional] 

## Example

```python
from nomad_generated.models.ephemeral_disk import EphemeralDisk

# TODO update the JSON string below
json = "{}"
# create an instance of EphemeralDisk from a JSON string
ephemeral_disk_instance = EphemeralDisk.from_json(json)
# print the JSON string representation of the object
print EphemeralDisk.to_json()

# convert the object into a dict
ephemeral_disk_dict = ephemeral_disk_instance.to_dict()
# create an instance of EphemeralDisk from a dict
ephemeral_disk_form_dict = ephemeral_disk.from_dict(ephemeral_disk_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


