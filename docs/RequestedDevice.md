# RequestedDevice


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**affinities** | [**List[Affinity]**](Affinity.md) |  | [optional] 
**constraints** | [**List[Constraint]**](Constraint.md) |  | [optional] 
**count** | **int** |  | [optional] 
**name** | **str** |  | [optional] 

## Example

```python
from nomad_generated.models.requested_device import RequestedDevice

# TODO update the JSON string below
json = "{}"
# create an instance of RequestedDevice from a JSON string
requested_device_instance = RequestedDevice.from_json(json)
# print the JSON string representation of the object
print RequestedDevice.to_json()

# convert the object into a dict
requested_device_dict = requested_device_instance.to_dict()
# create an instance of RequestedDevice from a dict
requested_device_form_dict = requested_device.from_dict(requested_device_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


