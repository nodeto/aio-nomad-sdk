# AllocationListStub


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**allocated_resources** | [**AllocatedResources**](AllocatedResources.md) |  | [optional] 
**client_description** | **str** |  | [optional] 
**client_status** | **str** |  | [optional] 
**create_index** | **int** |  | [optional] 
**create_time** | **int** |  | [optional] 
**deployment_status** | [**AllocDeploymentStatus**](AllocDeploymentStatus.md) |  | [optional] 
**desired_description** | **str** |  | [optional] 
**desired_status** | **str** |  | [optional] 
**eval_id** | **str** |  | [optional] 
**followup_eval_id** | **str** |  | [optional] 
**id** | **str** |  | [optional] 
**job_id** | **str** |  | [optional] 
**job_type** | **str** |  | [optional] 
**job_version** | **int** |  | [optional] 
**modify_index** | **int** |  | [optional] 
**modify_time** | **int** |  | [optional] 
**name** | **str** |  | [optional] 
**namespace** | **str** |  | [optional] 
**node_id** | **str** |  | [optional] 
**node_name** | **str** |  | [optional] 
**preempted_allocations** | **List[str]** |  | [optional] 
**preempted_by_allocation** | **str** |  | [optional] 
**reschedule_tracker** | [**RescheduleTracker**](RescheduleTracker.md) |  | [optional] 
**task_group** | **str** |  | [optional] 
**task_states** | [**Dict[str, TaskState]**](TaskState.md) |  | [optional] 

## Example

```python
from nomad_generated.models.allocation_list_stub import AllocationListStub

# TODO update the JSON string below
json = "{}"
# create an instance of AllocationListStub from a JSON string
allocation_list_stub_instance = AllocationListStub.from_json(json)
# print the JSON string representation of the object
print AllocationListStub.to_json()

# convert the object into a dict
allocation_list_stub_dict = allocation_list_stub_instance.to_dict()
# create an instance of AllocationListStub from a dict
allocation_list_stub_form_dict = allocation_list_stub.from_dict(allocation_list_stub_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


