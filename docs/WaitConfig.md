# WaitConfig


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**max** | **int** |  | [optional] 
**min** | **int** |  | [optional] 

## Example

```python
from nomad_generated.models.wait_config import WaitConfig

# TODO update the JSON string below
json = "{}"
# create an instance of WaitConfig from a JSON string
wait_config_instance = WaitConfig.from_json(json)
# print the JSON string representation of the object
print WaitConfig.to_json()

# convert the object into a dict
wait_config_dict = wait_config_instance.to_dict()
# create an instance of WaitConfig from a dict
wait_config_form_dict = wait_config.from_dict(wait_config_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


