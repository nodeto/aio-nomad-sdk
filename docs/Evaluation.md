# Evaluation


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**annotate_plan** | **bool** |  | [optional] 
**blocked_eval** | **str** |  | [optional] 
**class_eligibility** | **Dict[str, bool]** |  | [optional] 
**create_index** | **int** |  | [optional] 
**create_time** | **int** |  | [optional] 
**deployment_id** | **str** |  | [optional] 
**escaped_computed_class** | **bool** |  | [optional] 
**failed_tg_allocs** | [**Dict[str, AllocationMetric]**](AllocationMetric.md) |  | [optional] 
**id** | **str** |  | [optional] 
**job_id** | **str** |  | [optional] 
**job_modify_index** | **int** |  | [optional] 
**modify_index** | **int** |  | [optional] 
**modify_time** | **int** |  | [optional] 
**namespace** | **str** |  | [optional] 
**next_eval** | **str** |  | [optional] 
**node_id** | **str** |  | [optional] 
**node_modify_index** | **int** |  | [optional] 
**previous_eval** | **str** |  | [optional] 
**priority** | **int** |  | [optional] 
**queued_allocations** | **Dict[str, int]** |  | [optional] 
**quota_limit_reached** | **str** |  | [optional] 
**related_evals** | [**List[EvaluationStub]**](EvaluationStub.md) |  | [optional] 
**snapshot_index** | **int** |  | [optional] 
**status** | **str** |  | [optional] 
**status_description** | **str** |  | [optional] 
**triggered_by** | **str** |  | [optional] 
**type** | **str** |  | [optional] 
**wait** | **int** |  | [optional] 
**wait_until** | **datetime** |  | [optional] 

## Example

```python
from nomad_generated.models.evaluation import Evaluation

# TODO update the JSON string below
json = "{}"
# create an instance of Evaluation from a JSON string
evaluation_instance = Evaluation.from_json(json)
# print the JSON string representation of the object
print Evaluation.to_json()

# convert the object into a dict
evaluation_dict = evaluation_instance.to_dict()
# create an instance of Evaluation from a dict
evaluation_form_dict = evaluation.from_dict(evaluation_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


