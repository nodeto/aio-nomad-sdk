# ConsulMeshGateway


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**mode** | **str** |  | [optional] 

## Example

```python
from nomad_generated.models.consul_mesh_gateway import ConsulMeshGateway

# TODO update the JSON string below
json = "{}"
# create an instance of ConsulMeshGateway from a JSON string
consul_mesh_gateway_instance = ConsulMeshGateway.from_json(json)
# print the JSON string representation of the object
print ConsulMeshGateway.to_json()

# convert the object into a dict
consul_mesh_gateway_dict = consul_mesh_gateway_instance.to_dict()
# create an instance of ConsulMeshGateway from a dict
consul_mesh_gateway_form_dict = consul_mesh_gateway.from_dict(consul_mesh_gateway_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


