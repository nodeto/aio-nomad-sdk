# TaskCSIPluginConfig


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**health_timeout** | **int** |  | [optional] 
**id** | **str** |  | [optional] 
**mount_dir** | **str** |  | [optional] 
**stage_publish_base_dir** | **str** |  | [optional] 
**type** | **str** |  | [optional] 

## Example

```python
from nomad_generated.models.task_csi_plugin_config import TaskCSIPluginConfig

# TODO update the JSON string below
json = "{}"
# create an instance of TaskCSIPluginConfig from a JSON string
task_csi_plugin_config_instance = TaskCSIPluginConfig.from_json(json)
# print the JSON string representation of the object
print TaskCSIPluginConfig.to_json()

# convert the object into a dict
task_csi_plugin_config_dict = task_csi_plugin_config_instance.to_dict()
# create an instance of TaskCSIPluginConfig from a dict
task_csi_plugin_config_form_dict = task_csi_plugin_config.from_dict(task_csi_plugin_config_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


