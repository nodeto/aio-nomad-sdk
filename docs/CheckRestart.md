# CheckRestart


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**grace** | **int** |  | [optional] 
**ignore_warnings** | **bool** |  | [optional] 
**limit** | **int** |  | [optional] 

## Example

```python
from nomad_generated.models.check_restart import CheckRestart

# TODO update the JSON string below
json = "{}"
# create an instance of CheckRestart from a JSON string
check_restart_instance = CheckRestart.from_json(json)
# print the JSON string representation of the object
print CheckRestart.to_json()

# convert the object into a dict
check_restart_dict = check_restart_instance.to_dict()
# create an instance of CheckRestart from a dict
check_restart_form_dict = check_restart.from_dict(check_restart_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


