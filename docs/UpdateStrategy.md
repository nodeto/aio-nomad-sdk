# UpdateStrategy


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**auto_promote** | **bool** |  | [optional] 
**auto_revert** | **bool** |  | [optional] 
**canary** | **int** |  | [optional] 
**health_check** | **str** |  | [optional] 
**healthy_deadline** | **int** |  | [optional] 
**max_parallel** | **int** |  | [optional] 
**min_healthy_time** | **int** |  | [optional] 
**progress_deadline** | **int** |  | [optional] 
**stagger** | **int** |  | [optional] 

## Example

```python
from nomad_generated.models.update_strategy import UpdateStrategy

# TODO update the JSON string below
json = "{}"
# create an instance of UpdateStrategy from a JSON string
update_strategy_instance = UpdateStrategy.from_json(json)
# print the JSON string representation of the object
print UpdateStrategy.to_json()

# convert the object into a dict
update_strategy_dict = update_strategy_instance.to_dict()
# create an instance of UpdateStrategy from a dict
update_strategy_form_dict = update_strategy.from_dict(update_strategy_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


