# RaftConfiguration


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**index** | **int** |  | [optional] 
**servers** | [**List[RaftServer]**](RaftServer.md) |  | [optional] 

## Example

```python
from nomad_generated.models.raft_configuration import RaftConfiguration

# TODO update the JSON string below
json = "{}"
# create an instance of RaftConfiguration from a JSON string
raft_configuration_instance = RaftConfiguration.from_json(json)
# print the JSON string representation of the object
print RaftConfiguration.to_json()

# convert the object into a dict
raft_configuration_dict = raft_configuration_instance.to_dict()
# create an instance of RaftConfiguration from a dict
raft_configuration_form_dict = raft_configuration.from_dict(raft_configuration_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


