# OneTimeToken


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**accessor_id** | **str** |  | [optional] 
**create_index** | **int** |  | [optional] 
**expires_at** | **datetime** |  | [optional] 
**modify_index** | **int** |  | [optional] 
**one_time_secret_id** | **str** |  | [optional] 

## Example

```python
from nomad_generated.models.one_time_token import OneTimeToken

# TODO update the JSON string below
json = "{}"
# create an instance of OneTimeToken from a JSON string
one_time_token_instance = OneTimeToken.from_json(json)
# print the JSON string representation of the object
print OneTimeToken.to_json()

# convert the object into a dict
one_time_token_dict = one_time_token_instance.to_dict()
# create an instance of OneTimeToken from a dict
one_time_token_form_dict = one_time_token.from_dict(one_time_token_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


