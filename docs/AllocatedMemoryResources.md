# AllocatedMemoryResources


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**memory_mb** | **int** |  | [optional] 
**memory_max_mb** | **int** |  | [optional] 

## Example

```python
from nomad_generated.models.allocated_memory_resources import AllocatedMemoryResources

# TODO update the JSON string below
json = "{}"
# create an instance of AllocatedMemoryResources from a JSON string
allocated_memory_resources_instance = AllocatedMemoryResources.from_json(json)
# print the JSON string representation of the object
print AllocatedMemoryResources.to_json()

# convert the object into a dict
allocated_memory_resources_dict = allocated_memory_resources_instance.to_dict()
# create an instance of AllocatedMemoryResources from a dict
allocated_memory_resources_form_dict = allocated_memory_resources.from_dict(allocated_memory_resources_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


