# JobValidateResponse


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**driver_config_validated** | **bool** |  | [optional] 
**error** | **str** |  | [optional] 
**validation_errors** | **List[str]** |  | [optional] 
**warnings** | **str** |  | [optional] 

## Example

```python
from nomad_generated.models.job_validate_response import JobValidateResponse

# TODO update the JSON string below
json = "{}"
# create an instance of JobValidateResponse from a JSON string
job_validate_response_instance = JobValidateResponse.from_json(json)
# print the JSON string representation of the object
print JobValidateResponse.to_json()

# convert the object into a dict
job_validate_response_dict = job_validate_response_instance.to_dict()
# create an instance of JobValidateResponse from a dict
job_validate_response_form_dict = job_validate_response.from_dict(job_validate_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


