# Resources


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cpu** | **int** |  | [optional] 
**cores** | **int** |  | [optional] 
**devices** | [**List[RequestedDevice]**](RequestedDevice.md) |  | [optional] 
**disk_mb** | **int** |  | [optional] 
**iops** | **int** |  | [optional] 
**memory_mb** | **int** |  | [optional] 
**memory_max_mb** | **int** |  | [optional] 
**networks** | [**List[NetworkResource]**](NetworkResource.md) |  | [optional] 

## Example

```python
from nomad_generated.models.resources import Resources

# TODO update the JSON string below
json = "{}"
# create an instance of Resources from a JSON string
resources_instance = Resources.from_json(json)
# print the JSON string representation of the object
print Resources.to_json()

# convert the object into a dict
resources_dict = resources_instance.to_dict()
# create an instance of Resources from a dict
resources_form_dict = resources.from_dict(resources_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


