# Consul


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**namespace** | **str** |  | [optional] 

## Example

```python
from nomad_generated.models.consul import Consul

# TODO update the JSON string below
json = "{}"
# create an instance of Consul from a JSON string
consul_instance = Consul.from_json(json)
# print the JSON string representation of the object
print Consul.to_json()

# convert the object into a dict
consul_dict = consul_instance.to_dict()
# create an instance of Consul from a dict
consul_form_dict = consul.from_dict(consul_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


