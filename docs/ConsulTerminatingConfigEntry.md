# ConsulTerminatingConfigEntry


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**services** | [**List[ConsulLinkedService]**](ConsulLinkedService.md) |  | [optional] 

## Example

```python
from nomad_generated.models.consul_terminating_config_entry import ConsulTerminatingConfigEntry

# TODO update the JSON string below
json = "{}"
# create an instance of ConsulTerminatingConfigEntry from a JSON string
consul_terminating_config_entry_instance = ConsulTerminatingConfigEntry.from_json(json)
# print the JSON string representation of the object
print ConsulTerminatingConfigEntry.to_json()

# convert the object into a dict
consul_terminating_config_entry_dict = consul_terminating_config_entry_instance.to_dict()
# create an instance of ConsulTerminatingConfigEntry from a dict
consul_terminating_config_entry_form_dict = consul_terminating_config_entry.from_dict(consul_terminating_config_entry_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


