# DeploymentUnblockRequest


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**deployment_id** | **str** |  | [optional] 
**namespace** | **str** |  | [optional] 
**region** | **str** |  | [optional] 
**secret_id** | **str** |  | [optional] 

## Example

```python
from nomad_generated.models.deployment_unblock_request import DeploymentUnblockRequest

# TODO update the JSON string below
json = "{}"
# create an instance of DeploymentUnblockRequest from a JSON string
deployment_unblock_request_instance = DeploymentUnblockRequest.from_json(json)
# print the JSON string representation of the object
print DeploymentUnblockRequest.to_json()

# convert the object into a dict
deployment_unblock_request_dict = deployment_unblock_request_instance.to_dict()
# create an instance of DeploymentUnblockRequest from a dict
deployment_unblock_request_form_dict = deployment_unblock_request.from_dict(deployment_unblock_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


