# Deployment


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**create_index** | **int** |  | [optional] 
**id** | **str** |  | [optional] 
**is_multiregion** | **bool** |  | [optional] 
**job_create_index** | **int** |  | [optional] 
**job_id** | **str** |  | [optional] 
**job_modify_index** | **int** |  | [optional] 
**job_spec_modify_index** | **int** |  | [optional] 
**job_version** | **int** |  | [optional] 
**modify_index** | **int** |  | [optional] 
**namespace** | **str** |  | [optional] 
**status** | **str** |  | [optional] 
**status_description** | **str** |  | [optional] 
**task_groups** | [**Dict[str, DeploymentState]**](DeploymentState.md) |  | [optional] 

## Example

```python
from nomad_generated.models.deployment import Deployment

# TODO update the JSON string below
json = "{}"
# create an instance of Deployment from a JSON string
deployment_instance = Deployment.from_json(json)
# print the JSON string representation of the object
print Deployment.to_json()

# convert the object into a dict
deployment_dict = deployment_instance.to_dict()
# create an instance of Deployment from a dict
deployment_form_dict = deployment.from_dict(deployment_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


