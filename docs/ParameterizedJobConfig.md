# ParameterizedJobConfig


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**meta_optional** | **List[str]** |  | [optional] 
**meta_required** | **List[str]** |  | [optional] 
**payload** | **str** |  | [optional] 

## Example

```python
from nomad_generated.models.parameterized_job_config import ParameterizedJobConfig

# TODO update the JSON string below
json = "{}"
# create an instance of ParameterizedJobConfig from a JSON string
parameterized_job_config_instance = ParameterizedJobConfig.from_json(json)
# print the JSON string representation of the object
print ParameterizedJobConfig.to_json()

# convert the object into a dict
parameterized_job_config_dict = parameterized_job_config_instance.to_dict()
# create an instance of ParameterizedJobConfig from a dict
parameterized_job_config_form_dict = parameterized_job_config.from_dict(parameterized_job_config_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


