# NodeScoreMeta


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**node_id** | **str** |  | [optional] 
**norm_score** | **float** |  | [optional] 
**scores** | **Dict[str, float]** |  | [optional] 

## Example

```python
from nomad_generated.models.node_score_meta import NodeScoreMeta

# TODO update the JSON string below
json = "{}"
# create an instance of NodeScoreMeta from a JSON string
node_score_meta_instance = NodeScoreMeta.from_json(json)
# print the JSON string representation of the object
print NodeScoreMeta.to_json()

# convert the object into a dict
node_score_meta_dict = node_score_meta_instance.to_dict()
# create an instance of NodeScoreMeta from a dict
node_score_meta_form_dict = node_score_meta.from_dict(node_score_meta_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


