# ConsulUpstream


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**datacenter** | **str** |  | [optional] 
**destination_name** | **str** |  | [optional] 
**destination_namespace** | **str** |  | [optional] 
**local_bind_address** | **str** |  | [optional] 
**local_bind_port** | **int** |  | [optional] 
**mesh_gateway** | [**ConsulMeshGateway**](ConsulMeshGateway.md) |  | [optional] 

## Example

```python
from nomad_generated.models.consul_upstream import ConsulUpstream

# TODO update the JSON string below
json = "{}"
# create an instance of ConsulUpstream from a JSON string
consul_upstream_instance = ConsulUpstream.from_json(json)
# print the JSON string representation of the object
print ConsulUpstream.to_json()

# convert the object into a dict
consul_upstream_dict = consul_upstream_instance.to_dict()
# create an instance of ConsulUpstream from a dict
consul_upstream_form_dict = consul_upstream.from_dict(consul_upstream_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


