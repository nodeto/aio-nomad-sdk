# JobACL


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**group** | **str** |  | [optional] 
**job_id** | **str** |  | [optional] 
**namespace** | **str** |  | [optional] 
**task** | **str** |  | [optional] 

## Example

```python
from nomad_generated.models.job_acl import JobACL

# TODO update the JSON string below
json = "{}"
# create an instance of JobACL from a JSON string
job_acl_instance = JobACL.from_json(json)
# print the JSON string representation of the object
print JobACL.to_json()

# convert the object into a dict
job_acl_dict = job_acl_instance.to_dict()
# create an instance of JobACL from a dict
job_acl_form_dict = job_acl.from_dict(job_acl_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


