# SidecarTask


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**config** | **Dict[str, object]** |  | [optional] 
**driver** | **str** |  | [optional] 
**env** | **Dict[str, str]** |  | [optional] 
**kill_signal** | **str** |  | [optional] 
**kill_timeout** | **int** |  | [optional] 
**log_config** | [**LogConfig**](LogConfig.md) |  | [optional] 
**meta** | **Dict[str, str]** |  | [optional] 
**name** | **str** |  | [optional] 
**resources** | [**Resources**](Resources.md) |  | [optional] 
**shutdown_delay** | **int** |  | [optional] 
**user** | **str** |  | [optional] 

## Example

```python
from nomad_generated.models.sidecar_task import SidecarTask

# TODO update the JSON string below
json = "{}"
# create an instance of SidecarTask from a JSON string
sidecar_task_instance = SidecarTask.from_json(json)
# print the JSON string representation of the object
print SidecarTask.to_json()

# convert the object into a dict
sidecar_task_dict = sidecar_task_instance.to_dict()
# create an instance of SidecarTask from a dict
sidecar_task_form_dict = sidecar_task.from_dict(sidecar_task_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


