# NodeUpdateEligibilityRequest


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**eligibility** | **str** |  | [optional] 
**node_id** | **str** |  | [optional] 

## Example

```python
from nomad_generated.models.node_update_eligibility_request import NodeUpdateEligibilityRequest

# TODO update the JSON string below
json = "{}"
# create an instance of NodeUpdateEligibilityRequest from a JSON string
node_update_eligibility_request_instance = NodeUpdateEligibilityRequest.from_json(json)
# print the JSON string representation of the object
print NodeUpdateEligibilityRequest.to_json()

# convert the object into a dict
node_update_eligibility_request_dict = node_update_eligibility_request_instance.to_dict()
# create an instance of NodeUpdateEligibilityRequest from a dict
node_update_eligibility_request_form_dict = node_update_eligibility_request.from_dict(node_update_eligibility_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


