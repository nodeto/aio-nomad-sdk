# ServiceRegistration


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**address** | **str** |  | [optional] 
**alloc_id** | **str** |  | [optional] 
**create_index** | **int** |  | [optional] 
**datacenter** | **str** |  | [optional] 
**id** | **str** |  | [optional] 
**job_id** | **str** |  | [optional] 
**modify_index** | **int** |  | [optional] 
**namespace** | **str** |  | [optional] 
**node_id** | **str** |  | [optional] 
**port** | **int** |  | [optional] 
**service_name** | **str** |  | [optional] 
**tags** | **List[str]** |  | [optional] 

## Example

```python
from nomad_generated.models.service_registration import ServiceRegistration

# TODO update the JSON string below
json = "{}"
# create an instance of ServiceRegistration from a JSON string
service_registration_instance = ServiceRegistration.from_json(json)
# print the JSON string representation of the object
print ServiceRegistration.to_json()

# convert the object into a dict
service_registration_dict = service_registration_instance.to_dict()
# create an instance of ServiceRegistration from a dict
service_registration_form_dict = service_registration.from_dict(service_registration_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


