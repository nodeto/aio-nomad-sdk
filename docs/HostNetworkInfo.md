# HostNetworkInfo


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cidr** | **str** |  | [optional] 
**interface** | **str** |  | [optional] 
**name** | **str** |  | [optional] 
**reserved_ports** | **str** |  | [optional] 

## Example

```python
from nomad_generated.models.host_network_info import HostNetworkInfo

# TODO update the JSON string below
json = "{}"
# create an instance of HostNetworkInfo from a JSON string
host_network_info_instance = HostNetworkInfo.from_json(json)
# print the JSON string representation of the object
print HostNetworkInfo.to_json()

# convert the object into a dict
host_network_info_dict = host_network_info_instance.to_dict()
# create an instance of HostNetworkInfo from a dict
host_network_info_form_dict = host_network_info.from_dict(host_network_info_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


