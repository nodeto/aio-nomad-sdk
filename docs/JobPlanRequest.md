# JobPlanRequest


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**diff** | **bool** |  | [optional] 
**job** | [**Job**](Job.md) |  | [optional] 
**namespace** | **str** |  | [optional] 
**policy_override** | **bool** |  | [optional] 
**region** | **str** |  | [optional] 
**secret_id** | **str** |  | [optional] 

## Example

```python
from nomad_generated.models.job_plan_request import JobPlanRequest

# TODO update the JSON string below
json = "{}"
# create an instance of JobPlanRequest from a JSON string
job_plan_request_instance = JobPlanRequest.from_json(json)
# print the JSON string representation of the object
print JobPlanRequest.to_json()

# convert the object into a dict
job_plan_request_dict = job_plan_request_instance.to_dict()
# create an instance of JobPlanRequest from a dict
job_plan_request_form_dict = job_plan_request.from_dict(job_plan_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


