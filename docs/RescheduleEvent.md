# RescheduleEvent


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**prev_alloc_id** | **str** |  | [optional] 
**prev_node_id** | **str** |  | [optional] 
**reschedule_time** | **int** |  | [optional] 

## Example

```python
from nomad_generated.models.reschedule_event import RescheduleEvent

# TODO update the JSON string below
json = "{}"
# create an instance of RescheduleEvent from a JSON string
reschedule_event_instance = RescheduleEvent.from_json(json)
# print the JSON string representation of the object
print RescheduleEvent.to_json()

# convert the object into a dict
reschedule_event_dict = reschedule_event_instance.to_dict()
# create an instance of RescheduleEvent from a dict
reschedule_event_form_dict = reschedule_event.from_dict(reschedule_event_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


