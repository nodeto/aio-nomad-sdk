# Multiregion


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**regions** | [**List[MultiregionRegion]**](MultiregionRegion.md) |  | [optional] 
**strategy** | [**MultiregionStrategy**](MultiregionStrategy.md) |  | [optional] 

## Example

```python
from nomad_generated.models.multiregion import Multiregion

# TODO update the JSON string below
json = "{}"
# create an instance of Multiregion from a JSON string
multiregion_instance = Multiregion.from_json(json)
# print the JSON string representation of the object
print Multiregion.to_json()

# convert the object into a dict
multiregion_dict = multiregion_instance.to_dict()
# create an instance of Multiregion from a dict
multiregion_form_dict = multiregion.from_dict(multiregion_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


