# HostVolumeInfo


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**path** | **str** |  | [optional] 
**read_only** | **bool** |  | [optional] 

## Example

```python
from nomad_generated.models.host_volume_info import HostVolumeInfo

# TODO update the JSON string below
json = "{}"
# create an instance of HostVolumeInfo from a JSON string
host_volume_info_instance = HostVolumeInfo.from_json(json)
# print the JSON string representation of the object
print HostVolumeInfo.to_json()

# convert the object into a dict
host_volume_info_dict = host_volume_info_instance.to_dict()
# create an instance of HostVolumeInfo from a dict
host_volume_info_form_dict = host_volume_info.from_dict(host_volume_info_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


