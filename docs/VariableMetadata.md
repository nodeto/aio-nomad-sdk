# VariableMetadata


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**create_index** | **int** |  | [optional] 
**create_time** | **int** |  | [optional] 
**modify_index** | **int** |  | [optional] 
**modify_time** | **int** |  | [optional] 
**namespace** | **str** |  | [optional] 
**path** | **str** |  | [optional] 

## Example

```python
from nomad_generated.models.variable_metadata import VariableMetadata

# TODO update the JSON string below
json = "{}"
# create an instance of VariableMetadata from a JSON string
variable_metadata_instance = VariableMetadata.from_json(json)
# print the JSON string representation of the object
print VariableMetadata.to_json()

# convert the object into a dict
variable_metadata_dict = variable_metadata_instance.to_dict()
# create an instance of VariableMetadata from a dict
variable_metadata_form_dict = variable_metadata.from_dict(variable_metadata_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


