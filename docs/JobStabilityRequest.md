# JobStabilityRequest


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**job_id** | **str** |  | [optional] 
**job_version** | **int** |  | [optional] 
**namespace** | **str** |  | [optional] 
**region** | **str** |  | [optional] 
**secret_id** | **str** |  | [optional] 
**stable** | **bool** |  | [optional] 

## Example

```python
from nomad_generated.models.job_stability_request import JobStabilityRequest

# TODO update the JSON string below
json = "{}"
# create an instance of JobStabilityRequest from a JSON string
job_stability_request_instance = JobStabilityRequest.from_json(json)
# print the JSON string representation of the object
print JobStabilityRequest.to_json()

# convert the object into a dict
job_stability_request_dict = job_stability_request_instance.to_dict()
# create an instance of JobStabilityRequest from a dict
job_stability_request_form_dict = job_stability_request.from_dict(job_stability_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


