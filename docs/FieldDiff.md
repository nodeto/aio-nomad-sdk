# FieldDiff


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**annotations** | **List[str]** |  | [optional] 
**name** | **str** |  | [optional] 
**new** | **str** |  | [optional] 
**old** | **str** |  | [optional] 
**type** | **str** |  | [optional] 

## Example

```python
from nomad_generated.models.field_diff import FieldDiff

# TODO update the JSON string below
json = "{}"
# create an instance of FieldDiff from a JSON string
field_diff_instance = FieldDiff.from_json(json)
# print the JSON string representation of the object
print FieldDiff.to_json()

# convert the object into a dict
field_diff_dict = field_diff_instance.to_dict()
# create an instance of FieldDiff from a dict
field_diff_form_dict = field_diff.from_dict(field_diff_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


