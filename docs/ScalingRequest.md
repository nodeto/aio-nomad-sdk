# ScalingRequest


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**count** | **int** |  | [optional] 
**error** | **bool** |  | [optional] 
**message** | **str** |  | [optional] 
**meta** | **Dict[str, object]** |  | [optional] 
**namespace** | **str** |  | [optional] 
**policy_override** | **bool** |  | [optional] 
**region** | **str** |  | [optional] 
**secret_id** | **str** |  | [optional] 
**target** | **Dict[str, str]** |  | [optional] 

## Example

```python
from nomad_generated.models.scaling_request import ScalingRequest

# TODO update the JSON string below
json = "{}"
# create an instance of ScalingRequest from a JSON string
scaling_request_instance = ScalingRequest.from_json(json)
# print the JSON string representation of the object
print ScalingRequest.to_json()

# convert the object into a dict
scaling_request_dict = scaling_request_instance.to_dict()
# create an instance of ScalingRequest from a dict
scaling_request_form_dict = scaling_request.from_dict(scaling_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


