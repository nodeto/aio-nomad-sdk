# NodeReservedResources


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cpu** | [**NodeReservedCpuResources**](NodeReservedCpuResources.md) |  | [optional] 
**disk** | [**NodeReservedDiskResources**](NodeReservedDiskResources.md) |  | [optional] 
**memory** | [**NodeReservedMemoryResources**](NodeReservedMemoryResources.md) |  | [optional] 
**networks** | [**NodeReservedNetworkResources**](NodeReservedNetworkResources.md) |  | [optional] 

## Example

```python
from nomad_generated.models.node_reserved_resources import NodeReservedResources

# TODO update the JSON string below
json = "{}"
# create an instance of NodeReservedResources from a JSON string
node_reserved_resources_instance = NodeReservedResources.from_json(json)
# print the JSON string representation of the object
print NodeReservedResources.to_json()

# convert the object into a dict
node_reserved_resources_dict = node_reserved_resources_instance.to_dict()
# create an instance of NodeReservedResources from a dict
node_reserved_resources_form_dict = node_reserved_resources.from_dict(node_reserved_resources_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


