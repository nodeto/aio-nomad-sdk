# CSISnapshotListResponse


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**known_leader** | **bool** |  | [optional] 
**last_contact** | **int** |  | [optional] 
**last_index** | **int** |  | [optional] 
**next_token** | **str** |  | [optional] 
**request_time** | **int** |  | [optional] 
**snapshots** | [**List[CSISnapshot]**](CSISnapshot.md) |  | [optional] 

## Example

```python
from nomad_generated.models.csi_snapshot_list_response import CSISnapshotListResponse

# TODO update the JSON string below
json = "{}"
# create an instance of CSISnapshotListResponse from a JSON string
csi_snapshot_list_response_instance = CSISnapshotListResponse.from_json(json)
# print the JSON string representation of the object
print CSISnapshotListResponse.to_json()

# convert the object into a dict
csi_snapshot_list_response_dict = csi_snapshot_list_response_instance.to_dict()
# create an instance of CSISnapshotListResponse from a dict
csi_snapshot_list_response_form_dict = csi_snapshot_list_response.from_dict(csi_snapshot_list_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


