# MultiregionStrategy


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**max_parallel** | **int** |  | [optional] 
**on_failure** | **str** |  | [optional] 

## Example

```python
from nomad_generated.models.multiregion_strategy import MultiregionStrategy

# TODO update the JSON string below
json = "{}"
# create an instance of MultiregionStrategy from a JSON string
multiregion_strategy_instance = MultiregionStrategy.from_json(json)
# print the JSON string representation of the object
print MultiregionStrategy.to_json()

# convert the object into a dict
multiregion_strategy_dict = multiregion_strategy_instance.to_dict()
# create an instance of MultiregionStrategy from a dict
multiregion_strategy_form_dict = multiregion_strategy.from_dict(multiregion_strategy_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


