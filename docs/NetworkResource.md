# NetworkResource


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cidr** | **str** |  | [optional] 
**dns** | [**DNSConfig**](DNSConfig.md) |  | [optional] 
**device** | **str** |  | [optional] 
**dynamic_ports** | [**List[Port]**](Port.md) |  | [optional] 
**hostname** | **str** |  | [optional] 
**ip** | **str** |  | [optional] 
**m_bits** | **int** |  | [optional] 
**mode** | **str** |  | [optional] 
**reserved_ports** | [**List[Port]**](Port.md) |  | [optional] 

## Example

```python
from nomad_generated.models.network_resource import NetworkResource

# TODO update the JSON string below
json = "{}"
# create an instance of NetworkResource from a JSON string
network_resource_instance = NetworkResource.from_json(json)
# print the JSON string representation of the object
print NetworkResource.to_json()

# convert the object into a dict
network_resource_dict = network_resource_instance.to_dict()
# create an instance of NetworkResource from a dict
network_resource_form_dict = network_resource.from_dict(network_resource_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


