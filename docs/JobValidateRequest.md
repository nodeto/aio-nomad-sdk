# JobValidateRequest


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**job** | [**Job**](Job.md) |  | [optional] 
**namespace** | **str** |  | [optional] 
**region** | **str** |  | [optional] 
**secret_id** | **str** |  | [optional] 

## Example

```python
from nomad_generated.models.job_validate_request import JobValidateRequest

# TODO update the JSON string below
json = "{}"
# create an instance of JobValidateRequest from a JSON string
job_validate_request_instance = JobValidateRequest.from_json(json)
# print the JSON string representation of the object
print JobValidateRequest.to_json()

# convert the object into a dict
job_validate_request_dict = job_validate_request_instance.to_dict()
# create an instance of JobValidateRequest from a dict
job_validate_request_form_dict = job_validate_request.from_dict(job_validate_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


