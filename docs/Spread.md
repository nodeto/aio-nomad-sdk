# Spread


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**attribute** | **str** |  | [optional] 
**spread_target** | [**List[SpreadTarget]**](SpreadTarget.md) |  | [optional] 
**weight** | **int** |  | [optional] 

## Example

```python
from nomad_generated.models.spread import Spread

# TODO update the JSON string below
json = "{}"
# create an instance of Spread from a JSON string
spread_instance = Spread.from_json(json)
# print the JSON string representation of the object
print Spread.to_json()

# convert the object into a dict
spread_dict = spread_instance.to_dict()
# create an instance of Spread from a dict
spread_form_dict = spread.from_dict(spread_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


