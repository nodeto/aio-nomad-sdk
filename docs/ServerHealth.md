# ServerHealth


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**address** | **str** |  | [optional] 
**healthy** | **bool** |  | [optional] 
**id** | **str** |  | [optional] 
**last_contact** | **int** |  | [optional] 
**last_index** | **int** |  | [optional] 
**last_term** | **int** |  | [optional] 
**leader** | **bool** |  | [optional] 
**name** | **str** |  | [optional] 
**serf_status** | **str** |  | [optional] 
**stable_since** | **datetime** |  | [optional] 
**version** | **str** |  | [optional] 
**voter** | **bool** |  | [optional] 

## Example

```python
from nomad_generated.models.server_health import ServerHealth

# TODO update the JSON string below
json = "{}"
# create an instance of ServerHealth from a JSON string
server_health_instance = ServerHealth.from_json(json)
# print the JSON string representation of the object
print ServerHealth.to_json()

# convert the object into a dict
server_health_dict = server_health_instance.to_dict()
# create an instance of ServerHealth from a dict
server_health_form_dict = server_health.from_dict(server_health_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


