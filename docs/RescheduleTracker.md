# RescheduleTracker


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**events** | [**List[RescheduleEvent]**](RescheduleEvent.md) |  | [optional] 

## Example

```python
from nomad_generated.models.reschedule_tracker import RescheduleTracker

# TODO update the JSON string below
json = "{}"
# create an instance of RescheduleTracker from a JSON string
reschedule_tracker_instance = RescheduleTracker.from_json(json)
# print the JSON string representation of the object
print RescheduleTracker.to_json()

# convert the object into a dict
reschedule_tracker_dict = reschedule_tracker_instance.to_dict()
# create an instance of RescheduleTracker from a dict
reschedule_tracker_form_dict = reschedule_tracker.from_dict(reschedule_tracker_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


