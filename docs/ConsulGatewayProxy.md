# ConsulGatewayProxy


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**config** | **Dict[str, object]** |  | [optional] 
**connect_timeout** | **int** |  | [optional] 
**envoy_dns_discovery_type** | **str** |  | [optional] 
**envoy_gateway_bind_addresses** | [**Dict[str, ConsulGatewayBindAddress]**](ConsulGatewayBindAddress.md) |  | [optional] 
**envoy_gateway_bind_tagged_addresses** | **bool** |  | [optional] 
**envoy_gateway_no_default_bind** | **bool** |  | [optional] 

## Example

```python
from nomad_generated.models.consul_gateway_proxy import ConsulGatewayProxy

# TODO update the JSON string below
json = "{}"
# create an instance of ConsulGatewayProxy from a JSON string
consul_gateway_proxy_instance = ConsulGatewayProxy.from_json(json)
# print the JSON string representation of the object
print ConsulGatewayProxy.to_json()

# convert the object into a dict
consul_gateway_proxy_dict = consul_gateway_proxy_instance.to_dict()
# create an instance of ConsulGatewayProxy from a dict
consul_gateway_proxy_form_dict = consul_gateway_proxy.from_dict(consul_gateway_proxy_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


