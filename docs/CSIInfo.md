# CSIInfo


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**alloc_id** | **str** |  | [optional] 
**controller_info** | [**CSIControllerInfo**](CSIControllerInfo.md) |  | [optional] 
**health_description** | **str** |  | [optional] 
**healthy** | **bool** |  | [optional] 
**node_info** | [**CSINodeInfo**](CSINodeInfo.md) |  | [optional] 
**plugin_id** | **str** |  | [optional] 
**requires_controller_plugin** | **bool** |  | [optional] 
**requires_topologies** | **bool** |  | [optional] 
**update_time** | **datetime** |  | [optional] 

## Example

```python
from nomad_generated.models.csi_info import CSIInfo

# TODO update the JSON string below
json = "{}"
# create an instance of CSIInfo from a JSON string
csi_info_instance = CSIInfo.from_json(json)
# print the JSON string representation of the object
print CSIInfo.to_json()

# convert the object into a dict
csi_info_dict = csi_info_instance.to_dict()
# create an instance of CSIInfo from a dict
csi_info_form_dict = csi_info.from_dict(csi_info_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


