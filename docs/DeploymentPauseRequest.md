# DeploymentPauseRequest


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**deployment_id** | **str** |  | [optional] 
**namespace** | **str** |  | [optional] 
**pause** | **bool** |  | [optional] 
**region** | **str** |  | [optional] 
**secret_id** | **str** |  | [optional] 

## Example

```python
from nomad_generated.models.deployment_pause_request import DeploymentPauseRequest

# TODO update the JSON string below
json = "{}"
# create an instance of DeploymentPauseRequest from a JSON string
deployment_pause_request_instance = DeploymentPauseRequest.from_json(json)
# print the JSON string representation of the object
print DeploymentPauseRequest.to_json()

# convert the object into a dict
deployment_pause_request_dict = deployment_pause_request_instance.to_dict()
# create an instance of DeploymentPauseRequest from a dict
deployment_pause_request_form_dict = deployment_pause_request.from_dict(deployment_pause_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


