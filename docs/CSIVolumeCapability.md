# CSIVolumeCapability


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**access_mode** | **str** |  | [optional] 
**attachment_mode** | **str** |  | [optional] 

## Example

```python
from nomad_generated.models.csi_volume_capability import CSIVolumeCapability

# TODO update the JSON string below
json = "{}"
# create an instance of CSIVolumeCapability from a JSON string
csi_volume_capability_instance = CSIVolumeCapability.from_json(json)
# print the JSON string representation of the object
print CSIVolumeCapability.to_json()

# convert the object into a dict
csi_volume_capability_dict = csi_volume_capability_instance.to_dict()
# create an instance of CSIVolumeCapability from a dict
csi_volume_capability_form_dict = csi_volume_capability.from_dict(csi_volume_capability_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


