# GaugeValue


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**labels** | **Dict[str, str]** |  | [optional] 
**name** | **str** |  | [optional] 
**value** | **float** |  | [optional] 

## Example

```python
from nomad_generated.models.gauge_value import GaugeValue

# TODO update the JSON string below
json = "{}"
# create an instance of GaugeValue from a JSON string
gauge_value_instance = GaugeValue.from_json(json)
# print the JSON string representation of the object
print GaugeValue.to_json()

# convert the object into a dict
gauge_value_dict = gauge_value_instance.to_dict()
# create an instance of GaugeValue from a dict
gauge_value_form_dict = gauge_value.from_dict(gauge_value_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


