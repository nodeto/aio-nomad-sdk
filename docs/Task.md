# Task


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**affinities** | [**List[Affinity]**](Affinity.md) |  | [optional] 
**artifacts** | [**List[TaskArtifact]**](TaskArtifact.md) |  | [optional] 
**csi_plugin_config** | [**TaskCSIPluginConfig**](TaskCSIPluginConfig.md) |  | [optional] 
**config** | **Dict[str, object]** |  | [optional] 
**constraints** | [**List[Constraint]**](Constraint.md) |  | [optional] 
**dispatch_payload** | [**DispatchPayloadConfig**](DispatchPayloadConfig.md) |  | [optional] 
**driver** | **str** |  | [optional] 
**env** | **Dict[str, str]** |  | [optional] 
**kill_signal** | **str** |  | [optional] 
**kill_timeout** | **int** |  | [optional] 
**kind** | **str** |  | [optional] 
**leader** | **bool** |  | [optional] 
**lifecycle** | [**TaskLifecycle**](TaskLifecycle.md) |  | [optional] 
**log_config** | [**LogConfig**](LogConfig.md) |  | [optional] 
**meta** | **Dict[str, str]** |  | [optional] 
**name** | **str** |  | [optional] 
**resources** | [**Resources**](Resources.md) |  | [optional] 
**restart_policy** | [**RestartPolicy**](RestartPolicy.md) |  | [optional] 
**scaling_policies** | [**List[ScalingPolicy]**](ScalingPolicy.md) |  | [optional] 
**services** | [**List[Service]**](Service.md) |  | [optional] 
**shutdown_delay** | **int** |  | [optional] 
**templates** | [**List[Template]**](Template.md) |  | [optional] 
**user** | **str** |  | [optional] 
**vault** | [**Vault**](Vault.md) |  | [optional] 
**volume_mounts** | [**List[VolumeMount]**](VolumeMount.md) |  | [optional] 

## Example

```python
from nomad_generated.models.task import Task

# TODO update the JSON string below
json = "{}"
# create an instance of Task from a JSON string
task_instance = Task.from_json(json)
# print the JSON string representation of the object
print Task.to_json()

# convert the object into a dict
task_dict = task_instance.to_dict()
# create an instance of Task from a dict
task_form_dict = task.from_dict(task_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


