# NodeDiskResources


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**disk_mb** | **int** |  | [optional] 

## Example

```python
from nomad_generated.models.node_disk_resources import NodeDiskResources

# TODO update the JSON string below
json = "{}"
# create an instance of NodeDiskResources from a JSON string
node_disk_resources_instance = NodeDiskResources.from_json(json)
# print the JSON string representation of the object
print NodeDiskResources.to_json()

# convert the object into a dict
node_disk_resources_dict = node_disk_resources_instance.to_dict()
# create an instance of NodeDiskResources from a dict
node_disk_resources_form_dict = node_disk_resources.from_dict(node_disk_resources_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


