# JobEvaluateRequest


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**eval_options** | [**EvalOptions**](EvalOptions.md) |  | [optional] 
**job_id** | **str** |  | [optional] 
**namespace** | **str** |  | [optional] 
**region** | **str** |  | [optional] 
**secret_id** | **str** |  | [optional] 

## Example

```python
from nomad_generated.models.job_evaluate_request import JobEvaluateRequest

# TODO update the JSON string below
json = "{}"
# create an instance of JobEvaluateRequest from a JSON string
job_evaluate_request_instance = JobEvaluateRequest.from_json(json)
# print the JSON string representation of the object
print JobEvaluateRequest.to_json()

# convert the object into a dict
job_evaluate_request_dict = job_evaluate_request_instance.to_dict()
# create an instance of JobEvaluateRequest from a dict
job_evaluate_request_form_dict = job_evaluate_request.from_dict(job_evaluate_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


