# RestartPolicy


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**attempts** | **int** |  | [optional] 
**delay** | **int** |  | [optional] 
**interval** | **int** |  | [optional] 
**mode** | **str** |  | [optional] 

## Example

```python
from nomad_generated.models.restart_policy import RestartPolicy

# TODO update the JSON string below
json = "{}"
# create an instance of RestartPolicy from a JSON string
restart_policy_instance = RestartPolicy.from_json(json)
# print the JSON string representation of the object
print RestartPolicy.to_json()

# convert the object into a dict
restart_policy_dict = restart_policy_instance.to_dict()
# create an instance of RestartPolicy from a dict
restart_policy_form_dict = restart_policy.from_dict(restart_policy_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


