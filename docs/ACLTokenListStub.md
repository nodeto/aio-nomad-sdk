# ACLTokenListStub


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**accessor_id** | **str** |  | [optional] 
**create_index** | **int** |  | [optional] 
**create_time** | **datetime** |  | [optional] 
**var_global** | **bool** |  | [optional] 
**modify_index** | **int** |  | [optional] 
**name** | **str** |  | [optional] 
**policies** | **List[str]** |  | [optional] 
**roles** | [**List[ACLTokenRoleLink]**](ACLTokenRoleLink.md) |  | [optional] 
**type** | **str** |  | [optional] 
**expiration_time** | **datetime** |  | [optional] 

## Example

```python
from nomad_generated.models.acl_token_list_stub import ACLTokenListStub

# TODO update the JSON string below
json = "{}"
# create an instance of ACLTokenListStub from a JSON string
acl_token_list_stub_instance = ACLTokenListStub.from_json(json)
# print the JSON string representation of the object
print ACLTokenListStub.to_json()

# convert the object into a dict
acl_token_list_stub_dict = acl_token_list_stub_instance.to_dict()
# create an instance of ACLTokenListStub from a dict
acl_token_list_stub_form_dict = acl_token_list_stub.from_dict(acl_token_list_stub_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


