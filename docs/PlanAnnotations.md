# PlanAnnotations


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**desired_tg_updates** | [**Dict[str, DesiredUpdates]**](DesiredUpdates.md) |  | [optional] 
**preempted_allocs** | [**List[AllocationListStub]**](AllocationListStub.md) |  | [optional] 

## Example

```python
from nomad_generated.models.plan_annotations import PlanAnnotations

# TODO update the JSON string below
json = "{}"
# create an instance of PlanAnnotations from a JSON string
plan_annotations_instance = PlanAnnotations.from_json(json)
# print the JSON string representation of the object
print PlanAnnotations.to_json()

# convert the object into a dict
plan_annotations_dict = plan_annotations_instance.to_dict()
# create an instance of PlanAnnotations from a dict
plan_annotations_form_dict = plan_annotations.from_dict(plan_annotations_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


