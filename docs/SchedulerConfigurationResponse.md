# SchedulerConfigurationResponse


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**known_leader** | **bool** |  | [optional] 
**last_contact** | **int** |  | [optional] 
**last_index** | **int** |  | [optional] 
**next_token** | **str** |  | [optional] 
**request_time** | **int** |  | [optional] 
**scheduler_config** | [**SchedulerConfiguration**](SchedulerConfiguration.md) |  | [optional] 

## Example

```python
from nomad_generated.models.scheduler_configuration_response import SchedulerConfigurationResponse

# TODO update the JSON string below
json = "{}"
# create an instance of SchedulerConfigurationResponse from a JSON string
scheduler_configuration_response_instance = SchedulerConfigurationResponse.from_json(json)
# print the JSON string representation of the object
print SchedulerConfigurationResponse.to_json()

# convert the object into a dict
scheduler_configuration_response_dict = scheduler_configuration_response_instance.to_dict()
# create an instance of SchedulerConfigurationResponse from a dict
scheduler_configuration_response_form_dict = scheduler_configuration_response.from_dict(scheduler_configuration_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


