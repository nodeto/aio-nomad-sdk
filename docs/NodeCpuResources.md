# NodeCpuResources


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cpu_shares** | **int** |  | [optional] 
**reservable_cpu_cores** | **List[int]** |  | [optional] 
**total_cpu_cores** | **int** |  | [optional] 

## Example

```python
from nomad_generated.models.node_cpu_resources import NodeCpuResources

# TODO update the JSON string below
json = "{}"
# create an instance of NodeCpuResources from a JSON string
node_cpu_resources_instance = NodeCpuResources.from_json(json)
# print the JSON string representation of the object
print NodeCpuResources.to_json()

# convert the object into a dict
node_cpu_resources_dict = node_cpu_resources_instance.to_dict()
# create an instance of NodeCpuResources from a dict
node_cpu_resources_form_dict = node_cpu_resources.from_dict(node_cpu_resources_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


