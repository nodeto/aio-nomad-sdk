# CSIVolumeListExternalResponse


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**next_token** | **str** |  | [optional] 
**volumes** | [**List[CSIVolumeExternalStub]**](CSIVolumeExternalStub.md) |  | [optional] 

## Example

```python
from nomad_generated.models.csi_volume_list_external_response import CSIVolumeListExternalResponse

# TODO update the JSON string below
json = "{}"
# create an instance of CSIVolumeListExternalResponse from a JSON string
csi_volume_list_external_response_instance = CSIVolumeListExternalResponse.from_json(json)
# print the JSON string representation of the object
print CSIVolumeListExternalResponse.to_json()

# convert the object into a dict
csi_volume_list_external_response_dict = csi_volume_list_external_response_instance.to_dict()
# create an instance of CSIVolumeListExternalResponse from a dict
csi_volume_list_external_response_form_dict = csi_volume_list_external_response.from_dict(csi_volume_list_external_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


