# JobRegisterResponse


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**eval_create_index** | **int** |  | [optional] 
**eval_id** | **str** |  | [optional] 
**job_modify_index** | **int** |  | [optional] 
**known_leader** | **bool** |  | [optional] 
**last_contact** | **int** |  | [optional] 
**last_index** | **int** |  | [optional] 
**next_token** | **str** |  | [optional] 
**request_time** | **int** |  | [optional] 
**warnings** | **str** |  | [optional] 

## Example

```python
from nomad_generated.models.job_register_response import JobRegisterResponse

# TODO update the JSON string below
json = "{}"
# create an instance of JobRegisterResponse from a JSON string
job_register_response_instance = JobRegisterResponse.from_json(json)
# print the JSON string representation of the object
print JobRegisterResponse.to_json()

# convert the object into a dict
job_register_response_dict = job_register_response_instance.to_dict()
# create an instance of JobRegisterResponse from a dict
job_register_response_form_dict = job_register_response.from_dict(job_register_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


