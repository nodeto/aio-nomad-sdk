# JobListStub


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**create_index** | **int** |  | [optional] 
**datacenters** | **List[str]** |  | [optional] 
**id** | **str** |  | [optional] 
**job_modify_index** | **int** |  | [optional] 
**job_summary** | [**JobSummary**](JobSummary.md) |  | [optional] 
**modify_index** | **int** |  | [optional] 
**name** | **str** |  | [optional] 
**namespace** | **str** |  | [optional] 
**parameterized_job** | **bool** |  | [optional] 
**parent_id** | **str** |  | [optional] 
**periodic** | **bool** |  | [optional] 
**priority** | **int** |  | [optional] 
**status** | **str** |  | [optional] 
**status_description** | **str** |  | [optional] 
**stop** | **bool** |  | [optional] 
**submit_time** | **int** |  | [optional] 
**type** | **str** |  | [optional] 

## Example

```python
from nomad_generated.models.job_list_stub import JobListStub

# TODO update the JSON string below
json = "{}"
# create an instance of JobListStub from a JSON string
job_list_stub_instance = JobListStub.from_json(json)
# print the JSON string representation of the object
print JobListStub.to_json()

# convert the object into a dict
job_list_stub_dict = job_list_stub_instance.to_dict()
# create an instance of JobListStub from a dict
job_list_stub_form_dict = job_list_stub.from_dict(job_list_stub_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


