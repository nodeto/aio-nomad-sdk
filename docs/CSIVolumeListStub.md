# CSIVolumeListStub


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**access_mode** | **str** |  | [optional] 
**attachment_mode** | **str** |  | [optional] 
**controller_required** | **bool** |  | [optional] 
**controllers_expected** | **int** |  | [optional] 
**controllers_healthy** | **int** |  | [optional] 
**create_index** | **int** |  | [optional] 
**current_readers** | **int** |  | [optional] 
**current_writers** | **int** |  | [optional] 
**external_id** | **str** |  | [optional] 
**id** | **str** |  | [optional] 
**modify_index** | **int** |  | [optional] 
**name** | **str** |  | [optional] 
**namespace** | **str** |  | [optional] 
**nodes_expected** | **int** |  | [optional] 
**nodes_healthy** | **int** |  | [optional] 
**plugin_id** | **str** |  | [optional] 
**provider** | **str** |  | [optional] 
**resource_exhausted** | **datetime** |  | [optional] 
**schedulable** | **bool** |  | [optional] 
**topologies** | [**List[CSITopology]**](CSITopology.md) |  | [optional] 

## Example

```python
from nomad_generated.models.csi_volume_list_stub import CSIVolumeListStub

# TODO update the JSON string below
json = "{}"
# create an instance of CSIVolumeListStub from a JSON string
csi_volume_list_stub_instance = CSIVolumeListStub.from_json(json)
# print the JSON string representation of the object
print CSIVolumeListStub.to_json()

# convert the object into a dict
csi_volume_list_stub_dict = csi_volume_list_stub_instance.to_dict()
# create an instance of CSIVolumeListStub from a dict
csi_volume_list_stub_form_dict = csi_volume_list_stub.from_dict(csi_volume_list_stub_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


