# NodeDevice


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**health_description** | **str** |  | [optional] 
**healthy** | **bool** |  | [optional] 
**id** | **str** |  | [optional] 
**locality** | [**NodeDeviceLocality**](NodeDeviceLocality.md) |  | [optional] 

## Example

```python
from nomad_generated.models.node_device import NodeDevice

# TODO update the JSON string below
json = "{}"
# create an instance of NodeDevice from a JSON string
node_device_instance = NodeDevice.from_json(json)
# print the JSON string representation of the object
print NodeDevice.to_json()

# convert the object into a dict
node_device_dict = node_device_instance.to_dict()
# create an instance of NodeDevice from a dict
node_device_form_dict = node_device.from_dict(node_device_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


