# TaskLifecycle


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**hook** | **str** |  | [optional] 
**sidecar** | **bool** |  | [optional] 

## Example

```python
from nomad_generated.models.task_lifecycle import TaskLifecycle

# TODO update the JSON string below
json = "{}"
# create an instance of TaskLifecycle from a JSON string
task_lifecycle_instance = TaskLifecycle.from_json(json)
# print the JSON string representation of the object
print TaskLifecycle.to_json()

# convert the object into a dict
task_lifecycle_dict = task_lifecycle_instance.to_dict()
# create an instance of TaskLifecycle from a dict
task_lifecycle_form_dict = task_lifecycle.from_dict(task_lifecycle_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


