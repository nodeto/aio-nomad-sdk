# CSISnapshot


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**create_time** | **int** |  | [optional] 
**external_source_volume_id** | **str** |  | [optional] 
**id** | **str** |  | [optional] 
**is_ready** | **bool** |  | [optional] 
**name** | **str** |  | [optional] 
**parameters** | **Dict[str, str]** |  | [optional] 
**plugin_id** | **str** |  | [optional] 
**secrets** | **Dict[str, str]** |  | [optional] 
**size_bytes** | **int** |  | [optional] 
**source_volume_id** | **str** |  | [optional] 

## Example

```python
from nomad_generated.models.csi_snapshot import CSISnapshot

# TODO update the JSON string below
json = "{}"
# create an instance of CSISnapshot from a JSON string
csi_snapshot_instance = CSISnapshot.from_json(json)
# print the JSON string representation of the object
print CSISnapshot.to_json()

# convert the object into a dict
csi_snapshot_dict = csi_snapshot_instance.to_dict()
# create an instance of CSISnapshot from a dict
csi_snapshot_form_dict = csi_snapshot.from_dict(csi_snapshot_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


