# JobDispatchResponse


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dispatched_job_id** | **str** |  | [optional] 
**eval_create_index** | **int** |  | [optional] 
**eval_id** | **str** |  | [optional] 
**job_create_index** | **int** |  | [optional] 
**last_index** | **int** |  | [optional] 
**request_time** | **int** |  | [optional] 

## Example

```python
from nomad_generated.models.job_dispatch_response import JobDispatchResponse

# TODO update the JSON string below
json = "{}"
# create an instance of JobDispatchResponse from a JSON string
job_dispatch_response_instance = JobDispatchResponse.from_json(json)
# print the JSON string representation of the object
print JobDispatchResponse.to_json()

# convert the object into a dict
job_dispatch_response_dict = job_dispatch_response_instance.to_dict()
# create an instance of JobDispatchResponse from a dict
job_dispatch_response_form_dict = job_dispatch_response.from_dict(job_dispatch_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


