# SchedulerSetConfigurationResponse


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**last_index** | **int** |  | [optional] 
**request_time** | **int** |  | [optional] 
**updated** | **bool** |  | [optional] 

## Example

```python
from nomad_generated.models.scheduler_set_configuration_response import SchedulerSetConfigurationResponse

# TODO update the JSON string below
json = "{}"
# create an instance of SchedulerSetConfigurationResponse from a JSON string
scheduler_set_configuration_response_instance = SchedulerSetConfigurationResponse.from_json(json)
# print the JSON string representation of the object
print SchedulerSetConfigurationResponse.to_json()

# convert the object into a dict
scheduler_set_configuration_response_dict = scheduler_set_configuration_response_instance.to_dict()
# create an instance of SchedulerSetConfigurationResponse from a dict
scheduler_set_configuration_response_form_dict = scheduler_set_configuration_response.from_dict(scheduler_set_configuration_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


