# AllocatedDeviceResource


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**device_ids** | **List[str]** |  | [optional] 
**name** | **str** |  | [optional] 
**type** | **str** |  | [optional] 
**vendor** | **str** |  | [optional] 

## Example

```python
from nomad_generated.models.allocated_device_resource import AllocatedDeviceResource

# TODO update the JSON string below
json = "{}"
# create an instance of AllocatedDeviceResource from a JSON string
allocated_device_resource_instance = AllocatedDeviceResource.from_json(json)
# print the JSON string representation of the object
print AllocatedDeviceResource.to_json()

# convert the object into a dict
allocated_device_resource_dict = allocated_device_resource_instance.to_dict()
# create an instance of AllocatedDeviceResource from a dict
allocated_device_resource_form_dict = allocated_device_resource.from_dict(allocated_device_resource_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


