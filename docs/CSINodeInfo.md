# CSINodeInfo


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**accessible_topology** | [**CSITopology**](CSITopology.md) |  | [optional] 
**id** | **str** |  | [optional] 
**max_volumes** | **int** |  | [optional] 
**requires_node_stage_volume** | **bool** |  | [optional] 
**supports_condition** | **bool** |  | [optional] 
**supports_expand** | **bool** |  | [optional] 
**supports_stats** | **bool** |  | [optional] 

## Example

```python
from nomad_generated.models.csi_node_info import CSINodeInfo

# TODO update the JSON string below
json = "{}"
# create an instance of CSINodeInfo from a JSON string
csi_node_info_instance = CSINodeInfo.from_json(json)
# print the JSON string representation of the object
print CSINodeInfo.to_json()

# convert the object into a dict
csi_node_info_dict = csi_node_info_instance.to_dict()
# create an instance of CSINodeInfo from a dict
csi_node_info_form_dict = csi_node_info.from_dict(csi_node_info_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


