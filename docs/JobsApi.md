# nomad_generated.JobsApi

All URIs are relative to *http://127.0.0.1:4646/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**delete_job**](JobsApi.md#delete_job) | **DELETE** /job/{jobName} | 
[**get_job**](JobsApi.md#get_job) | **GET** /job/{jobName} | 
[**get_job_allocations**](JobsApi.md#get_job_allocations) | **GET** /job/{jobName}/allocations | 
[**get_job_deployment**](JobsApi.md#get_job_deployment) | **GET** /job/{jobName}/deployment | 
[**get_job_deployments**](JobsApi.md#get_job_deployments) | **GET** /job/{jobName}/deployments | 
[**get_job_evaluations**](JobsApi.md#get_job_evaluations) | **GET** /job/{jobName}/evaluations | 
[**get_job_scale_status**](JobsApi.md#get_job_scale_status) | **GET** /job/{jobName}/scale | 
[**get_job_summary**](JobsApi.md#get_job_summary) | **GET** /job/{jobName}/summary | 
[**get_job_versions**](JobsApi.md#get_job_versions) | **GET** /job/{jobName}/versions | 
[**get_jobs**](JobsApi.md#get_jobs) | **GET** /jobs | 
[**post_job**](JobsApi.md#post_job) | **POST** /job/{jobName} | 
[**post_job_dispatch**](JobsApi.md#post_job_dispatch) | **POST** /job/{jobName}/dispatch | 
[**post_job_evaluate**](JobsApi.md#post_job_evaluate) | **POST** /job/{jobName}/evaluate | 
[**post_job_parse**](JobsApi.md#post_job_parse) | **POST** /jobs/parse | 
[**post_job_periodic_force**](JobsApi.md#post_job_periodic_force) | **POST** /job/{jobName}/periodic/force | 
[**post_job_plan**](JobsApi.md#post_job_plan) | **POST** /job/{jobName}/plan | 
[**post_job_revert**](JobsApi.md#post_job_revert) | **POST** /job/{jobName}/revert | 
[**post_job_scaling_request**](JobsApi.md#post_job_scaling_request) | **POST** /job/{jobName}/scale | 
[**post_job_stability**](JobsApi.md#post_job_stability) | **POST** /job/{jobName}/stable | 
[**post_job_validate_request**](JobsApi.md#post_job_validate_request) | **POST** /validate/job | 
[**register_job**](JobsApi.md#register_job) | **POST** /jobs | 


# **delete_job**
> JobDeregisterResponse delete_job(job_name, region=region, namespace=namespace, x_nomad_token=x_nomad_token, idempotency_token=idempotency_token, purge=purge, var_global=var_global)



### Example

* Api Key Authentication (X-Nomad-Token):
```python
import time
import os
import nomad_generated
from nomad_generated.models.job_deregister_response import JobDeregisterResponse
from nomad_generated.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://127.0.0.1:4646/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = nomad_generated.Configuration(
    host = "http://127.0.0.1:4646/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: X-Nomad-Token
configuration.api_key['X-Nomad-Token'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['X-Nomad-Token'] = 'Bearer'

# Enter a context with an instance of the API client
async with nomad_generated.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = nomad_generated.JobsApi(api_client)
    job_name = 'job_name_example' # str | The job identifier.
    region = 'region_example' # str | Filters results based on the specified region. (optional)
    namespace = 'namespace_example' # str | Filters results based on the specified namespace. (optional)
    x_nomad_token = 'x_nomad_token_example' # str | A Nomad ACL token. (optional)
    idempotency_token = 'idempotency_token_example' # str | Can be used to ensure operations are only run once. (optional)
    purge = True # bool | Boolean flag indicating whether to purge allocations of the job after deleting. (optional)
    var_global = True # bool | Boolean flag indicating whether the operation should apply to all instances of the job globally. (optional)

    try:
        api_response = await api_instance.delete_job(job_name, region=region, namespace=namespace, x_nomad_token=x_nomad_token, idempotency_token=idempotency_token, purge=purge, var_global=var_global)
        print("The response of JobsApi->delete_job:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling JobsApi->delete_job: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **job_name** | **str**| The job identifier. | 
 **region** | **str**| Filters results based on the specified region. | [optional] 
 **namespace** | **str**| Filters results based on the specified namespace. | [optional] 
 **x_nomad_token** | **str**| A Nomad ACL token. | [optional] 
 **idempotency_token** | **str**| Can be used to ensure operations are only run once. | [optional] 
 **purge** | **bool**| Boolean flag indicating whether to purge allocations of the job after deleting. | [optional] 
 **var_global** | **bool**| Boolean flag indicating whether the operation should apply to all instances of the job globally. | [optional] 

### Return type

[**JobDeregisterResponse**](JobDeregisterResponse.md)

### Authorization

[X-Nomad-Token](../README.md#X-Nomad-Token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  * X-Nomad-Index - A unique identifier representing the current state of the requested resource. On a new Nomad cluster the value of this index starts at 1. <br>  |
**400** | Bad request |  -  |
**403** | Forbidden |  -  |
**405** | Method not allowed |  -  |
**500** | Internal server error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_job**
> Job get_job(job_name, region=region, namespace=namespace, index=index, wait=wait, stale=stale, prefix=prefix, x_nomad_token=x_nomad_token, per_page=per_page, next_token=next_token)



### Example

* Api Key Authentication (X-Nomad-Token):
```python
import time
import os
import nomad_generated
from nomad_generated.models.job import Job
from nomad_generated.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://127.0.0.1:4646/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = nomad_generated.Configuration(
    host = "http://127.0.0.1:4646/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: X-Nomad-Token
configuration.api_key['X-Nomad-Token'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['X-Nomad-Token'] = 'Bearer'

# Enter a context with an instance of the API client
async with nomad_generated.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = nomad_generated.JobsApi(api_client)
    job_name = 'job_name_example' # str | The job identifier.
    region = 'region_example' # str | Filters results based on the specified region. (optional)
    namespace = 'namespace_example' # str | Filters results based on the specified namespace. (optional)
    index = 56 # int | If set, wait until query exceeds given index. Must be provided with WaitParam. (optional)
    wait = 'wait_example' # str | Provided with IndexParam to wait for change. (optional)
    stale = 'stale_example' # str | If present, results will include stale reads. (optional)
    prefix = 'prefix_example' # str | Constrains results to jobs that start with the defined prefix (optional)
    x_nomad_token = 'x_nomad_token_example' # str | A Nomad ACL token. (optional)
    per_page = 56 # int | Maximum number of results to return. (optional)
    next_token = 'next_token_example' # str | Indicates where to start paging for queries that support pagination. (optional)

    try:
        api_response = await api_instance.get_job(job_name, region=region, namespace=namespace, index=index, wait=wait, stale=stale, prefix=prefix, x_nomad_token=x_nomad_token, per_page=per_page, next_token=next_token)
        print("The response of JobsApi->get_job:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling JobsApi->get_job: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **job_name** | **str**| The job identifier. | 
 **region** | **str**| Filters results based on the specified region. | [optional] 
 **namespace** | **str**| Filters results based on the specified namespace. | [optional] 
 **index** | **int**| If set, wait until query exceeds given index. Must be provided with WaitParam. | [optional] 
 **wait** | **str**| Provided with IndexParam to wait for change. | [optional] 
 **stale** | **str**| If present, results will include stale reads. | [optional] 
 **prefix** | **str**| Constrains results to jobs that start with the defined prefix | [optional] 
 **x_nomad_token** | **str**| A Nomad ACL token. | [optional] 
 **per_page** | **int**| Maximum number of results to return. | [optional] 
 **next_token** | **str**| Indicates where to start paging for queries that support pagination. | [optional] 

### Return type

[**Job**](Job.md)

### Authorization

[X-Nomad-Token](../README.md#X-Nomad-Token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  * X-Nomad-Index - A unique identifier representing the current state of the requested resource. On a new Nomad cluster the value of this index starts at 1. <br>  * X-Nomad-KnownLeader - Boolean indicating if there is a known cluster leader. <br>  * X-Nomad-LastContact - The time in milliseconds that a server was last contacted by the leader node. <br>  |
**400** | Bad request |  -  |
**403** | Forbidden |  -  |
**405** | Method not allowed |  -  |
**500** | Internal server error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_job_allocations**
> List[AllocationListStub] get_job_allocations(job_name, region=region, namespace=namespace, index=index, wait=wait, stale=stale, prefix=prefix, x_nomad_token=x_nomad_token, per_page=per_page, next_token=next_token, all=all)



### Example

* Api Key Authentication (X-Nomad-Token):
```python
import time
import os
import nomad_generated
from nomad_generated.models.allocation_list_stub import AllocationListStub
from nomad_generated.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://127.0.0.1:4646/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = nomad_generated.Configuration(
    host = "http://127.0.0.1:4646/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: X-Nomad-Token
configuration.api_key['X-Nomad-Token'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['X-Nomad-Token'] = 'Bearer'

# Enter a context with an instance of the API client
async with nomad_generated.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = nomad_generated.JobsApi(api_client)
    job_name = 'job_name_example' # str | The job identifier.
    region = 'region_example' # str | Filters results based on the specified region. (optional)
    namespace = 'namespace_example' # str | Filters results based on the specified namespace. (optional)
    index = 56 # int | If set, wait until query exceeds given index. Must be provided with WaitParam. (optional)
    wait = 'wait_example' # str | Provided with IndexParam to wait for change. (optional)
    stale = 'stale_example' # str | If present, results will include stale reads. (optional)
    prefix = 'prefix_example' # str | Constrains results to jobs that start with the defined prefix (optional)
    x_nomad_token = 'x_nomad_token_example' # str | A Nomad ACL token. (optional)
    per_page = 56 # int | Maximum number of results to return. (optional)
    next_token = 'next_token_example' # str | Indicates where to start paging for queries that support pagination. (optional)
    all = True # bool | Specifies whether the list of allocations should include allocations from a previously registered job with the same ID. This is possible if the job is deregistered and reregistered. (optional)

    try:
        api_response = await api_instance.get_job_allocations(job_name, region=region, namespace=namespace, index=index, wait=wait, stale=stale, prefix=prefix, x_nomad_token=x_nomad_token, per_page=per_page, next_token=next_token, all=all)
        print("The response of JobsApi->get_job_allocations:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling JobsApi->get_job_allocations: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **job_name** | **str**| The job identifier. | 
 **region** | **str**| Filters results based on the specified region. | [optional] 
 **namespace** | **str**| Filters results based on the specified namespace. | [optional] 
 **index** | **int**| If set, wait until query exceeds given index. Must be provided with WaitParam. | [optional] 
 **wait** | **str**| Provided with IndexParam to wait for change. | [optional] 
 **stale** | **str**| If present, results will include stale reads. | [optional] 
 **prefix** | **str**| Constrains results to jobs that start with the defined prefix | [optional] 
 **x_nomad_token** | **str**| A Nomad ACL token. | [optional] 
 **per_page** | **int**| Maximum number of results to return. | [optional] 
 **next_token** | **str**| Indicates where to start paging for queries that support pagination. | [optional] 
 **all** | **bool**| Specifies whether the list of allocations should include allocations from a previously registered job with the same ID. This is possible if the job is deregistered and reregistered. | [optional] 

### Return type

[**List[AllocationListStub]**](AllocationListStub.md)

### Authorization

[X-Nomad-Token](../README.md#X-Nomad-Token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  * X-Nomad-Index - A unique identifier representing the current state of the requested resource. On a new Nomad cluster the value of this index starts at 1. <br>  * X-Nomad-KnownLeader - Boolean indicating if there is a known cluster leader. <br>  * X-Nomad-LastContact - The time in milliseconds that a server was last contacted by the leader node. <br>  |
**400** | Bad request |  -  |
**403** | Forbidden |  -  |
**405** | Method not allowed |  -  |
**500** | Internal server error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_job_deployment**
> Deployment get_job_deployment(job_name, region=region, namespace=namespace, index=index, wait=wait, stale=stale, prefix=prefix, x_nomad_token=x_nomad_token, per_page=per_page, next_token=next_token)



### Example

* Api Key Authentication (X-Nomad-Token):
```python
import time
import os
import nomad_generated
from nomad_generated.models.deployment import Deployment
from nomad_generated.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://127.0.0.1:4646/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = nomad_generated.Configuration(
    host = "http://127.0.0.1:4646/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: X-Nomad-Token
configuration.api_key['X-Nomad-Token'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['X-Nomad-Token'] = 'Bearer'

# Enter a context with an instance of the API client
async with nomad_generated.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = nomad_generated.JobsApi(api_client)
    job_name = 'job_name_example' # str | The job identifier.
    region = 'region_example' # str | Filters results based on the specified region. (optional)
    namespace = 'namespace_example' # str | Filters results based on the specified namespace. (optional)
    index = 56 # int | If set, wait until query exceeds given index. Must be provided with WaitParam. (optional)
    wait = 'wait_example' # str | Provided with IndexParam to wait for change. (optional)
    stale = 'stale_example' # str | If present, results will include stale reads. (optional)
    prefix = 'prefix_example' # str | Constrains results to jobs that start with the defined prefix (optional)
    x_nomad_token = 'x_nomad_token_example' # str | A Nomad ACL token. (optional)
    per_page = 56 # int | Maximum number of results to return. (optional)
    next_token = 'next_token_example' # str | Indicates where to start paging for queries that support pagination. (optional)

    try:
        api_response = await api_instance.get_job_deployment(job_name, region=region, namespace=namespace, index=index, wait=wait, stale=stale, prefix=prefix, x_nomad_token=x_nomad_token, per_page=per_page, next_token=next_token)
        print("The response of JobsApi->get_job_deployment:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling JobsApi->get_job_deployment: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **job_name** | **str**| The job identifier. | 
 **region** | **str**| Filters results based on the specified region. | [optional] 
 **namespace** | **str**| Filters results based on the specified namespace. | [optional] 
 **index** | **int**| If set, wait until query exceeds given index. Must be provided with WaitParam. | [optional] 
 **wait** | **str**| Provided with IndexParam to wait for change. | [optional] 
 **stale** | **str**| If present, results will include stale reads. | [optional] 
 **prefix** | **str**| Constrains results to jobs that start with the defined prefix | [optional] 
 **x_nomad_token** | **str**| A Nomad ACL token. | [optional] 
 **per_page** | **int**| Maximum number of results to return. | [optional] 
 **next_token** | **str**| Indicates where to start paging for queries that support pagination. | [optional] 

### Return type

[**Deployment**](Deployment.md)

### Authorization

[X-Nomad-Token](../README.md#X-Nomad-Token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  * X-Nomad-Index - A unique identifier representing the current state of the requested resource. On a new Nomad cluster the value of this index starts at 1. <br>  * X-Nomad-KnownLeader - Boolean indicating if there is a known cluster leader. <br>  * X-Nomad-LastContact - The time in milliseconds that a server was last contacted by the leader node. <br>  |
**400** | Bad request |  -  |
**403** | Forbidden |  -  |
**405** | Method not allowed |  -  |
**500** | Internal server error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_job_deployments**
> List[Deployment] get_job_deployments(job_name, region=region, namespace=namespace, index=index, wait=wait, stale=stale, prefix=prefix, x_nomad_token=x_nomad_token, per_page=per_page, next_token=next_token, all=all)



### Example

* Api Key Authentication (X-Nomad-Token):
```python
import time
import os
import nomad_generated
from nomad_generated.models.deployment import Deployment
from nomad_generated.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://127.0.0.1:4646/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = nomad_generated.Configuration(
    host = "http://127.0.0.1:4646/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: X-Nomad-Token
configuration.api_key['X-Nomad-Token'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['X-Nomad-Token'] = 'Bearer'

# Enter a context with an instance of the API client
async with nomad_generated.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = nomad_generated.JobsApi(api_client)
    job_name = 'job_name_example' # str | The job identifier.
    region = 'region_example' # str | Filters results based on the specified region. (optional)
    namespace = 'namespace_example' # str | Filters results based on the specified namespace. (optional)
    index = 56 # int | If set, wait until query exceeds given index. Must be provided with WaitParam. (optional)
    wait = 'wait_example' # str | Provided with IndexParam to wait for change. (optional)
    stale = 'stale_example' # str | If present, results will include stale reads. (optional)
    prefix = 'prefix_example' # str | Constrains results to jobs that start with the defined prefix (optional)
    x_nomad_token = 'x_nomad_token_example' # str | A Nomad ACL token. (optional)
    per_page = 56 # int | Maximum number of results to return. (optional)
    next_token = 'next_token_example' # str | Indicates where to start paging for queries that support pagination. (optional)
    all = 56 # int | Flag indicating whether to constrain by job creation index or not. (optional)

    try:
        api_response = await api_instance.get_job_deployments(job_name, region=region, namespace=namespace, index=index, wait=wait, stale=stale, prefix=prefix, x_nomad_token=x_nomad_token, per_page=per_page, next_token=next_token, all=all)
        print("The response of JobsApi->get_job_deployments:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling JobsApi->get_job_deployments: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **job_name** | **str**| The job identifier. | 
 **region** | **str**| Filters results based on the specified region. | [optional] 
 **namespace** | **str**| Filters results based on the specified namespace. | [optional] 
 **index** | **int**| If set, wait until query exceeds given index. Must be provided with WaitParam. | [optional] 
 **wait** | **str**| Provided with IndexParam to wait for change. | [optional] 
 **stale** | **str**| If present, results will include stale reads. | [optional] 
 **prefix** | **str**| Constrains results to jobs that start with the defined prefix | [optional] 
 **x_nomad_token** | **str**| A Nomad ACL token. | [optional] 
 **per_page** | **int**| Maximum number of results to return. | [optional] 
 **next_token** | **str**| Indicates where to start paging for queries that support pagination. | [optional] 
 **all** | **int**| Flag indicating whether to constrain by job creation index or not. | [optional] 

### Return type

[**List[Deployment]**](Deployment.md)

### Authorization

[X-Nomad-Token](../README.md#X-Nomad-Token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  * X-Nomad-Index - A unique identifier representing the current state of the requested resource. On a new Nomad cluster the value of this index starts at 1. <br>  * X-Nomad-KnownLeader - Boolean indicating if there is a known cluster leader. <br>  * X-Nomad-LastContact - The time in milliseconds that a server was last contacted by the leader node. <br>  |
**400** | Bad request |  -  |
**403** | Forbidden |  -  |
**405** | Method not allowed |  -  |
**500** | Internal server error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_job_evaluations**
> List[Evaluation] get_job_evaluations(job_name, region=region, namespace=namespace, index=index, wait=wait, stale=stale, prefix=prefix, x_nomad_token=x_nomad_token, per_page=per_page, next_token=next_token)



### Example

* Api Key Authentication (X-Nomad-Token):
```python
import time
import os
import nomad_generated
from nomad_generated.models.evaluation import Evaluation
from nomad_generated.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://127.0.0.1:4646/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = nomad_generated.Configuration(
    host = "http://127.0.0.1:4646/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: X-Nomad-Token
configuration.api_key['X-Nomad-Token'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['X-Nomad-Token'] = 'Bearer'

# Enter a context with an instance of the API client
async with nomad_generated.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = nomad_generated.JobsApi(api_client)
    job_name = 'job_name_example' # str | The job identifier.
    region = 'region_example' # str | Filters results based on the specified region. (optional)
    namespace = 'namespace_example' # str | Filters results based on the specified namespace. (optional)
    index = 56 # int | If set, wait until query exceeds given index. Must be provided with WaitParam. (optional)
    wait = 'wait_example' # str | Provided with IndexParam to wait for change. (optional)
    stale = 'stale_example' # str | If present, results will include stale reads. (optional)
    prefix = 'prefix_example' # str | Constrains results to jobs that start with the defined prefix (optional)
    x_nomad_token = 'x_nomad_token_example' # str | A Nomad ACL token. (optional)
    per_page = 56 # int | Maximum number of results to return. (optional)
    next_token = 'next_token_example' # str | Indicates where to start paging for queries that support pagination. (optional)

    try:
        api_response = await api_instance.get_job_evaluations(job_name, region=region, namespace=namespace, index=index, wait=wait, stale=stale, prefix=prefix, x_nomad_token=x_nomad_token, per_page=per_page, next_token=next_token)
        print("The response of JobsApi->get_job_evaluations:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling JobsApi->get_job_evaluations: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **job_name** | **str**| The job identifier. | 
 **region** | **str**| Filters results based on the specified region. | [optional] 
 **namespace** | **str**| Filters results based on the specified namespace. | [optional] 
 **index** | **int**| If set, wait until query exceeds given index. Must be provided with WaitParam. | [optional] 
 **wait** | **str**| Provided with IndexParam to wait for change. | [optional] 
 **stale** | **str**| If present, results will include stale reads. | [optional] 
 **prefix** | **str**| Constrains results to jobs that start with the defined prefix | [optional] 
 **x_nomad_token** | **str**| A Nomad ACL token. | [optional] 
 **per_page** | **int**| Maximum number of results to return. | [optional] 
 **next_token** | **str**| Indicates where to start paging for queries that support pagination. | [optional] 

### Return type

[**List[Evaluation]**](Evaluation.md)

### Authorization

[X-Nomad-Token](../README.md#X-Nomad-Token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  * X-Nomad-Index - A unique identifier representing the current state of the requested resource. On a new Nomad cluster the value of this index starts at 1. <br>  * X-Nomad-KnownLeader - Boolean indicating if there is a known cluster leader. <br>  * X-Nomad-LastContact - The time in milliseconds that a server was last contacted by the leader node. <br>  |
**400** | Bad request |  -  |
**403** | Forbidden |  -  |
**405** | Method not allowed |  -  |
**500** | Internal server error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_job_scale_status**
> JobScaleStatusResponse get_job_scale_status(job_name, region=region, namespace=namespace, index=index, wait=wait, stale=stale, prefix=prefix, x_nomad_token=x_nomad_token, per_page=per_page, next_token=next_token)



### Example

* Api Key Authentication (X-Nomad-Token):
```python
import time
import os
import nomad_generated
from nomad_generated.models.job_scale_status_response import JobScaleStatusResponse
from nomad_generated.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://127.0.0.1:4646/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = nomad_generated.Configuration(
    host = "http://127.0.0.1:4646/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: X-Nomad-Token
configuration.api_key['X-Nomad-Token'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['X-Nomad-Token'] = 'Bearer'

# Enter a context with an instance of the API client
async with nomad_generated.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = nomad_generated.JobsApi(api_client)
    job_name = 'job_name_example' # str | The job identifier.
    region = 'region_example' # str | Filters results based on the specified region. (optional)
    namespace = 'namespace_example' # str | Filters results based on the specified namespace. (optional)
    index = 56 # int | If set, wait until query exceeds given index. Must be provided with WaitParam. (optional)
    wait = 'wait_example' # str | Provided with IndexParam to wait for change. (optional)
    stale = 'stale_example' # str | If present, results will include stale reads. (optional)
    prefix = 'prefix_example' # str | Constrains results to jobs that start with the defined prefix (optional)
    x_nomad_token = 'x_nomad_token_example' # str | A Nomad ACL token. (optional)
    per_page = 56 # int | Maximum number of results to return. (optional)
    next_token = 'next_token_example' # str | Indicates where to start paging for queries that support pagination. (optional)

    try:
        api_response = await api_instance.get_job_scale_status(job_name, region=region, namespace=namespace, index=index, wait=wait, stale=stale, prefix=prefix, x_nomad_token=x_nomad_token, per_page=per_page, next_token=next_token)
        print("The response of JobsApi->get_job_scale_status:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling JobsApi->get_job_scale_status: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **job_name** | **str**| The job identifier. | 
 **region** | **str**| Filters results based on the specified region. | [optional] 
 **namespace** | **str**| Filters results based on the specified namespace. | [optional] 
 **index** | **int**| If set, wait until query exceeds given index. Must be provided with WaitParam. | [optional] 
 **wait** | **str**| Provided with IndexParam to wait for change. | [optional] 
 **stale** | **str**| If present, results will include stale reads. | [optional] 
 **prefix** | **str**| Constrains results to jobs that start with the defined prefix | [optional] 
 **x_nomad_token** | **str**| A Nomad ACL token. | [optional] 
 **per_page** | **int**| Maximum number of results to return. | [optional] 
 **next_token** | **str**| Indicates where to start paging for queries that support pagination. | [optional] 

### Return type

[**JobScaleStatusResponse**](JobScaleStatusResponse.md)

### Authorization

[X-Nomad-Token](../README.md#X-Nomad-Token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  * X-Nomad-Index - A unique identifier representing the current state of the requested resource. On a new Nomad cluster the value of this index starts at 1. <br>  * X-Nomad-KnownLeader - Boolean indicating if there is a known cluster leader. <br>  * X-Nomad-LastContact - The time in milliseconds that a server was last contacted by the leader node. <br>  |
**400** | Bad request |  -  |
**403** | Forbidden |  -  |
**405** | Method not allowed |  -  |
**500** | Internal server error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_job_summary**
> JobSummary get_job_summary(job_name, region=region, namespace=namespace, index=index, wait=wait, stale=stale, prefix=prefix, x_nomad_token=x_nomad_token, per_page=per_page, next_token=next_token)



### Example

* Api Key Authentication (X-Nomad-Token):
```python
import time
import os
import nomad_generated
from nomad_generated.models.job_summary import JobSummary
from nomad_generated.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://127.0.0.1:4646/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = nomad_generated.Configuration(
    host = "http://127.0.0.1:4646/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: X-Nomad-Token
configuration.api_key['X-Nomad-Token'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['X-Nomad-Token'] = 'Bearer'

# Enter a context with an instance of the API client
async with nomad_generated.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = nomad_generated.JobsApi(api_client)
    job_name = 'job_name_example' # str | The job identifier.
    region = 'region_example' # str | Filters results based on the specified region. (optional)
    namespace = 'namespace_example' # str | Filters results based on the specified namespace. (optional)
    index = 56 # int | If set, wait until query exceeds given index. Must be provided with WaitParam. (optional)
    wait = 'wait_example' # str | Provided with IndexParam to wait for change. (optional)
    stale = 'stale_example' # str | If present, results will include stale reads. (optional)
    prefix = 'prefix_example' # str | Constrains results to jobs that start with the defined prefix (optional)
    x_nomad_token = 'x_nomad_token_example' # str | A Nomad ACL token. (optional)
    per_page = 56 # int | Maximum number of results to return. (optional)
    next_token = 'next_token_example' # str | Indicates where to start paging for queries that support pagination. (optional)

    try:
        api_response = await api_instance.get_job_summary(job_name, region=region, namespace=namespace, index=index, wait=wait, stale=stale, prefix=prefix, x_nomad_token=x_nomad_token, per_page=per_page, next_token=next_token)
        print("The response of JobsApi->get_job_summary:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling JobsApi->get_job_summary: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **job_name** | **str**| The job identifier. | 
 **region** | **str**| Filters results based on the specified region. | [optional] 
 **namespace** | **str**| Filters results based on the specified namespace. | [optional] 
 **index** | **int**| If set, wait until query exceeds given index. Must be provided with WaitParam. | [optional] 
 **wait** | **str**| Provided with IndexParam to wait for change. | [optional] 
 **stale** | **str**| If present, results will include stale reads. | [optional] 
 **prefix** | **str**| Constrains results to jobs that start with the defined prefix | [optional] 
 **x_nomad_token** | **str**| A Nomad ACL token. | [optional] 
 **per_page** | **int**| Maximum number of results to return. | [optional] 
 **next_token** | **str**| Indicates where to start paging for queries that support pagination. | [optional] 

### Return type

[**JobSummary**](JobSummary.md)

### Authorization

[X-Nomad-Token](../README.md#X-Nomad-Token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  * X-Nomad-Index - A unique identifier representing the current state of the requested resource. On a new Nomad cluster the value of this index starts at 1. <br>  * X-Nomad-KnownLeader - Boolean indicating if there is a known cluster leader. <br>  * X-Nomad-LastContact - The time in milliseconds that a server was last contacted by the leader node. <br>  |
**400** | Bad request |  -  |
**403** | Forbidden |  -  |
**405** | Method not allowed |  -  |
**500** | Internal server error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_job_versions**
> JobVersionsResponse get_job_versions(job_name, region=region, namespace=namespace, index=index, wait=wait, stale=stale, prefix=prefix, x_nomad_token=x_nomad_token, per_page=per_page, next_token=next_token, diffs=diffs)



### Example

* Api Key Authentication (X-Nomad-Token):
```python
import time
import os
import nomad_generated
from nomad_generated.models.job_versions_response import JobVersionsResponse
from nomad_generated.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://127.0.0.1:4646/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = nomad_generated.Configuration(
    host = "http://127.0.0.1:4646/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: X-Nomad-Token
configuration.api_key['X-Nomad-Token'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['X-Nomad-Token'] = 'Bearer'

# Enter a context with an instance of the API client
async with nomad_generated.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = nomad_generated.JobsApi(api_client)
    job_name = 'job_name_example' # str | The job identifier.
    region = 'region_example' # str | Filters results based on the specified region. (optional)
    namespace = 'namespace_example' # str | Filters results based on the specified namespace. (optional)
    index = 56 # int | If set, wait until query exceeds given index. Must be provided with WaitParam. (optional)
    wait = 'wait_example' # str | Provided with IndexParam to wait for change. (optional)
    stale = 'stale_example' # str | If present, results will include stale reads. (optional)
    prefix = 'prefix_example' # str | Constrains results to jobs that start with the defined prefix (optional)
    x_nomad_token = 'x_nomad_token_example' # str | A Nomad ACL token. (optional)
    per_page = 56 # int | Maximum number of results to return. (optional)
    next_token = 'next_token_example' # str | Indicates where to start paging for queries that support pagination. (optional)
    diffs = True # bool | Boolean flag indicating whether to compute job diffs. (optional)

    try:
        api_response = await api_instance.get_job_versions(job_name, region=region, namespace=namespace, index=index, wait=wait, stale=stale, prefix=prefix, x_nomad_token=x_nomad_token, per_page=per_page, next_token=next_token, diffs=diffs)
        print("The response of JobsApi->get_job_versions:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling JobsApi->get_job_versions: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **job_name** | **str**| The job identifier. | 
 **region** | **str**| Filters results based on the specified region. | [optional] 
 **namespace** | **str**| Filters results based on the specified namespace. | [optional] 
 **index** | **int**| If set, wait until query exceeds given index. Must be provided with WaitParam. | [optional] 
 **wait** | **str**| Provided with IndexParam to wait for change. | [optional] 
 **stale** | **str**| If present, results will include stale reads. | [optional] 
 **prefix** | **str**| Constrains results to jobs that start with the defined prefix | [optional] 
 **x_nomad_token** | **str**| A Nomad ACL token. | [optional] 
 **per_page** | **int**| Maximum number of results to return. | [optional] 
 **next_token** | **str**| Indicates where to start paging for queries that support pagination. | [optional] 
 **diffs** | **bool**| Boolean flag indicating whether to compute job diffs. | [optional] 

### Return type

[**JobVersionsResponse**](JobVersionsResponse.md)

### Authorization

[X-Nomad-Token](../README.md#X-Nomad-Token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  * X-Nomad-Index - A unique identifier representing the current state of the requested resource. On a new Nomad cluster the value of this index starts at 1. <br>  * X-Nomad-KnownLeader - Boolean indicating if there is a known cluster leader. <br>  * X-Nomad-LastContact - The time in milliseconds that a server was last contacted by the leader node. <br>  |
**400** | Bad request |  -  |
**403** | Forbidden |  -  |
**405** | Method not allowed |  -  |
**500** | Internal server error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_jobs**
> List[JobListStub] get_jobs(region=region, namespace=namespace, index=index, wait=wait, stale=stale, prefix=prefix, x_nomad_token=x_nomad_token, per_page=per_page, next_token=next_token)



### Example

* Api Key Authentication (X-Nomad-Token):
```python
import time
import os
import nomad_generated
from nomad_generated.models.job_list_stub import JobListStub
from nomad_generated.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://127.0.0.1:4646/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = nomad_generated.Configuration(
    host = "http://127.0.0.1:4646/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: X-Nomad-Token
configuration.api_key['X-Nomad-Token'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['X-Nomad-Token'] = 'Bearer'

# Enter a context with an instance of the API client
async with nomad_generated.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = nomad_generated.JobsApi(api_client)
    region = 'region_example' # str | Filters results based on the specified region. (optional)
    namespace = 'namespace_example' # str | Filters results based on the specified namespace. (optional)
    index = 56 # int | If set, wait until query exceeds given index. Must be provided with WaitParam. (optional)
    wait = 'wait_example' # str | Provided with IndexParam to wait for change. (optional)
    stale = 'stale_example' # str | If present, results will include stale reads. (optional)
    prefix = 'prefix_example' # str | Constrains results to jobs that start with the defined prefix (optional)
    x_nomad_token = 'x_nomad_token_example' # str | A Nomad ACL token. (optional)
    per_page = 56 # int | Maximum number of results to return. (optional)
    next_token = 'next_token_example' # str | Indicates where to start paging for queries that support pagination. (optional)

    try:
        api_response = await api_instance.get_jobs(region=region, namespace=namespace, index=index, wait=wait, stale=stale, prefix=prefix, x_nomad_token=x_nomad_token, per_page=per_page, next_token=next_token)
        print("The response of JobsApi->get_jobs:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling JobsApi->get_jobs: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **region** | **str**| Filters results based on the specified region. | [optional] 
 **namespace** | **str**| Filters results based on the specified namespace. | [optional] 
 **index** | **int**| If set, wait until query exceeds given index. Must be provided with WaitParam. | [optional] 
 **wait** | **str**| Provided with IndexParam to wait for change. | [optional] 
 **stale** | **str**| If present, results will include stale reads. | [optional] 
 **prefix** | **str**| Constrains results to jobs that start with the defined prefix | [optional] 
 **x_nomad_token** | **str**| A Nomad ACL token. | [optional] 
 **per_page** | **int**| Maximum number of results to return. | [optional] 
 **next_token** | **str**| Indicates where to start paging for queries that support pagination. | [optional] 

### Return type

[**List[JobListStub]**](JobListStub.md)

### Authorization

[X-Nomad-Token](../README.md#X-Nomad-Token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  * X-Nomad-Index - A unique identifier representing the current state of the requested resource. On a new Nomad cluster the value of this index starts at 1. <br>  * X-Nomad-KnownLeader - Boolean indicating if there is a known cluster leader. <br>  * X-Nomad-LastContact - The time in milliseconds that a server was last contacted by the leader node. <br>  |
**400** | Bad request |  -  |
**403** | Forbidden |  -  |
**405** | Method not allowed |  -  |
**500** | Internal server error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_job**
> JobRegisterResponse post_job(job_name, job_register_request, region=region, namespace=namespace, x_nomad_token=x_nomad_token, idempotency_token=idempotency_token)



### Example

* Api Key Authentication (X-Nomad-Token):
```python
import time
import os
import nomad_generated
from nomad_generated.models.job_register_request import JobRegisterRequest
from nomad_generated.models.job_register_response import JobRegisterResponse
from nomad_generated.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://127.0.0.1:4646/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = nomad_generated.Configuration(
    host = "http://127.0.0.1:4646/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: X-Nomad-Token
configuration.api_key['X-Nomad-Token'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['X-Nomad-Token'] = 'Bearer'

# Enter a context with an instance of the API client
async with nomad_generated.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = nomad_generated.JobsApi(api_client)
    job_name = 'job_name_example' # str | The job identifier.
    job_register_request = nomad_generated.JobRegisterRequest() # JobRegisterRequest | 
    region = 'region_example' # str | Filters results based on the specified region. (optional)
    namespace = 'namespace_example' # str | Filters results based on the specified namespace. (optional)
    x_nomad_token = 'x_nomad_token_example' # str | A Nomad ACL token. (optional)
    idempotency_token = 'idempotency_token_example' # str | Can be used to ensure operations are only run once. (optional)

    try:
        api_response = await api_instance.post_job(job_name, job_register_request, region=region, namespace=namespace, x_nomad_token=x_nomad_token, idempotency_token=idempotency_token)
        print("The response of JobsApi->post_job:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling JobsApi->post_job: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **job_name** | **str**| The job identifier. | 
 **job_register_request** | [**JobRegisterRequest**](JobRegisterRequest.md)|  | 
 **region** | **str**| Filters results based on the specified region. | [optional] 
 **namespace** | **str**| Filters results based on the specified namespace. | [optional] 
 **x_nomad_token** | **str**| A Nomad ACL token. | [optional] 
 **idempotency_token** | **str**| Can be used to ensure operations are only run once. | [optional] 

### Return type

[**JobRegisterResponse**](JobRegisterResponse.md)

### Authorization

[X-Nomad-Token](../README.md#X-Nomad-Token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  * X-Nomad-Index - A unique identifier representing the current state of the requested resource. On a new Nomad cluster the value of this index starts at 1. <br>  * X-Nomad-KnownLeader - Boolean indicating if there is a known cluster leader. <br>  * X-Nomad-LastContact - The time in milliseconds that a server was last contacted by the leader node. <br>  |
**400** | Bad request |  -  |
**403** | Forbidden |  -  |
**405** | Method not allowed |  -  |
**500** | Internal server error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_job_dispatch**
> JobDispatchResponse post_job_dispatch(job_name, job_dispatch_request, region=region, namespace=namespace, x_nomad_token=x_nomad_token, idempotency_token=idempotency_token)



### Example

* Api Key Authentication (X-Nomad-Token):
```python
import time
import os
import nomad_generated
from nomad_generated.models.job_dispatch_request import JobDispatchRequest
from nomad_generated.models.job_dispatch_response import JobDispatchResponse
from nomad_generated.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://127.0.0.1:4646/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = nomad_generated.Configuration(
    host = "http://127.0.0.1:4646/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: X-Nomad-Token
configuration.api_key['X-Nomad-Token'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['X-Nomad-Token'] = 'Bearer'

# Enter a context with an instance of the API client
async with nomad_generated.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = nomad_generated.JobsApi(api_client)
    job_name = 'job_name_example' # str | The job identifier.
    job_dispatch_request = nomad_generated.JobDispatchRequest() # JobDispatchRequest | 
    region = 'region_example' # str | Filters results based on the specified region. (optional)
    namespace = 'namespace_example' # str | Filters results based on the specified namespace. (optional)
    x_nomad_token = 'x_nomad_token_example' # str | A Nomad ACL token. (optional)
    idempotency_token = 'idempotency_token_example' # str | Can be used to ensure operations are only run once. (optional)

    try:
        api_response = await api_instance.post_job_dispatch(job_name, job_dispatch_request, region=region, namespace=namespace, x_nomad_token=x_nomad_token, idempotency_token=idempotency_token)
        print("The response of JobsApi->post_job_dispatch:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling JobsApi->post_job_dispatch: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **job_name** | **str**| The job identifier. | 
 **job_dispatch_request** | [**JobDispatchRequest**](JobDispatchRequest.md)|  | 
 **region** | **str**| Filters results based on the specified region. | [optional] 
 **namespace** | **str**| Filters results based on the specified namespace. | [optional] 
 **x_nomad_token** | **str**| A Nomad ACL token. | [optional] 
 **idempotency_token** | **str**| Can be used to ensure operations are only run once. | [optional] 

### Return type

[**JobDispatchResponse**](JobDispatchResponse.md)

### Authorization

[X-Nomad-Token](../README.md#X-Nomad-Token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  * X-Nomad-Index - A unique identifier representing the current state of the requested resource. On a new Nomad cluster the value of this index starts at 1. <br>  |
**400** | Bad request |  -  |
**403** | Forbidden |  -  |
**405** | Method not allowed |  -  |
**500** | Internal server error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_job_evaluate**
> JobRegisterResponse post_job_evaluate(job_name, job_evaluate_request, region=region, namespace=namespace, x_nomad_token=x_nomad_token, idempotency_token=idempotency_token)



### Example

* Api Key Authentication (X-Nomad-Token):
```python
import time
import os
import nomad_generated
from nomad_generated.models.job_evaluate_request import JobEvaluateRequest
from nomad_generated.models.job_register_response import JobRegisterResponse
from nomad_generated.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://127.0.0.1:4646/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = nomad_generated.Configuration(
    host = "http://127.0.0.1:4646/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: X-Nomad-Token
configuration.api_key['X-Nomad-Token'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['X-Nomad-Token'] = 'Bearer'

# Enter a context with an instance of the API client
async with nomad_generated.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = nomad_generated.JobsApi(api_client)
    job_name = 'job_name_example' # str | The job identifier.
    job_evaluate_request = nomad_generated.JobEvaluateRequest() # JobEvaluateRequest | 
    region = 'region_example' # str | Filters results based on the specified region. (optional)
    namespace = 'namespace_example' # str | Filters results based on the specified namespace. (optional)
    x_nomad_token = 'x_nomad_token_example' # str | A Nomad ACL token. (optional)
    idempotency_token = 'idempotency_token_example' # str | Can be used to ensure operations are only run once. (optional)

    try:
        api_response = await api_instance.post_job_evaluate(job_name, job_evaluate_request, region=region, namespace=namespace, x_nomad_token=x_nomad_token, idempotency_token=idempotency_token)
        print("The response of JobsApi->post_job_evaluate:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling JobsApi->post_job_evaluate: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **job_name** | **str**| The job identifier. | 
 **job_evaluate_request** | [**JobEvaluateRequest**](JobEvaluateRequest.md)|  | 
 **region** | **str**| Filters results based on the specified region. | [optional] 
 **namespace** | **str**| Filters results based on the specified namespace. | [optional] 
 **x_nomad_token** | **str**| A Nomad ACL token. | [optional] 
 **idempotency_token** | **str**| Can be used to ensure operations are only run once. | [optional] 

### Return type

[**JobRegisterResponse**](JobRegisterResponse.md)

### Authorization

[X-Nomad-Token](../README.md#X-Nomad-Token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  * X-Nomad-Index - A unique identifier representing the current state of the requested resource. On a new Nomad cluster the value of this index starts at 1. <br>  |
**400** | Bad request |  -  |
**403** | Forbidden |  -  |
**405** | Method not allowed |  -  |
**500** | Internal server error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_job_parse**
> Job post_job_parse(jobs_parse_request)



### Example

* Api Key Authentication (X-Nomad-Token):
```python
import time
import os
import nomad_generated
from nomad_generated.models.job import Job
from nomad_generated.models.jobs_parse_request import JobsParseRequest
from nomad_generated.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://127.0.0.1:4646/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = nomad_generated.Configuration(
    host = "http://127.0.0.1:4646/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: X-Nomad-Token
configuration.api_key['X-Nomad-Token'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['X-Nomad-Token'] = 'Bearer'

# Enter a context with an instance of the API client
async with nomad_generated.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = nomad_generated.JobsApi(api_client)
    jobs_parse_request = nomad_generated.JobsParseRequest() # JobsParseRequest | 

    try:
        api_response = await api_instance.post_job_parse(jobs_parse_request)
        print("The response of JobsApi->post_job_parse:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling JobsApi->post_job_parse: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **jobs_parse_request** | [**JobsParseRequest**](JobsParseRequest.md)|  | 

### Return type

[**Job**](Job.md)

### Authorization

[X-Nomad-Token](../README.md#X-Nomad-Token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |
**400** | Bad request |  -  |
**403** | Forbidden |  -  |
**405** | Method not allowed |  -  |
**500** | Internal server error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_job_periodic_force**
> PeriodicForceResponse post_job_periodic_force(job_name, region=region, namespace=namespace, x_nomad_token=x_nomad_token, idempotency_token=idempotency_token)



### Example

* Api Key Authentication (X-Nomad-Token):
```python
import time
import os
import nomad_generated
from nomad_generated.models.periodic_force_response import PeriodicForceResponse
from nomad_generated.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://127.0.0.1:4646/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = nomad_generated.Configuration(
    host = "http://127.0.0.1:4646/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: X-Nomad-Token
configuration.api_key['X-Nomad-Token'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['X-Nomad-Token'] = 'Bearer'

# Enter a context with an instance of the API client
async with nomad_generated.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = nomad_generated.JobsApi(api_client)
    job_name = 'job_name_example' # str | The job identifier.
    region = 'region_example' # str | Filters results based on the specified region. (optional)
    namespace = 'namespace_example' # str | Filters results based on the specified namespace. (optional)
    x_nomad_token = 'x_nomad_token_example' # str | A Nomad ACL token. (optional)
    idempotency_token = 'idempotency_token_example' # str | Can be used to ensure operations are only run once. (optional)

    try:
        api_response = await api_instance.post_job_periodic_force(job_name, region=region, namespace=namespace, x_nomad_token=x_nomad_token, idempotency_token=idempotency_token)
        print("The response of JobsApi->post_job_periodic_force:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling JobsApi->post_job_periodic_force: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **job_name** | **str**| The job identifier. | 
 **region** | **str**| Filters results based on the specified region. | [optional] 
 **namespace** | **str**| Filters results based on the specified namespace. | [optional] 
 **x_nomad_token** | **str**| A Nomad ACL token. | [optional] 
 **idempotency_token** | **str**| Can be used to ensure operations are only run once. | [optional] 

### Return type

[**PeriodicForceResponse**](PeriodicForceResponse.md)

### Authorization

[X-Nomad-Token](../README.md#X-Nomad-Token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  * X-Nomad-Index - A unique identifier representing the current state of the requested resource. On a new Nomad cluster the value of this index starts at 1. <br>  |
**400** | Bad request |  -  |
**403** | Forbidden |  -  |
**405** | Method not allowed |  -  |
**500** | Internal server error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_job_plan**
> JobPlanResponse post_job_plan(job_name, job_plan_request, region=region, namespace=namespace, x_nomad_token=x_nomad_token, idempotency_token=idempotency_token)



### Example

* Api Key Authentication (X-Nomad-Token):
```python
import time
import os
import nomad_generated
from nomad_generated.models.job_plan_request import JobPlanRequest
from nomad_generated.models.job_plan_response import JobPlanResponse
from nomad_generated.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://127.0.0.1:4646/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = nomad_generated.Configuration(
    host = "http://127.0.0.1:4646/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: X-Nomad-Token
configuration.api_key['X-Nomad-Token'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['X-Nomad-Token'] = 'Bearer'

# Enter a context with an instance of the API client
async with nomad_generated.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = nomad_generated.JobsApi(api_client)
    job_name = 'job_name_example' # str | The job identifier.
    job_plan_request = nomad_generated.JobPlanRequest() # JobPlanRequest | 
    region = 'region_example' # str | Filters results based on the specified region. (optional)
    namespace = 'namespace_example' # str | Filters results based on the specified namespace. (optional)
    x_nomad_token = 'x_nomad_token_example' # str | A Nomad ACL token. (optional)
    idempotency_token = 'idempotency_token_example' # str | Can be used to ensure operations are only run once. (optional)

    try:
        api_response = await api_instance.post_job_plan(job_name, job_plan_request, region=region, namespace=namespace, x_nomad_token=x_nomad_token, idempotency_token=idempotency_token)
        print("The response of JobsApi->post_job_plan:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling JobsApi->post_job_plan: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **job_name** | **str**| The job identifier. | 
 **job_plan_request** | [**JobPlanRequest**](JobPlanRequest.md)|  | 
 **region** | **str**| Filters results based on the specified region. | [optional] 
 **namespace** | **str**| Filters results based on the specified namespace. | [optional] 
 **x_nomad_token** | **str**| A Nomad ACL token. | [optional] 
 **idempotency_token** | **str**| Can be used to ensure operations are only run once. | [optional] 

### Return type

[**JobPlanResponse**](JobPlanResponse.md)

### Authorization

[X-Nomad-Token](../README.md#X-Nomad-Token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  * X-Nomad-Index - A unique identifier representing the current state of the requested resource. On a new Nomad cluster the value of this index starts at 1. <br>  |
**400** | Bad request |  -  |
**403** | Forbidden |  -  |
**405** | Method not allowed |  -  |
**500** | Internal server error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_job_revert**
> JobRegisterResponse post_job_revert(job_name, job_revert_request, region=region, namespace=namespace, x_nomad_token=x_nomad_token, idempotency_token=idempotency_token)



### Example

* Api Key Authentication (X-Nomad-Token):
```python
import time
import os
import nomad_generated
from nomad_generated.models.job_register_response import JobRegisterResponse
from nomad_generated.models.job_revert_request import JobRevertRequest
from nomad_generated.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://127.0.0.1:4646/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = nomad_generated.Configuration(
    host = "http://127.0.0.1:4646/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: X-Nomad-Token
configuration.api_key['X-Nomad-Token'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['X-Nomad-Token'] = 'Bearer'

# Enter a context with an instance of the API client
async with nomad_generated.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = nomad_generated.JobsApi(api_client)
    job_name = 'job_name_example' # str | The job identifier.
    job_revert_request = nomad_generated.JobRevertRequest() # JobRevertRequest | 
    region = 'region_example' # str | Filters results based on the specified region. (optional)
    namespace = 'namespace_example' # str | Filters results based on the specified namespace. (optional)
    x_nomad_token = 'x_nomad_token_example' # str | A Nomad ACL token. (optional)
    idempotency_token = 'idempotency_token_example' # str | Can be used to ensure operations are only run once. (optional)

    try:
        api_response = await api_instance.post_job_revert(job_name, job_revert_request, region=region, namespace=namespace, x_nomad_token=x_nomad_token, idempotency_token=idempotency_token)
        print("The response of JobsApi->post_job_revert:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling JobsApi->post_job_revert: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **job_name** | **str**| The job identifier. | 
 **job_revert_request** | [**JobRevertRequest**](JobRevertRequest.md)|  | 
 **region** | **str**| Filters results based on the specified region. | [optional] 
 **namespace** | **str**| Filters results based on the specified namespace. | [optional] 
 **x_nomad_token** | **str**| A Nomad ACL token. | [optional] 
 **idempotency_token** | **str**| Can be used to ensure operations are only run once. | [optional] 

### Return type

[**JobRegisterResponse**](JobRegisterResponse.md)

### Authorization

[X-Nomad-Token](../README.md#X-Nomad-Token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  * X-Nomad-Index - A unique identifier representing the current state of the requested resource. On a new Nomad cluster the value of this index starts at 1. <br>  |
**400** | Bad request |  -  |
**403** | Forbidden |  -  |
**405** | Method not allowed |  -  |
**500** | Internal server error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_job_scaling_request**
> JobRegisterResponse post_job_scaling_request(job_name, scaling_request, region=region, namespace=namespace, x_nomad_token=x_nomad_token, idempotency_token=idempotency_token)



### Example

* Api Key Authentication (X-Nomad-Token):
```python
import time
import os
import nomad_generated
from nomad_generated.models.job_register_response import JobRegisterResponse
from nomad_generated.models.scaling_request import ScalingRequest
from nomad_generated.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://127.0.0.1:4646/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = nomad_generated.Configuration(
    host = "http://127.0.0.1:4646/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: X-Nomad-Token
configuration.api_key['X-Nomad-Token'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['X-Nomad-Token'] = 'Bearer'

# Enter a context with an instance of the API client
async with nomad_generated.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = nomad_generated.JobsApi(api_client)
    job_name = 'job_name_example' # str | The job identifier.
    scaling_request = nomad_generated.ScalingRequest() # ScalingRequest | 
    region = 'region_example' # str | Filters results based on the specified region. (optional)
    namespace = 'namespace_example' # str | Filters results based on the specified namespace. (optional)
    x_nomad_token = 'x_nomad_token_example' # str | A Nomad ACL token. (optional)
    idempotency_token = 'idempotency_token_example' # str | Can be used to ensure operations are only run once. (optional)

    try:
        api_response = await api_instance.post_job_scaling_request(job_name, scaling_request, region=region, namespace=namespace, x_nomad_token=x_nomad_token, idempotency_token=idempotency_token)
        print("The response of JobsApi->post_job_scaling_request:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling JobsApi->post_job_scaling_request: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **job_name** | **str**| The job identifier. | 
 **scaling_request** | [**ScalingRequest**](ScalingRequest.md)|  | 
 **region** | **str**| Filters results based on the specified region. | [optional] 
 **namespace** | **str**| Filters results based on the specified namespace. | [optional] 
 **x_nomad_token** | **str**| A Nomad ACL token. | [optional] 
 **idempotency_token** | **str**| Can be used to ensure operations are only run once. | [optional] 

### Return type

[**JobRegisterResponse**](JobRegisterResponse.md)

### Authorization

[X-Nomad-Token](../README.md#X-Nomad-Token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  * X-Nomad-Index - A unique identifier representing the current state of the requested resource. On a new Nomad cluster the value of this index starts at 1. <br>  |
**400** | Bad request |  -  |
**403** | Forbidden |  -  |
**405** | Method not allowed |  -  |
**500** | Internal server error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_job_stability**
> JobStabilityResponse post_job_stability(job_name, job_stability_request, region=region, namespace=namespace, x_nomad_token=x_nomad_token, idempotency_token=idempotency_token)



### Example

* Api Key Authentication (X-Nomad-Token):
```python
import time
import os
import nomad_generated
from nomad_generated.models.job_stability_request import JobStabilityRequest
from nomad_generated.models.job_stability_response import JobStabilityResponse
from nomad_generated.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://127.0.0.1:4646/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = nomad_generated.Configuration(
    host = "http://127.0.0.1:4646/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: X-Nomad-Token
configuration.api_key['X-Nomad-Token'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['X-Nomad-Token'] = 'Bearer'

# Enter a context with an instance of the API client
async with nomad_generated.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = nomad_generated.JobsApi(api_client)
    job_name = 'job_name_example' # str | The job identifier.
    job_stability_request = nomad_generated.JobStabilityRequest() # JobStabilityRequest | 
    region = 'region_example' # str | Filters results based on the specified region. (optional)
    namespace = 'namespace_example' # str | Filters results based on the specified namespace. (optional)
    x_nomad_token = 'x_nomad_token_example' # str | A Nomad ACL token. (optional)
    idempotency_token = 'idempotency_token_example' # str | Can be used to ensure operations are only run once. (optional)

    try:
        api_response = await api_instance.post_job_stability(job_name, job_stability_request, region=region, namespace=namespace, x_nomad_token=x_nomad_token, idempotency_token=idempotency_token)
        print("The response of JobsApi->post_job_stability:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling JobsApi->post_job_stability: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **job_name** | **str**| The job identifier. | 
 **job_stability_request** | [**JobStabilityRequest**](JobStabilityRequest.md)|  | 
 **region** | **str**| Filters results based on the specified region. | [optional] 
 **namespace** | **str**| Filters results based on the specified namespace. | [optional] 
 **x_nomad_token** | **str**| A Nomad ACL token. | [optional] 
 **idempotency_token** | **str**| Can be used to ensure operations are only run once. | [optional] 

### Return type

[**JobStabilityResponse**](JobStabilityResponse.md)

### Authorization

[X-Nomad-Token](../README.md#X-Nomad-Token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  * X-Nomad-Index - A unique identifier representing the current state of the requested resource. On a new Nomad cluster the value of this index starts at 1. <br>  |
**400** | Bad request |  -  |
**403** | Forbidden |  -  |
**405** | Method not allowed |  -  |
**500** | Internal server error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_job_validate_request**
> JobValidateResponse post_job_validate_request(job_validate_request, region=region, namespace=namespace, x_nomad_token=x_nomad_token, idempotency_token=idempotency_token)



### Example

* Api Key Authentication (X-Nomad-Token):
```python
import time
import os
import nomad_generated
from nomad_generated.models.job_validate_request import JobValidateRequest
from nomad_generated.models.job_validate_response import JobValidateResponse
from nomad_generated.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://127.0.0.1:4646/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = nomad_generated.Configuration(
    host = "http://127.0.0.1:4646/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: X-Nomad-Token
configuration.api_key['X-Nomad-Token'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['X-Nomad-Token'] = 'Bearer'

# Enter a context with an instance of the API client
async with nomad_generated.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = nomad_generated.JobsApi(api_client)
    job_validate_request = nomad_generated.JobValidateRequest() # JobValidateRequest | 
    region = 'region_example' # str | Filters results based on the specified region. (optional)
    namespace = 'namespace_example' # str | Filters results based on the specified namespace. (optional)
    x_nomad_token = 'x_nomad_token_example' # str | A Nomad ACL token. (optional)
    idempotency_token = 'idempotency_token_example' # str | Can be used to ensure operations are only run once. (optional)

    try:
        api_response = await api_instance.post_job_validate_request(job_validate_request, region=region, namespace=namespace, x_nomad_token=x_nomad_token, idempotency_token=idempotency_token)
        print("The response of JobsApi->post_job_validate_request:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling JobsApi->post_job_validate_request: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **job_validate_request** | [**JobValidateRequest**](JobValidateRequest.md)|  | 
 **region** | **str**| Filters results based on the specified region. | [optional] 
 **namespace** | **str**| Filters results based on the specified namespace. | [optional] 
 **x_nomad_token** | **str**| A Nomad ACL token. | [optional] 
 **idempotency_token** | **str**| Can be used to ensure operations are only run once. | [optional] 

### Return type

[**JobValidateResponse**](JobValidateResponse.md)

### Authorization

[X-Nomad-Token](../README.md#X-Nomad-Token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  * X-Nomad-Index - A unique identifier representing the current state of the requested resource. On a new Nomad cluster the value of this index starts at 1. <br>  |
**400** | Bad request |  -  |
**403** | Forbidden |  -  |
**405** | Method not allowed |  -  |
**500** | Internal server error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **register_job**
> JobRegisterResponse register_job(job_register_request, region=region, namespace=namespace, x_nomad_token=x_nomad_token, idempotency_token=idempotency_token)



### Example

* Api Key Authentication (X-Nomad-Token):
```python
import time
import os
import nomad_generated
from nomad_generated.models.job_register_request import JobRegisterRequest
from nomad_generated.models.job_register_response import JobRegisterResponse
from nomad_generated.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://127.0.0.1:4646/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = nomad_generated.Configuration(
    host = "http://127.0.0.1:4646/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: X-Nomad-Token
configuration.api_key['X-Nomad-Token'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['X-Nomad-Token'] = 'Bearer'

# Enter a context with an instance of the API client
async with nomad_generated.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = nomad_generated.JobsApi(api_client)
    job_register_request = nomad_generated.JobRegisterRequest() # JobRegisterRequest | 
    region = 'region_example' # str | Filters results based on the specified region. (optional)
    namespace = 'namespace_example' # str | Filters results based on the specified namespace. (optional)
    x_nomad_token = 'x_nomad_token_example' # str | A Nomad ACL token. (optional)
    idempotency_token = 'idempotency_token_example' # str | Can be used to ensure operations are only run once. (optional)

    try:
        api_response = await api_instance.register_job(job_register_request, region=region, namespace=namespace, x_nomad_token=x_nomad_token, idempotency_token=idempotency_token)
        print("The response of JobsApi->register_job:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling JobsApi->register_job: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **job_register_request** | [**JobRegisterRequest**](JobRegisterRequest.md)|  | 
 **region** | **str**| Filters results based on the specified region. | [optional] 
 **namespace** | **str**| Filters results based on the specified namespace. | [optional] 
 **x_nomad_token** | **str**| A Nomad ACL token. | [optional] 
 **idempotency_token** | **str**| Can be used to ensure operations are only run once. | [optional] 

### Return type

[**JobRegisterResponse**](JobRegisterResponse.md)

### Authorization

[X-Nomad-Token](../README.md#X-Nomad-Token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  * X-Nomad-Index - A unique identifier representing the current state of the requested resource. On a new Nomad cluster the value of this index starts at 1. <br>  * X-Nomad-KnownLeader - Boolean indicating if there is a known cluster leader. <br>  * X-Nomad-LastContact - The time in milliseconds that a server was last contacted by the leader node. <br>  |
**400** | Bad request |  -  |
**403** | Forbidden |  -  |
**405** | Method not allowed |  -  |
**500** | Internal server error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

