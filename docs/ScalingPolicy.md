# ScalingPolicy


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**create_index** | **int** |  | [optional] 
**enabled** | **bool** |  | [optional] 
**id** | **str** |  | [optional] 
**max** | **int** |  | [optional] 
**min** | **int** |  | [optional] 
**modify_index** | **int** |  | [optional] 
**namespace** | **str** |  | [optional] 
**policy** | **Dict[str, object]** |  | [optional] 
**target** | **Dict[str, str]** |  | [optional] 
**type** | **str** |  | [optional] 

## Example

```python
from nomad_generated.models.scaling_policy import ScalingPolicy

# TODO update the JSON string below
json = "{}"
# create an instance of ScalingPolicy from a JSON string
scaling_policy_instance = ScalingPolicy.from_json(json)
# print the JSON string representation of the object
print ScalingPolicy.to_json()

# convert the object into a dict
scaling_policy_dict = scaling_policy_instance.to_dict()
# create an instance of ScalingPolicy from a dict
scaling_policy_form_dict = scaling_policy.from_dict(scaling_policy_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


