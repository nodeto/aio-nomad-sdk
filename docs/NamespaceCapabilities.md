# NamespaceCapabilities


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**disabled_task_drivers** | **List[str]** |  | [optional] 
**enabled_task_drivers** | **List[str]** |  | [optional] 

## Example

```python
from nomad_generated.models.namespace_capabilities import NamespaceCapabilities

# TODO update the JSON string below
json = "{}"
# create an instance of NamespaceCapabilities from a JSON string
namespace_capabilities_instance = NamespaceCapabilities.from_json(json)
# print the JSON string representation of the object
print NamespaceCapabilities.to_json()

# convert the object into a dict
namespace_capabilities_dict = namespace_capabilities_instance.to_dict()
# create an instance of NamespaceCapabilities from a dict
namespace_capabilities_form_dict = namespace_capabilities.from_dict(namespace_capabilities_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


