# JobSummary


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**children** | [**JobChildrenSummary**](JobChildrenSummary.md) |  | [optional] 
**create_index** | **int** |  | [optional] 
**job_id** | **str** |  | [optional] 
**modify_index** | **int** |  | [optional] 
**namespace** | **str** |  | [optional] 
**summary** | [**Dict[str, TaskGroupSummary]**](TaskGroupSummary.md) |  | [optional] 

## Example

```python
from nomad_generated.models.job_summary import JobSummary

# TODO update the JSON string below
json = "{}"
# create an instance of JobSummary from a JSON string
job_summary_instance = JobSummary.from_json(json)
# print the JSON string representation of the object
print JobSummary.to_json()

# convert the object into a dict
job_summary_dict = job_summary_instance.to_dict()
# create an instance of JobSummary from a dict
job_summary_form_dict = job_summary.from_dict(job_summary_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


