# NodeUpdateDrainRequest


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**drain_spec** | [**DrainSpec**](DrainSpec.md) |  | [optional] 
**mark_eligible** | **bool** |  | [optional] 
**meta** | **Dict[str, str]** |  | [optional] 
**node_id** | **str** |  | [optional] 

## Example

```python
from nomad_generated.models.node_update_drain_request import NodeUpdateDrainRequest

# TODO update the JSON string below
json = "{}"
# create an instance of NodeUpdateDrainRequest from a JSON string
node_update_drain_request_instance = NodeUpdateDrainRequest.from_json(json)
# print the JSON string representation of the object
print NodeUpdateDrainRequest.to_json()

# convert the object into a dict
node_update_drain_request_dict = node_update_drain_request_instance.to_dict()
# create an instance of NodeUpdateDrainRequest from a dict
node_update_drain_request_form_dict = node_update_drain_request.from_dict(node_update_drain_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


