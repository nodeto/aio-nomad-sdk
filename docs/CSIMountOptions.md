# CSIMountOptions


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**fs_type** | **str** |  | [optional] 
**mount_flags** | **List[str]** |  | [optional] 

## Example

```python
from nomad_generated.models.csi_mount_options import CSIMountOptions

# TODO update the JSON string below
json = "{}"
# create an instance of CSIMountOptions from a JSON string
csi_mount_options_instance = CSIMountOptions.from_json(json)
# print the JSON string representation of the object
print CSIMountOptions.to_json()

# convert the object into a dict
csi_mount_options_dict = csi_mount_options_instance.to_dict()
# create an instance of CSIMountOptions from a dict
csi_mount_options_form_dict = csi_mount_options.from_dict(csi_mount_options_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


