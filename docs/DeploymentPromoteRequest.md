# DeploymentPromoteRequest


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**all** | **bool** |  | [optional] 
**deployment_id** | **str** |  | [optional] 
**groups** | **List[str]** |  | [optional] 
**namespace** | **str** |  | [optional] 
**region** | **str** |  | [optional] 
**secret_id** | **str** |  | [optional] 

## Example

```python
from nomad_generated.models.deployment_promote_request import DeploymentPromoteRequest

# TODO update the JSON string below
json = "{}"
# create an instance of DeploymentPromoteRequest from a JSON string
deployment_promote_request_instance = DeploymentPromoteRequest.from_json(json)
# print the JSON string representation of the object
print DeploymentPromoteRequest.to_json()

# convert the object into a dict
deployment_promote_request_dict = deployment_promote_request_instance.to_dict()
# create an instance of DeploymentPromoteRequest from a dict
deployment_promote_request_form_dict = deployment_promote_request.from_dict(deployment_promote_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


