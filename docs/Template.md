# Template


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**change_mode** | **str** |  | [optional] 
**change_script** | [**ChangeScript**](ChangeScript.md) |  | [optional] 
**change_signal** | **str** |  | [optional] 
**dest_path** | **str** |  | [optional] 
**embedded_tmpl** | **str** |  | [optional] 
**envvars** | **bool** |  | [optional] 
**gid** | **int** |  | [optional] 
**left_delim** | **str** |  | [optional] 
**perms** | **str** |  | [optional] 
**right_delim** | **str** |  | [optional] 
**source_path** | **str** |  | [optional] 
**splay** | **int** |  | [optional] 
**uid** | **int** |  | [optional] 
**vault_grace** | **int** |  | [optional] 
**wait** | [**WaitConfig**](WaitConfig.md) |  | [optional] 

## Example

```python
from nomad_generated.models.template import Template

# TODO update the JSON string below
json = "{}"
# create an instance of Template from a JSON string
template_instance = Template.from_json(json)
# print the JSON string representation of the object
print Template.to_json()

# convert the object into a dict
template_dict = template_instance.to_dict()
# create an instance of Template from a dict
template_form_dict = template.from_dict(template_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


