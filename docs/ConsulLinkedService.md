# ConsulLinkedService


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ca_file** | **str** |  | [optional] 
**cert_file** | **str** |  | [optional] 
**key_file** | **str** |  | [optional] 
**name** | **str** |  | [optional] 
**sni** | **str** |  | [optional] 

## Example

```python
from nomad_generated.models.consul_linked_service import ConsulLinkedService

# TODO update the JSON string below
json = "{}"
# create an instance of ConsulLinkedService from a JSON string
consul_linked_service_instance = ConsulLinkedService.from_json(json)
# print the JSON string representation of the object
print ConsulLinkedService.to_json()

# convert the object into a dict
consul_linked_service_dict = consul_linked_service_instance.to_dict()
# create an instance of ConsulLinkedService from a dict
consul_linked_service_form_dict = consul_linked_service.from_dict(consul_linked_service_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


