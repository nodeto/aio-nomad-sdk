# NodeDrainUpdateResponse


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**eval_create_index** | **int** |  | [optional] 
**eval_ids** | **List[str]** |  | [optional] 
**last_index** | **int** |  | [optional] 
**node_modify_index** | **int** |  | [optional] 
**request_time** | **int** |  | [optional] 

## Example

```python
from nomad_generated.models.node_drain_update_response import NodeDrainUpdateResponse

# TODO update the JSON string below
json = "{}"
# create an instance of NodeDrainUpdateResponse from a JSON string
node_drain_update_response_instance = NodeDrainUpdateResponse.from_json(json)
# print the JSON string representation of the object
print NodeDrainUpdateResponse.to_json()

# convert the object into a dict
node_drain_update_response_dict = node_drain_update_response_instance.to_dict()
# create an instance of NodeDrainUpdateResponse from a dict
node_drain_update_response_form_dict = node_drain_update_response.from_dict(node_drain_update_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


