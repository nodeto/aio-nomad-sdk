# AutopilotConfiguration


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cleanup_dead_servers** | **bool** |  | [optional] 
**create_index** | **int** |  | [optional] 
**disable_upgrade_migration** | **bool** |  | [optional] 
**enable_custom_upgrades** | **bool** |  | [optional] 
**enable_redundancy_zones** | **bool** |  | [optional] 
**last_contact_threshold** | **str** |  | [optional] 
**max_trailing_logs** | **int** |  | [optional] 
**min_quorum** | **int** |  | [optional] 
**modify_index** | **int** |  | [optional] 
**server_stabilization_time** | **str** |  | [optional] 

## Example

```python
from nomad_generated.models.autopilot_configuration import AutopilotConfiguration

# TODO update the JSON string below
json = "{}"
# create an instance of AutopilotConfiguration from a JSON string
autopilot_configuration_instance = AutopilotConfiguration.from_json(json)
# print the JSON string representation of the object
print AutopilotConfiguration.to_json()

# convert the object into a dict
autopilot_configuration_dict = autopilot_configuration_instance.to_dict()
# create an instance of AutopilotConfiguration from a dict
autopilot_configuration_form_dict = autopilot_configuration.from_dict(autopilot_configuration_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


