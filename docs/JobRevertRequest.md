# JobRevertRequest


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**consul_token** | **str** |  | [optional] 
**enforce_prior_version** | **int** |  | [optional] 
**job_id** | **str** |  | [optional] 
**job_version** | **int** |  | [optional] 
**namespace** | **str** |  | [optional] 
**region** | **str** |  | [optional] 
**secret_id** | **str** |  | [optional] 
**vault_token** | **str** |  | [optional] 

## Example

```python
from nomad_generated.models.job_revert_request import JobRevertRequest

# TODO update the JSON string below
json = "{}"
# create an instance of JobRevertRequest from a JSON string
job_revert_request_instance = JobRevertRequest.from_json(json)
# print the JSON string representation of the object
print JobRevertRequest.to_json()

# convert the object into a dict
job_revert_request_dict = job_revert_request_instance.to_dict()
# create an instance of JobRevertRequest from a dict
job_revert_request_form_dict = job_revert_request.from_dict(job_revert_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


