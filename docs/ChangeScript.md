# ChangeScript


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**args** | **List[str]** |  | [optional] 
**command** | **str** |  | [optional] 
**fail_on_error** | **bool** |  | [optional] 
**timeout** | **int** |  | [optional] 

## Example

```python
from nomad_generated.models.change_script import ChangeScript

# TODO update the JSON string below
json = "{}"
# create an instance of ChangeScript from a JSON string
change_script_instance = ChangeScript.from_json(json)
# print the JSON string representation of the object
print ChangeScript.to_json()

# convert the object into a dict
change_script_dict = change_script_instance.to_dict()
# create an instance of ChangeScript from a dict
change_script_form_dict = change_script.from_dict(change_script_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


