# DesiredUpdates


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**canary** | **int** |  | [optional] 
**destructive_update** | **int** |  | [optional] 
**ignore** | **int** |  | [optional] 
**in_place_update** | **int** |  | [optional] 
**migrate** | **int** |  | [optional] 
**place** | **int** |  | [optional] 
**preemptions** | **int** |  | [optional] 
**stop** | **int** |  | [optional] 

## Example

```python
from nomad_generated.models.desired_updates import DesiredUpdates

# TODO update the JSON string below
json = "{}"
# create an instance of DesiredUpdates from a JSON string
desired_updates_instance = DesiredUpdates.from_json(json)
# print the JSON string representation of the object
print DesiredUpdates.to_json()

# convert the object into a dict
desired_updates_dict = desired_updates_instance.to_dict()
# create an instance of DesiredUpdates from a dict
desired_updates_form_dict = desired_updates.from_dict(desired_updates_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


