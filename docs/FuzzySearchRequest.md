# FuzzySearchRequest


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**allow_stale** | **bool** |  | [optional] 
**auth_token** | **str** |  | [optional] 
**context** | **str** |  | [optional] 
**filter** | **str** |  | [optional] 
**headers** | **Dict[str, str]** |  | [optional] 
**namespace** | **str** |  | [optional] 
**next_token** | **str** |  | [optional] 
**params** | **Dict[str, str]** |  | [optional] 
**per_page** | **int** |  | [optional] 
**prefix** | **str** |  | [optional] 
**region** | **str** |  | [optional] 
**reverse** | **bool** |  | [optional] 
**text** | **str** |  | [optional] 
**wait_index** | **int** |  | [optional] 
**wait_time** | **int** |  | [optional] 

## Example

```python
from nomad_generated.models.fuzzy_search_request import FuzzySearchRequest

# TODO update the JSON string below
json = "{}"
# create an instance of FuzzySearchRequest from a JSON string
fuzzy_search_request_instance = FuzzySearchRequest.from_json(json)
# print the JSON string representation of the object
print FuzzySearchRequest.to_json()

# convert the object into a dict
fuzzy_search_request_dict = fuzzy_search_request_instance.to_dict()
# create an instance of FuzzySearchRequest from a dict
fuzzy_search_request_form_dict = fuzzy_search_request.from_dict(fuzzy_search_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


