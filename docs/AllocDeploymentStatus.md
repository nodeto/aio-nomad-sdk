# AllocDeploymentStatus


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**canary** | **bool** |  | [optional] 
**healthy** | **bool** |  | [optional] 
**modify_index** | **int** |  | [optional] 
**timestamp** | **datetime** |  | [optional] 

## Example

```python
from nomad_generated.models.alloc_deployment_status import AllocDeploymentStatus

# TODO update the JSON string below
json = "{}"
# create an instance of AllocDeploymentStatus from a JSON string
alloc_deployment_status_instance = AllocDeploymentStatus.from_json(json)
# print the JSON string representation of the object
print AllocDeploymentStatus.to_json()

# convert the object into a dict
alloc_deployment_status_dict = alloc_deployment_status_instance.to_dict()
# create an instance of AllocDeploymentStatus from a dict
alloc_deployment_status_form_dict = alloc_deployment_status.from_dict(alloc_deployment_status_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


