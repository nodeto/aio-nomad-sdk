# ConsulIngressConfigEntry


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**listeners** | [**List[ConsulIngressListener]**](ConsulIngressListener.md) |  | [optional] 
**tls** | [**ConsulGatewayTLSConfig**](ConsulGatewayTLSConfig.md) |  | [optional] 

## Example

```python
from nomad_generated.models.consul_ingress_config_entry import ConsulIngressConfigEntry

# TODO update the JSON string below
json = "{}"
# create an instance of ConsulIngressConfigEntry from a JSON string
consul_ingress_config_entry_instance = ConsulIngressConfigEntry.from_json(json)
# print the JSON string representation of the object
print ConsulIngressConfigEntry.to_json()

# convert the object into a dict
consul_ingress_config_entry_dict = consul_ingress_config_entry_instance.to_dict()
# create an instance of ConsulIngressConfigEntry from a dict
consul_ingress_config_entry_form_dict = consul_ingress_config_entry.from_dict(consul_ingress_config_entry_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


