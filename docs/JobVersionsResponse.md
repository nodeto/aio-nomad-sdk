# JobVersionsResponse


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**diffs** | [**List[JobDiff]**](JobDiff.md) |  | [optional] 
**known_leader** | **bool** |  | [optional] 
**last_contact** | **int** |  | [optional] 
**last_index** | **int** |  | [optional] 
**next_token** | **str** |  | [optional] 
**request_time** | **int** |  | [optional] 
**versions** | [**List[Job]**](Job.md) |  | [optional] 

## Example

```python
from nomad_generated.models.job_versions_response import JobVersionsResponse

# TODO update the JSON string below
json = "{}"
# create an instance of JobVersionsResponse from a JSON string
job_versions_response_instance = JobVersionsResponse.from_json(json)
# print the JSON string representation of the object
print JobVersionsResponse.to_json()

# convert the object into a dict
job_versions_response_dict = job_versions_response_instance.to_dict()
# create an instance of JobVersionsResponse from a dict
job_versions_response_form_dict = job_versions_response.from_dict(job_versions_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


