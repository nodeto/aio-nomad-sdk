# JobDiff


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**fields** | [**List[FieldDiff]**](FieldDiff.md) |  | [optional] 
**id** | **str** |  | [optional] 
**objects** | [**List[ObjectDiff]**](ObjectDiff.md) |  | [optional] 
**task_groups** | [**List[TaskGroupDiff]**](TaskGroupDiff.md) |  | [optional] 
**type** | **str** |  | [optional] 

## Example

```python
from nomad_generated.models.job_diff import JobDiff

# TODO update the JSON string below
json = "{}"
# create an instance of JobDiff from a JSON string
job_diff_instance = JobDiff.from_json(json)
# print the JSON string representation of the object
print JobDiff.to_json()

# convert the object into a dict
job_diff_dict = job_diff_instance.to_dict()
# create an instance of JobDiff from a dict
job_diff_form_dict = job_diff.from_dict(job_diff_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


