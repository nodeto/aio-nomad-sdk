# JobPlanResponse


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**annotations** | [**PlanAnnotations**](PlanAnnotations.md) |  | [optional] 
**created_evals** | [**List[Evaluation]**](Evaluation.md) |  | [optional] 
**diff** | [**JobDiff**](JobDiff.md) |  | [optional] 
**failed_tg_allocs** | [**Dict[str, AllocationMetric]**](AllocationMetric.md) |  | [optional] 
**job_modify_index** | **int** |  | [optional] 
**next_periodic_launch** | **datetime** |  | [optional] 
**warnings** | **str** |  | [optional] 

## Example

```python
from nomad_generated.models.job_plan_response import JobPlanResponse

# TODO update the JSON string below
json = "{}"
# create an instance of JobPlanResponse from a JSON string
job_plan_response_instance = JobPlanResponse.from_json(json)
# print the JSON string representation of the object
print JobPlanResponse.to_json()

# convert the object into a dict
job_plan_response_dict = job_plan_response_instance.to_dict()
# create an instance of JobPlanResponse from a dict
job_plan_response_form_dict = job_plan_response.from_dict(job_plan_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


