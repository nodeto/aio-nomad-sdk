# ConsulIngressService


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**hosts** | **List[str]** |  | [optional] 
**name** | **str** |  | [optional] 

## Example

```python
from nomad_generated.models.consul_ingress_service import ConsulIngressService

# TODO update the JSON string below
json = "{}"
# create an instance of ConsulIngressService from a JSON string
consul_ingress_service_instance = ConsulIngressService.from_json(json)
# print the JSON string representation of the object
print ConsulIngressService.to_json()

# convert the object into a dict
consul_ingress_service_dict = consul_ingress_service_instance.to_dict()
# create an instance of ConsulIngressService from a dict
consul_ingress_service_form_dict = consul_ingress_service.from_dict(consul_ingress_service_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


