# TaskState


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**events** | [**List[TaskEvent]**](TaskEvent.md) |  | [optional] 
**failed** | **bool** |  | [optional] 
**finished_at** | **datetime** |  | [optional] 
**last_restart** | **datetime** |  | [optional] 
**restarts** | **int** |  | [optional] 
**started_at** | **datetime** |  | [optional] 
**state** | **str** |  | [optional] 
**task_handle** | [**TaskHandle**](TaskHandle.md) |  | [optional] 

## Example

```python
from nomad_generated.models.task_state import TaskState

# TODO update the JSON string below
json = "{}"
# create an instance of TaskState from a JSON string
task_state_instance = TaskState.from_json(json)
# print the JSON string representation of the object
print TaskState.to_json()

# convert the object into a dict
task_state_dict = task_state_instance.to_dict()
# create an instance of TaskState from a dict
task_state_form_dict = task_state.from_dict(task_state_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


