# AllocStopResponse


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**eval_id** | **str** |  | [optional] 
**index** | **int** |  | [optional] 

## Example

```python
from nomad_generated.models.alloc_stop_response import AllocStopResponse

# TODO update the JSON string below
json = "{}"
# create an instance of AllocStopResponse from a JSON string
alloc_stop_response_instance = AllocStopResponse.from_json(json)
# print the JSON string representation of the object
print AllocStopResponse.to_json()

# convert the object into a dict
alloc_stop_response_dict = alloc_stop_response_instance.to_dict()
# create an instance of AllocStopResponse from a dict
alloc_stop_response_form_dict = alloc_stop_response.from_dict(alloc_stop_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


