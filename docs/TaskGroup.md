# TaskGroup


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**affinities** | [**List[Affinity]**](Affinity.md) |  | [optional] 
**constraints** | [**List[Constraint]**](Constraint.md) |  | [optional] 
**consul** | [**Consul**](Consul.md) |  | [optional] 
**count** | **int** |  | [optional] 
**ephemeral_disk** | [**EphemeralDisk**](EphemeralDisk.md) |  | [optional] 
**max_client_disconnect** | **int** |  | [optional] 
**meta** | **Dict[str, str]** |  | [optional] 
**migrate** | [**MigrateStrategy**](MigrateStrategy.md) |  | [optional] 
**name** | **str** |  | [optional] 
**networks** | [**List[NetworkResource]**](NetworkResource.md) |  | [optional] 
**reschedule_policy** | [**ReschedulePolicy**](ReschedulePolicy.md) |  | [optional] 
**restart_policy** | [**RestartPolicy**](RestartPolicy.md) |  | [optional] 
**scaling** | [**ScalingPolicy**](ScalingPolicy.md) |  | [optional] 
**services** | [**List[Service]**](Service.md) |  | [optional] 
**shutdown_delay** | **int** |  | [optional] 
**spreads** | [**List[Spread]**](Spread.md) |  | [optional] 
**stop_after_client_disconnect** | **int** |  | [optional] 
**tasks** | [**List[Task]**](Task.md) |  | [optional] 
**update** | [**UpdateStrategy**](UpdateStrategy.md) |  | [optional] 
**volumes** | [**Dict[str, VolumeRequest]**](VolumeRequest.md) |  | [optional] 

## Example

```python
from nomad_generated.models.task_group import TaskGroup

# TODO update the JSON string below
json = "{}"
# create an instance of TaskGroup from a JSON string
task_group_instance = TaskGroup.from_json(json)
# print the JSON string representation of the object
print TaskGroup.to_json()

# convert the object into a dict
task_group_dict = task_group_instance.to_dict()
# create an instance of TaskGroup from a dict
task_group_form_dict = task_group.from_dict(task_group_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


