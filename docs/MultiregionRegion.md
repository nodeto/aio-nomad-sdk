# MultiregionRegion


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**count** | **int** |  | [optional] 
**datacenters** | **List[str]** |  | [optional] 
**meta** | **Dict[str, str]** |  | [optional] 
**name** | **str** |  | [optional] 

## Example

```python
from nomad_generated.models.multiregion_region import MultiregionRegion

# TODO update the JSON string below
json = "{}"
# create an instance of MultiregionRegion from a JSON string
multiregion_region_instance = MultiregionRegion.from_json(json)
# print the JSON string representation of the object
print MultiregionRegion.to_json()

# convert the object into a dict
multiregion_region_dict = multiregion_region_instance.to_dict()
# create an instance of MultiregionRegion from a dict
multiregion_region_form_dict = multiregion_region.from_dict(multiregion_region_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


