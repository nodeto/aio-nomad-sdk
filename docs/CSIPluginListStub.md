# CSIPluginListStub


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**controller_required** | **bool** |  | [optional] 
**controllers_expected** | **int** |  | [optional] 
**controllers_healthy** | **int** |  | [optional] 
**create_index** | **int** |  | [optional] 
**id** | **str** |  | [optional] 
**modify_index** | **int** |  | [optional] 
**nodes_expected** | **int** |  | [optional] 
**nodes_healthy** | **int** |  | [optional] 
**provider** | **str** |  | [optional] 

## Example

```python
from nomad_generated.models.csi_plugin_list_stub import CSIPluginListStub

# TODO update the JSON string below
json = "{}"
# create an instance of CSIPluginListStub from a JSON string
csi_plugin_list_stub_instance = CSIPluginListStub.from_json(json)
# print the JSON string representation of the object
print CSIPluginListStub.to_json()

# convert the object into a dict
csi_plugin_list_stub_dict = csi_plugin_list_stub_instance.to_dict()
# create an instance of CSIPluginListStub from a dict
csi_plugin_list_stub_form_dict = csi_plugin_list_stub.from_dict(csi_plugin_list_stub_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


