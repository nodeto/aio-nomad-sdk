# JobDispatchRequest


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**job_id** | **str** |  | [optional] 
**meta** | **Dict[str, str]** |  | [optional] 
**payload** | **bytearray** |  | [optional] 

## Example

```python
from nomad_generated.models.job_dispatch_request import JobDispatchRequest

# TODO update the JSON string below
json = "{}"
# create an instance of JobDispatchRequest from a JSON string
job_dispatch_request_instance = JobDispatchRequest.from_json(json)
# print the JSON string representation of the object
print JobDispatchRequest.to_json()

# convert the object into a dict
job_dispatch_request_dict = job_dispatch_request_instance.to_dict()
# create an instance of JobDispatchRequest from a dict
job_dispatch_request_form_dict = job_dispatch_request.from_dict(job_dispatch_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


