# RaftServer


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**address** | **str** |  | [optional] 
**id** | **str** |  | [optional] 
**leader** | **bool** |  | [optional] 
**node** | **str** |  | [optional] 
**raft_protocol** | **str** |  | [optional] 
**voter** | **bool** |  | [optional] 

## Example

```python
from nomad_generated.models.raft_server import RaftServer

# TODO update the JSON string below
json = "{}"
# create an instance of RaftServer from a JSON string
raft_server_instance = RaftServer.from_json(json)
# print the JSON string representation of the object
print RaftServer.to_json()

# convert the object into a dict
raft_server_dict = raft_server_instance.to_dict()
# create an instance of RaftServer from a dict
raft_server_form_dict = raft_server.from_dict(raft_server_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


