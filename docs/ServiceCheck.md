# ServiceCheck


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**address_mode** | **str** |  | [optional] 
**advertise** | **str** |  | [optional] 
**args** | **List[str]** |  | [optional] 
**body** | **str** |  | [optional] 
**check_restart** | [**CheckRestart**](CheckRestart.md) |  | [optional] 
**command** | **str** |  | [optional] 
**expose** | **bool** |  | [optional] 
**failures_before_critical** | **int** |  | [optional] 
**grpc_service** | **str** |  | [optional] 
**grpc_use_tls** | **bool** |  | [optional] 
**header** | **Dict[str, List[str]]** |  | [optional] 
**initial_status** | **str** |  | [optional] 
**interval** | **int** |  | [optional] 
**method** | **str** |  | [optional] 
**name** | **str** |  | [optional] 
**on_update** | **str** |  | [optional] 
**path** | **str** |  | [optional] 
**port_label** | **str** |  | [optional] 
**protocol** | **str** |  | [optional] 
**success_before_passing** | **int** |  | [optional] 
**tls_skip_verify** | **bool** |  | [optional] 
**task_name** | **str** |  | [optional] 
**timeout** | **int** |  | [optional] 
**type** | **str** |  | [optional] 

## Example

```python
from nomad_generated.models.service_check import ServiceCheck

# TODO update the JSON string below
json = "{}"
# create an instance of ServiceCheck from a JSON string
service_check_instance = ServiceCheck.from_json(json)
# print the JSON string representation of the object
print ServiceCheck.to_json()

# convert the object into a dict
service_check_dict = service_check_instance.to_dict()
# create an instance of ServiceCheck from a dict
service_check_form_dict = service_check.from_dict(service_check_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


