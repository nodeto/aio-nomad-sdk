# ScalingPolicyListStub


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**create_index** | **int** |  | [optional] 
**enabled** | **bool** |  | [optional] 
**id** | **str** |  | [optional] 
**modify_index** | **int** |  | [optional] 
**target** | **Dict[str, str]** |  | [optional] 
**type** | **str** |  | [optional] 

## Example

```python
from nomad_generated.models.scaling_policy_list_stub import ScalingPolicyListStub

# TODO update the JSON string below
json = "{}"
# create an instance of ScalingPolicyListStub from a JSON string
scaling_policy_list_stub_instance = ScalingPolicyListStub.from_json(json)
# print the JSON string representation of the object
print ScalingPolicyListStub.to_json()

# convert the object into a dict
scaling_policy_list_stub_dict = scaling_policy_list_stub_instance.to_dict()
# create an instance of ScalingPolicyListStub from a dict
scaling_policy_list_stub_form_dict = scaling_policy_list_stub.from_dict(scaling_policy_list_stub_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


