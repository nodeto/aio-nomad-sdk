# ACLTokenRoleLink


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** |  | [optional] 
**name** | **str** |  | [optional] 

## Example

```python
from nomad_generated.models.acl_token_role_link import ACLTokenRoleLink

# TODO update the JSON string below
json = "{}"
# create an instance of ACLTokenRoleLink from a JSON string
acl_token_role_link_instance = ACLTokenRoleLink.from_json(json)
# print the JSON string representation of the object
print ACLTokenRoleLink.to_json()

# convert the object into a dict
acl_token_role_link_dict = acl_token_role_link_instance.to_dict()
# create an instance of ACLTokenRoleLink from a dict
acl_token_role_link_form_dict = acl_token_role_link.from_dict(acl_token_role_link_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


