# SearchRequest


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**allow_stale** | **bool** |  | [optional] 
**auth_token** | **str** |  | [optional] 
**context** | **str** |  | [optional] 
**filter** | **str** |  | [optional] 
**headers** | **Dict[str, str]** |  | [optional] 
**namespace** | **str** |  | [optional] 
**next_token** | **str** |  | [optional] 
**params** | **Dict[str, str]** |  | [optional] 
**per_page** | **int** |  | [optional] 
**prefix** | **str** |  | [optional] 
**region** | **str** |  | [optional] 
**reverse** | **bool** |  | [optional] 
**wait_index** | **int** |  | [optional] 
**wait_time** | **int** |  | [optional] 

## Example

```python
from nomad_generated.models.search_request import SearchRequest

# TODO update the JSON string below
json = "{}"
# create an instance of SearchRequest from a JSON string
search_request_instance = SearchRequest.from_json(json)
# print the JSON string representation of the object
print SearchRequest.to_json()

# convert the object into a dict
search_request_dict = search_request_instance.to_dict()
# create an instance of SearchRequest from a dict
search_request_form_dict = search_request.from_dict(search_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


