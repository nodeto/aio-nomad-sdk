# NodePurgeResponse


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**eval_create_index** | **int** |  | [optional] 
**eval_ids** | **List[str]** |  | [optional] 
**node_modify_index** | **int** |  | [optional] 

## Example

```python
from nomad_generated.models.node_purge_response import NodePurgeResponse

# TODO update the JSON string below
json = "{}"
# create an instance of NodePurgeResponse from a JSON string
node_purge_response_instance = NodePurgeResponse.from_json(json)
# print the JSON string representation of the object
print NodePurgeResponse.to_json()

# convert the object into a dict
node_purge_response_dict = node_purge_response_instance.to_dict()
# create an instance of NodePurgeResponse from a dict
node_purge_response_form_dict = node_purge_response.from_dict(node_purge_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


