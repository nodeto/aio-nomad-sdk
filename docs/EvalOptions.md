# EvalOptions


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**force_reschedule** | **bool** |  | [optional] 

## Example

```python
from nomad_generated.models.eval_options import EvalOptions

# TODO update the JSON string below
json = "{}"
# create an instance of EvalOptions from a JSON string
eval_options_instance = EvalOptions.from_json(json)
# print the JSON string representation of the object
print EvalOptions.to_json()

# convert the object into a dict
eval_options_dict = eval_options_instance.to_dict()
# create an instance of EvalOptions from a dict
eval_options_form_dict = eval_options.from_dict(eval_options_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


