# NodeResources


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cpu** | [**NodeCpuResources**](NodeCpuResources.md) |  | [optional] 
**devices** | [**List[NodeDeviceResource]**](NodeDeviceResource.md) |  | [optional] 
**disk** | [**NodeDiskResources**](NodeDiskResources.md) |  | [optional] 
**max_dynamic_port** | **int** |  | [optional] 
**memory** | [**NodeMemoryResources**](NodeMemoryResources.md) |  | [optional] 
**min_dynamic_port** | **int** |  | [optional] 
**networks** | [**List[NetworkResource]**](NetworkResource.md) |  | [optional] 

## Example

```python
from nomad_generated.models.node_resources import NodeResources

# TODO update the JSON string below
json = "{}"
# create an instance of NodeResources from a JSON string
node_resources_instance = NodeResources.from_json(json)
# print the JSON string representation of the object
print NodeResources.to_json()

# convert the object into a dict
node_resources_dict = node_resources_instance.to_dict()
# create an instance of NodeResources from a dict
node_resources_form_dict = node_resources.from_dict(node_resources_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


