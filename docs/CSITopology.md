# CSITopology


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**segments** | **Dict[str, str]** |  | [optional] 

## Example

```python
from nomad_generated.models.csi_topology import CSITopology

# TODO update the JSON string below
json = "{}"
# create an instance of CSITopology from a JSON string
csi_topology_instance = CSITopology.from_json(json)
# print the JSON string representation of the object
print CSITopology.to_json()

# convert the object into a dict
csi_topology_dict = csi_topology_instance.to_dict()
# create an instance of CSITopology from a dict
csi_topology_form_dict = csi_topology.from_dict(csi_topology_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


