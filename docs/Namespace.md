# Namespace


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**capabilities** | [**NamespaceCapabilities**](NamespaceCapabilities.md) |  | [optional] 
**create_index** | **int** |  | [optional] 
**description** | **str** |  | [optional] 
**meta** | **Dict[str, str]** |  | [optional] 
**modify_index** | **int** |  | [optional] 
**name** | **str** |  | [optional] 
**quota** | **str** |  | [optional] 

## Example

```python
from nomad_generated.models.namespace import Namespace

# TODO update the JSON string below
json = "{}"
# create an instance of Namespace from a JSON string
namespace_instance = Namespace.from_json(json)
# print the JSON string representation of the object
print Namespace.to_json()

# convert the object into a dict
namespace_dict = namespace_instance.to_dict()
# create an instance of Namespace from a dict
namespace_form_dict = namespace.from_dict(namespace_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


