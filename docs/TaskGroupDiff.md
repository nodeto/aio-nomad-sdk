# TaskGroupDiff


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**fields** | [**List[FieldDiff]**](FieldDiff.md) |  | [optional] 
**name** | **str** |  | [optional] 
**objects** | [**List[ObjectDiff]**](ObjectDiff.md) |  | [optional] 
**tasks** | [**List[TaskDiff]**](TaskDiff.md) |  | [optional] 
**type** | **str** |  | [optional] 
**updates** | **Dict[str, int]** |  | [optional] 

## Example

```python
from nomad_generated.models.task_group_diff import TaskGroupDiff

# TODO update the JSON string below
json = "{}"
# create an instance of TaskGroupDiff from a JSON string
task_group_diff_instance = TaskGroupDiff.from_json(json)
# print the JSON string representation of the object
print TaskGroupDiff.to_json()

# convert the object into a dict
task_group_diff_dict = task_group_diff_instance.to_dict()
# create an instance of TaskGroupDiff from a dict
task_group_diff_form_dict = task_group_diff.from_dict(task_group_diff_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


