# AllocationMetric


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**allocation_time** | **int** |  | [optional] 
**class_exhausted** | **Dict[str, int]** |  | [optional] 
**class_filtered** | **Dict[str, int]** |  | [optional] 
**coalesced_failures** | **int** |  | [optional] 
**constraint_filtered** | **Dict[str, int]** |  | [optional] 
**dimension_exhausted** | **Dict[str, int]** |  | [optional] 
**nodes_available** | **Dict[str, int]** |  | [optional] 
**nodes_evaluated** | **int** |  | [optional] 
**nodes_exhausted** | **int** |  | [optional] 
**nodes_filtered** | **int** |  | [optional] 
**quota_exhausted** | **List[str]** |  | [optional] 
**resources_exhausted** | [**Dict[str, Resources]**](Resources.md) |  | [optional] 
**score_meta_data** | [**List[NodeScoreMeta]**](NodeScoreMeta.md) |  | [optional] 
**scores** | **Dict[str, float]** |  | [optional] 

## Example

```python
from nomad_generated.models.allocation_metric import AllocationMetric

# TODO update the JSON string below
json = "{}"
# create an instance of AllocationMetric from a JSON string
allocation_metric_instance = AllocationMetric.from_json(json)
# print the JSON string representation of the object
print AllocationMetric.to_json()

# convert the object into a dict
allocation_metric_dict = allocation_metric_instance.to_dict()
# create an instance of AllocationMetric from a dict
allocation_metric_form_dict = allocation_metric.from_dict(allocation_metric_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


