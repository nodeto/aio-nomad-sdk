# PreemptionConfig


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**batch_scheduler_enabled** | **bool** |  | [optional] 
**service_scheduler_enabled** | **bool** |  | [optional] 
**sys_batch_scheduler_enabled** | **bool** |  | [optional] 
**system_scheduler_enabled** | **bool** |  | [optional] 

## Example

```python
from nomad_generated.models.preemption_config import PreemptionConfig

# TODO update the JSON string below
json = "{}"
# create an instance of PreemptionConfig from a JSON string
preemption_config_instance = PreemptionConfig.from_json(json)
# print the JSON string representation of the object
print PreemptionConfig.to_json()

# convert the object into a dict
preemption_config_dict = preemption_config_instance.to_dict()
# create an instance of PreemptionConfig from a dict
preemption_config_form_dict = preemption_config.from_dict(preemption_config_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


