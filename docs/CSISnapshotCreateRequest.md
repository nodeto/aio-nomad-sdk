# CSISnapshotCreateRequest


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**namespace** | **str** |  | [optional] 
**region** | **str** |  | [optional] 
**secret_id** | **str** |  | [optional] 
**snapshots** | [**List[CSISnapshot]**](CSISnapshot.md) |  | [optional] 

## Example

```python
from nomad_generated.models.csi_snapshot_create_request import CSISnapshotCreateRequest

# TODO update the JSON string below
json = "{}"
# create an instance of CSISnapshotCreateRequest from a JSON string
csi_snapshot_create_request_instance = CSISnapshotCreateRequest.from_json(json)
# print the JSON string representation of the object
print CSISnapshotCreateRequest.to_json()

# convert the object into a dict
csi_snapshot_create_request_dict = csi_snapshot_create_request_instance.to_dict()
# create an instance of CSISnapshotCreateRequest from a dict
csi_snapshot_create_request_form_dict = csi_snapshot_create_request.from_dict(csi_snapshot_create_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


