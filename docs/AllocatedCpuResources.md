# AllocatedCpuResources


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cpu_shares** | **int** |  | [optional] 

## Example

```python
from nomad_generated.models.allocated_cpu_resources import AllocatedCpuResources

# TODO update the JSON string below
json = "{}"
# create an instance of AllocatedCpuResources from a JSON string
allocated_cpu_resources_instance = AllocatedCpuResources.from_json(json)
# print the JSON string representation of the object
print AllocatedCpuResources.to_json()

# convert the object into a dict
allocated_cpu_resources_dict = allocated_cpu_resources_instance.to_dict()
# create an instance of AllocatedCpuResources from a dict
allocated_cpu_resources_form_dict = allocated_cpu_resources.from_dict(allocated_cpu_resources_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


