# ConsulGatewayBindAddress


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**address** | **str** |  | [optional] 
**name** | **str** |  | [optional] 
**port** | **int** |  | [optional] 

## Example

```python
from nomad_generated.models.consul_gateway_bind_address import ConsulGatewayBindAddress

# TODO update the JSON string below
json = "{}"
# create an instance of ConsulGatewayBindAddress from a JSON string
consul_gateway_bind_address_instance = ConsulGatewayBindAddress.from_json(json)
# print the JSON string representation of the object
print ConsulGatewayBindAddress.to_json()

# convert the object into a dict
consul_gateway_bind_address_dict = consul_gateway_bind_address_instance.to_dict()
# create an instance of ConsulGatewayBindAddress from a dict
consul_gateway_bind_address_form_dict = consul_gateway_bind_address.from_dict(consul_gateway_bind_address_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


