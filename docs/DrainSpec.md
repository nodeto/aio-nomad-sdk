# DrainSpec


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**deadline** | **int** |  | [optional] 
**ignore_system_jobs** | **bool** |  | [optional] 

## Example

```python
from nomad_generated.models.drain_spec import DrainSpec

# TODO update the JSON string below
json = "{}"
# create an instance of DrainSpec from a JSON string
drain_spec_instance = DrainSpec.from_json(json)
# print the JSON string representation of the object
print DrainSpec.to_json()

# convert the object into a dict
drain_spec_dict = drain_spec_instance.to_dict()
# create an instance of DrainSpec from a dict
drain_spec_form_dict = drain_spec.from_dict(drain_spec_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


