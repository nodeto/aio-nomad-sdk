# SchedulerConfiguration


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**create_index** | **int** |  | [optional] 
**memory_oversubscription_enabled** | **bool** |  | [optional] 
**modify_index** | **int** |  | [optional] 
**pause_eval_broker** | **bool** |  | [optional] 
**preemption_config** | [**PreemptionConfig**](PreemptionConfig.md) |  | [optional] 
**reject_job_registration** | **bool** |  | [optional] 
**scheduler_algorithm** | **str** |  | [optional] 

## Example

```python
from nomad_generated.models.scheduler_configuration import SchedulerConfiguration

# TODO update the JSON string below
json = "{}"
# create an instance of SchedulerConfiguration from a JSON string
scheduler_configuration_instance = SchedulerConfiguration.from_json(json)
# print the JSON string representation of the object
print SchedulerConfiguration.to_json()

# convert the object into a dict
scheduler_configuration_dict = scheduler_configuration_instance.to_dict()
# create an instance of SchedulerConfiguration from a dict
scheduler_configuration_form_dict = scheduler_configuration.from_dict(scheduler_configuration_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


