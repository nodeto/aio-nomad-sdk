# TaskArtifact


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**getter_headers** | **Dict[str, str]** |  | [optional] 
**getter_mode** | **str** |  | [optional] 
**getter_options** | **Dict[str, str]** |  | [optional] 
**getter_source** | **str** |  | [optional] 
**relative_dest** | **str** |  | [optional] 

## Example

```python
from nomad_generated.models.task_artifact import TaskArtifact

# TODO update the JSON string below
json = "{}"
# create an instance of TaskArtifact from a JSON string
task_artifact_instance = TaskArtifact.from_json(json)
# print the JSON string representation of the object
print TaskArtifact.to_json()

# convert the object into a dict
task_artifact_dict = task_artifact_instance.to_dict()
# create an instance of TaskArtifact from a dict
task_artifact_form_dict = task_artifact.from_dict(task_artifact_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


