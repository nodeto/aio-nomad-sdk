# DrainStrategy


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**deadline** | **int** |  | [optional] 
**force_deadline** | **datetime** |  | [optional] 
**ignore_system_jobs** | **bool** |  | [optional] 
**started_at** | **datetime** |  | [optional] 

## Example

```python
from nomad_generated.models.drain_strategy import DrainStrategy

# TODO update the JSON string below
json = "{}"
# create an instance of DrainStrategy from a JSON string
drain_strategy_instance = DrainStrategy.from_json(json)
# print the JSON string representation of the object
print DrainStrategy.to_json()

# convert the object into a dict
drain_strategy_dict = drain_strategy_instance.to_dict()
# create an instance of DrainStrategy from a dict
drain_strategy_form_dict = drain_strategy.from_dict(drain_strategy_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


