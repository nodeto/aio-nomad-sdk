# TaskHandle


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**driver_state** | **bytearray** |  | [optional] 
**version** | **int** |  | [optional] 

## Example

```python
from nomad_generated.models.task_handle import TaskHandle

# TODO update the JSON string below
json = "{}"
# create an instance of TaskHandle from a JSON string
task_handle_instance = TaskHandle.from_json(json)
# print the JSON string representation of the object
print TaskHandle.to_json()

# convert the object into a dict
task_handle_dict = task_handle_instance.to_dict()
# create an instance of TaskHandle from a dict
task_handle_form_dict = task_handle.from_dict(task_handle_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


