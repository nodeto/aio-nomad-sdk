# DeploymentUpdateResponse


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**deployment_modify_index** | **int** |  | [optional] 
**eval_create_index** | **int** |  | [optional] 
**eval_id** | **str** |  | [optional] 
**last_index** | **int** |  | [optional] 
**request_time** | **int** |  | [optional] 
**reverted_job_version** | **int** |  | [optional] 

## Example

```python
from nomad_generated.models.deployment_update_response import DeploymentUpdateResponse

# TODO update the JSON string below
json = "{}"
# create an instance of DeploymentUpdateResponse from a JSON string
deployment_update_response_instance = DeploymentUpdateResponse.from_json(json)
# print the JSON string representation of the object
print DeploymentUpdateResponse.to_json()

# convert the object into a dict
deployment_update_response_dict = deployment_update_response_instance.to_dict()
# create an instance of DeploymentUpdateResponse from a dict
deployment_update_response_form_dict = deployment_update_response.from_dict(deployment_update_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


