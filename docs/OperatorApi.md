# nomad_generated.OperatorApi

All URIs are relative to *http://127.0.0.1:4646/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**delete_operator_raft_peer**](OperatorApi.md#delete_operator_raft_peer) | **DELETE** /operator/raft/peer | 
[**get_operator_autopilot_configuration**](OperatorApi.md#get_operator_autopilot_configuration) | **GET** /operator/autopilot/configuration | 
[**get_operator_autopilot_health**](OperatorApi.md#get_operator_autopilot_health) | **GET** /operator/autopilot/health | 
[**get_operator_raft_configuration**](OperatorApi.md#get_operator_raft_configuration) | **GET** /operator/raft/configuration | 
[**get_operator_scheduler_configuration**](OperatorApi.md#get_operator_scheduler_configuration) | **GET** /operator/scheduler/configuration | 
[**post_operator_scheduler_configuration**](OperatorApi.md#post_operator_scheduler_configuration) | **POST** /operator/scheduler/configuration | 
[**put_operator_autopilot_configuration**](OperatorApi.md#put_operator_autopilot_configuration) | **PUT** /operator/autopilot/configuration | 


# **delete_operator_raft_peer**
> delete_operator_raft_peer(region=region, namespace=namespace, x_nomad_token=x_nomad_token, idempotency_token=idempotency_token)



### Example

* Api Key Authentication (X-Nomad-Token):
```python
import time
import os
import nomad_generated
from nomad_generated.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://127.0.0.1:4646/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = nomad_generated.Configuration(
    host = "http://127.0.0.1:4646/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: X-Nomad-Token
configuration.api_key['X-Nomad-Token'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['X-Nomad-Token'] = 'Bearer'

# Enter a context with an instance of the API client
async with nomad_generated.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = nomad_generated.OperatorApi(api_client)
    region = 'region_example' # str | Filters results based on the specified region. (optional)
    namespace = 'namespace_example' # str | Filters results based on the specified namespace. (optional)
    x_nomad_token = 'x_nomad_token_example' # str | A Nomad ACL token. (optional)
    idempotency_token = 'idempotency_token_example' # str | Can be used to ensure operations are only run once. (optional)

    try:
        await api_instance.delete_operator_raft_peer(region=region, namespace=namespace, x_nomad_token=x_nomad_token, idempotency_token=idempotency_token)
    except Exception as e:
        print("Exception when calling OperatorApi->delete_operator_raft_peer: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **region** | **str**| Filters results based on the specified region. | [optional] 
 **namespace** | **str**| Filters results based on the specified namespace. | [optional] 
 **x_nomad_token** | **str**| A Nomad ACL token. | [optional] 
 **idempotency_token** | **str**| Can be used to ensure operations are only run once. | [optional] 

### Return type

void (empty response body)

### Authorization

[X-Nomad-Token](../README.md#X-Nomad-Token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |
**400** | Bad request |  -  |
**403** | Forbidden |  -  |
**405** | Method not allowed |  -  |
**500** | Internal server error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_operator_autopilot_configuration**
> AutopilotConfiguration get_operator_autopilot_configuration(region=region, namespace=namespace, index=index, wait=wait, stale=stale, prefix=prefix, x_nomad_token=x_nomad_token, per_page=per_page, next_token=next_token)



### Example

* Api Key Authentication (X-Nomad-Token):
```python
import time
import os
import nomad_generated
from nomad_generated.models.autopilot_configuration import AutopilotConfiguration
from nomad_generated.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://127.0.0.1:4646/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = nomad_generated.Configuration(
    host = "http://127.0.0.1:4646/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: X-Nomad-Token
configuration.api_key['X-Nomad-Token'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['X-Nomad-Token'] = 'Bearer'

# Enter a context with an instance of the API client
async with nomad_generated.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = nomad_generated.OperatorApi(api_client)
    region = 'region_example' # str | Filters results based on the specified region. (optional)
    namespace = 'namespace_example' # str | Filters results based on the specified namespace. (optional)
    index = 56 # int | If set, wait until query exceeds given index. Must be provided with WaitParam. (optional)
    wait = 'wait_example' # str | Provided with IndexParam to wait for change. (optional)
    stale = 'stale_example' # str | If present, results will include stale reads. (optional)
    prefix = 'prefix_example' # str | Constrains results to jobs that start with the defined prefix (optional)
    x_nomad_token = 'x_nomad_token_example' # str | A Nomad ACL token. (optional)
    per_page = 56 # int | Maximum number of results to return. (optional)
    next_token = 'next_token_example' # str | Indicates where to start paging for queries that support pagination. (optional)

    try:
        api_response = await api_instance.get_operator_autopilot_configuration(region=region, namespace=namespace, index=index, wait=wait, stale=stale, prefix=prefix, x_nomad_token=x_nomad_token, per_page=per_page, next_token=next_token)
        print("The response of OperatorApi->get_operator_autopilot_configuration:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling OperatorApi->get_operator_autopilot_configuration: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **region** | **str**| Filters results based on the specified region. | [optional] 
 **namespace** | **str**| Filters results based on the specified namespace. | [optional] 
 **index** | **int**| If set, wait until query exceeds given index. Must be provided with WaitParam. | [optional] 
 **wait** | **str**| Provided with IndexParam to wait for change. | [optional] 
 **stale** | **str**| If present, results will include stale reads. | [optional] 
 **prefix** | **str**| Constrains results to jobs that start with the defined prefix | [optional] 
 **x_nomad_token** | **str**| A Nomad ACL token. | [optional] 
 **per_page** | **int**| Maximum number of results to return. | [optional] 
 **next_token** | **str**| Indicates where to start paging for queries that support pagination. | [optional] 

### Return type

[**AutopilotConfiguration**](AutopilotConfiguration.md)

### Authorization

[X-Nomad-Token](../README.md#X-Nomad-Token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |
**400** | Bad request |  -  |
**403** | Forbidden |  -  |
**405** | Method not allowed |  -  |
**500** | Internal server error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_operator_autopilot_health**
> OperatorHealthReply get_operator_autopilot_health(region=region, namespace=namespace, index=index, wait=wait, stale=stale, prefix=prefix, x_nomad_token=x_nomad_token, per_page=per_page, next_token=next_token)



### Example

* Api Key Authentication (X-Nomad-Token):
```python
import time
import os
import nomad_generated
from nomad_generated.models.operator_health_reply import OperatorHealthReply
from nomad_generated.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://127.0.0.1:4646/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = nomad_generated.Configuration(
    host = "http://127.0.0.1:4646/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: X-Nomad-Token
configuration.api_key['X-Nomad-Token'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['X-Nomad-Token'] = 'Bearer'

# Enter a context with an instance of the API client
async with nomad_generated.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = nomad_generated.OperatorApi(api_client)
    region = 'region_example' # str | Filters results based on the specified region. (optional)
    namespace = 'namespace_example' # str | Filters results based on the specified namespace. (optional)
    index = 56 # int | If set, wait until query exceeds given index. Must be provided with WaitParam. (optional)
    wait = 'wait_example' # str | Provided with IndexParam to wait for change. (optional)
    stale = 'stale_example' # str | If present, results will include stale reads. (optional)
    prefix = 'prefix_example' # str | Constrains results to jobs that start with the defined prefix (optional)
    x_nomad_token = 'x_nomad_token_example' # str | A Nomad ACL token. (optional)
    per_page = 56 # int | Maximum number of results to return. (optional)
    next_token = 'next_token_example' # str | Indicates where to start paging for queries that support pagination. (optional)

    try:
        api_response = await api_instance.get_operator_autopilot_health(region=region, namespace=namespace, index=index, wait=wait, stale=stale, prefix=prefix, x_nomad_token=x_nomad_token, per_page=per_page, next_token=next_token)
        print("The response of OperatorApi->get_operator_autopilot_health:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling OperatorApi->get_operator_autopilot_health: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **region** | **str**| Filters results based on the specified region. | [optional] 
 **namespace** | **str**| Filters results based on the specified namespace. | [optional] 
 **index** | **int**| If set, wait until query exceeds given index. Must be provided with WaitParam. | [optional] 
 **wait** | **str**| Provided with IndexParam to wait for change. | [optional] 
 **stale** | **str**| If present, results will include stale reads. | [optional] 
 **prefix** | **str**| Constrains results to jobs that start with the defined prefix | [optional] 
 **x_nomad_token** | **str**| A Nomad ACL token. | [optional] 
 **per_page** | **int**| Maximum number of results to return. | [optional] 
 **next_token** | **str**| Indicates where to start paging for queries that support pagination. | [optional] 

### Return type

[**OperatorHealthReply**](OperatorHealthReply.md)

### Authorization

[X-Nomad-Token](../README.md#X-Nomad-Token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |
**400** | Bad request |  -  |
**403** | Forbidden |  -  |
**405** | Method not allowed |  -  |
**500** | Internal server error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_operator_raft_configuration**
> RaftConfiguration get_operator_raft_configuration(region=region, namespace=namespace, index=index, wait=wait, stale=stale, prefix=prefix, x_nomad_token=x_nomad_token, per_page=per_page, next_token=next_token)



### Example

* Api Key Authentication (X-Nomad-Token):
```python
import time
import os
import nomad_generated
from nomad_generated.models.raft_configuration import RaftConfiguration
from nomad_generated.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://127.0.0.1:4646/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = nomad_generated.Configuration(
    host = "http://127.0.0.1:4646/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: X-Nomad-Token
configuration.api_key['X-Nomad-Token'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['X-Nomad-Token'] = 'Bearer'

# Enter a context with an instance of the API client
async with nomad_generated.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = nomad_generated.OperatorApi(api_client)
    region = 'region_example' # str | Filters results based on the specified region. (optional)
    namespace = 'namespace_example' # str | Filters results based on the specified namespace. (optional)
    index = 56 # int | If set, wait until query exceeds given index. Must be provided with WaitParam. (optional)
    wait = 'wait_example' # str | Provided with IndexParam to wait for change. (optional)
    stale = 'stale_example' # str | If present, results will include stale reads. (optional)
    prefix = 'prefix_example' # str | Constrains results to jobs that start with the defined prefix (optional)
    x_nomad_token = 'x_nomad_token_example' # str | A Nomad ACL token. (optional)
    per_page = 56 # int | Maximum number of results to return. (optional)
    next_token = 'next_token_example' # str | Indicates where to start paging for queries that support pagination. (optional)

    try:
        api_response = await api_instance.get_operator_raft_configuration(region=region, namespace=namespace, index=index, wait=wait, stale=stale, prefix=prefix, x_nomad_token=x_nomad_token, per_page=per_page, next_token=next_token)
        print("The response of OperatorApi->get_operator_raft_configuration:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling OperatorApi->get_operator_raft_configuration: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **region** | **str**| Filters results based on the specified region. | [optional] 
 **namespace** | **str**| Filters results based on the specified namespace. | [optional] 
 **index** | **int**| If set, wait until query exceeds given index. Must be provided with WaitParam. | [optional] 
 **wait** | **str**| Provided with IndexParam to wait for change. | [optional] 
 **stale** | **str**| If present, results will include stale reads. | [optional] 
 **prefix** | **str**| Constrains results to jobs that start with the defined prefix | [optional] 
 **x_nomad_token** | **str**| A Nomad ACL token. | [optional] 
 **per_page** | **int**| Maximum number of results to return. | [optional] 
 **next_token** | **str**| Indicates where to start paging for queries that support pagination. | [optional] 

### Return type

[**RaftConfiguration**](RaftConfiguration.md)

### Authorization

[X-Nomad-Token](../README.md#X-Nomad-Token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |
**400** | Bad request |  -  |
**403** | Forbidden |  -  |
**405** | Method not allowed |  -  |
**500** | Internal server error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_operator_scheduler_configuration**
> SchedulerConfigurationResponse get_operator_scheduler_configuration(region=region, namespace=namespace, index=index, wait=wait, stale=stale, prefix=prefix, x_nomad_token=x_nomad_token, per_page=per_page, next_token=next_token)



### Example

* Api Key Authentication (X-Nomad-Token):
```python
import time
import os
import nomad_generated
from nomad_generated.models.scheduler_configuration_response import SchedulerConfigurationResponse
from nomad_generated.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://127.0.0.1:4646/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = nomad_generated.Configuration(
    host = "http://127.0.0.1:4646/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: X-Nomad-Token
configuration.api_key['X-Nomad-Token'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['X-Nomad-Token'] = 'Bearer'

# Enter a context with an instance of the API client
async with nomad_generated.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = nomad_generated.OperatorApi(api_client)
    region = 'region_example' # str | Filters results based on the specified region. (optional)
    namespace = 'namespace_example' # str | Filters results based on the specified namespace. (optional)
    index = 56 # int | If set, wait until query exceeds given index. Must be provided with WaitParam. (optional)
    wait = 'wait_example' # str | Provided with IndexParam to wait for change. (optional)
    stale = 'stale_example' # str | If present, results will include stale reads. (optional)
    prefix = 'prefix_example' # str | Constrains results to jobs that start with the defined prefix (optional)
    x_nomad_token = 'x_nomad_token_example' # str | A Nomad ACL token. (optional)
    per_page = 56 # int | Maximum number of results to return. (optional)
    next_token = 'next_token_example' # str | Indicates where to start paging for queries that support pagination. (optional)

    try:
        api_response = await api_instance.get_operator_scheduler_configuration(region=region, namespace=namespace, index=index, wait=wait, stale=stale, prefix=prefix, x_nomad_token=x_nomad_token, per_page=per_page, next_token=next_token)
        print("The response of OperatorApi->get_operator_scheduler_configuration:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling OperatorApi->get_operator_scheduler_configuration: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **region** | **str**| Filters results based on the specified region. | [optional] 
 **namespace** | **str**| Filters results based on the specified namespace. | [optional] 
 **index** | **int**| If set, wait until query exceeds given index. Must be provided with WaitParam. | [optional] 
 **wait** | **str**| Provided with IndexParam to wait for change. | [optional] 
 **stale** | **str**| If present, results will include stale reads. | [optional] 
 **prefix** | **str**| Constrains results to jobs that start with the defined prefix | [optional] 
 **x_nomad_token** | **str**| A Nomad ACL token. | [optional] 
 **per_page** | **int**| Maximum number of results to return. | [optional] 
 **next_token** | **str**| Indicates where to start paging for queries that support pagination. | [optional] 

### Return type

[**SchedulerConfigurationResponse**](SchedulerConfigurationResponse.md)

### Authorization

[X-Nomad-Token](../README.md#X-Nomad-Token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  * X-Nomad-Index - A unique identifier representing the current state of the requested resource. On a new Nomad cluster the value of this index starts at 1. <br>  * X-Nomad-KnownLeader - Boolean indicating if there is a known cluster leader. <br>  * X-Nomad-LastContact - The time in milliseconds that a server was last contacted by the leader node. <br>  |
**400** | Bad request |  -  |
**403** | Forbidden |  -  |
**405** | Method not allowed |  -  |
**500** | Internal server error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_operator_scheduler_configuration**
> SchedulerSetConfigurationResponse post_operator_scheduler_configuration(scheduler_configuration, region=region, namespace=namespace, x_nomad_token=x_nomad_token, idempotency_token=idempotency_token)



### Example

* Api Key Authentication (X-Nomad-Token):
```python
import time
import os
import nomad_generated
from nomad_generated.models.scheduler_configuration import SchedulerConfiguration
from nomad_generated.models.scheduler_set_configuration_response import SchedulerSetConfigurationResponse
from nomad_generated.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://127.0.0.1:4646/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = nomad_generated.Configuration(
    host = "http://127.0.0.1:4646/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: X-Nomad-Token
configuration.api_key['X-Nomad-Token'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['X-Nomad-Token'] = 'Bearer'

# Enter a context with an instance of the API client
async with nomad_generated.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = nomad_generated.OperatorApi(api_client)
    scheduler_configuration = nomad_generated.SchedulerConfiguration() # SchedulerConfiguration | 
    region = 'region_example' # str | Filters results based on the specified region. (optional)
    namespace = 'namespace_example' # str | Filters results based on the specified namespace. (optional)
    x_nomad_token = 'x_nomad_token_example' # str | A Nomad ACL token. (optional)
    idempotency_token = 'idempotency_token_example' # str | Can be used to ensure operations are only run once. (optional)

    try:
        api_response = await api_instance.post_operator_scheduler_configuration(scheduler_configuration, region=region, namespace=namespace, x_nomad_token=x_nomad_token, idempotency_token=idempotency_token)
        print("The response of OperatorApi->post_operator_scheduler_configuration:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling OperatorApi->post_operator_scheduler_configuration: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **scheduler_configuration** | [**SchedulerConfiguration**](SchedulerConfiguration.md)|  | 
 **region** | **str**| Filters results based on the specified region. | [optional] 
 **namespace** | **str**| Filters results based on the specified namespace. | [optional] 
 **x_nomad_token** | **str**| A Nomad ACL token. | [optional] 
 **idempotency_token** | **str**| Can be used to ensure operations are only run once. | [optional] 

### Return type

[**SchedulerSetConfigurationResponse**](SchedulerSetConfigurationResponse.md)

### Authorization

[X-Nomad-Token](../README.md#X-Nomad-Token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  * X-Nomad-Index - A unique identifier representing the current state of the requested resource. On a new Nomad cluster the value of this index starts at 1. <br>  |
**400** | Bad request |  -  |
**403** | Forbidden |  -  |
**405** | Method not allowed |  -  |
**500** | Internal server error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **put_operator_autopilot_configuration**
> bool put_operator_autopilot_configuration(autopilot_configuration, region=region, namespace=namespace, x_nomad_token=x_nomad_token, idempotency_token=idempotency_token)



### Example

* Api Key Authentication (X-Nomad-Token):
```python
import time
import os
import nomad_generated
from nomad_generated.models.autopilot_configuration import AutopilotConfiguration
from nomad_generated.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://127.0.0.1:4646/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = nomad_generated.Configuration(
    host = "http://127.0.0.1:4646/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: X-Nomad-Token
configuration.api_key['X-Nomad-Token'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['X-Nomad-Token'] = 'Bearer'

# Enter a context with an instance of the API client
async with nomad_generated.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = nomad_generated.OperatorApi(api_client)
    autopilot_configuration = nomad_generated.AutopilotConfiguration() # AutopilotConfiguration | 
    region = 'region_example' # str | Filters results based on the specified region. (optional)
    namespace = 'namespace_example' # str | Filters results based on the specified namespace. (optional)
    x_nomad_token = 'x_nomad_token_example' # str | A Nomad ACL token. (optional)
    idempotency_token = 'idempotency_token_example' # str | Can be used to ensure operations are only run once. (optional)

    try:
        api_response = await api_instance.put_operator_autopilot_configuration(autopilot_configuration, region=region, namespace=namespace, x_nomad_token=x_nomad_token, idempotency_token=idempotency_token)
        print("The response of OperatorApi->put_operator_autopilot_configuration:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling OperatorApi->put_operator_autopilot_configuration: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **autopilot_configuration** | [**AutopilotConfiguration**](AutopilotConfiguration.md)|  | 
 **region** | **str**| Filters results based on the specified region. | [optional] 
 **namespace** | **str**| Filters results based on the specified namespace. | [optional] 
 **x_nomad_token** | **str**| A Nomad ACL token. | [optional] 
 **idempotency_token** | **str**| Can be used to ensure operations are only run once. | [optional] 

### Return type

**bool**

### Authorization

[X-Nomad-Token](../README.md#X-Nomad-Token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |
**400** | Bad request |  -  |
**403** | Forbidden |  -  |
**405** | Method not allowed |  -  |
**500** | Internal server error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

