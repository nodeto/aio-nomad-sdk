# FuzzySearchResponse


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**known_leader** | **bool** |  | [optional] 
**last_contact** | **int** |  | [optional] 
**last_index** | **int** |  | [optional] 
**matches** | **Dict[str, List[FuzzyMatch]]** |  | [optional] 
**next_token** | **str** |  | [optional] 
**request_time** | **int** |  | [optional] 
**truncations** | **Dict[str, bool]** |  | [optional] 

## Example

```python
from nomad_generated.models.fuzzy_search_response import FuzzySearchResponse

# TODO update the JSON string below
json = "{}"
# create an instance of FuzzySearchResponse from a JSON string
fuzzy_search_response_instance = FuzzySearchResponse.from_json(json)
# print the JSON string representation of the object
print FuzzySearchResponse.to_json()

# convert the object into a dict
fuzzy_search_response_dict = fuzzy_search_response_instance.to_dict()
# create an instance of FuzzySearchResponse from a dict
fuzzy_search_response_form_dict = fuzzy_search_response.from_dict(fuzzy_search_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


