# NodeListStub


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**address** | **str** |  | [optional] 
**attributes** | **Dict[str, str]** |  | [optional] 
**create_index** | **int** |  | [optional] 
**datacenter** | **str** |  | [optional] 
**drain** | **bool** |  | [optional] 
**drivers** | [**Dict[str, DriverInfo]**](DriverInfo.md) |  | [optional] 
**id** | **str** |  | [optional] 
**last_drain** | [**DrainMetadata**](DrainMetadata.md) |  | [optional] 
**modify_index** | **int** |  | [optional] 
**name** | **str** |  | [optional] 
**node_class** | **str** |  | [optional] 
**node_resources** | [**NodeResources**](NodeResources.md) |  | [optional] 
**reserved_resources** | [**NodeReservedResources**](NodeReservedResources.md) |  | [optional] 
**scheduling_eligibility** | **str** |  | [optional] 
**status** | **str** |  | [optional] 
**status_description** | **str** |  | [optional] 
**version** | **str** |  | [optional] 

## Example

```python
from nomad_generated.models.node_list_stub import NodeListStub

# TODO update the JSON string below
json = "{}"
# create an instance of NodeListStub from a JSON string
node_list_stub_instance = NodeListStub.from_json(json)
# print the JSON string representation of the object
print NodeListStub.to_json()

# convert the object into a dict
node_list_stub_dict = node_list_stub_instance.to_dict()
# create an instance of NodeListStub from a dict
node_list_stub_form_dict = node_list_stub.from_dict(node_list_stub_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


