# TaskEvent


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**details** | **Dict[str, str]** |  | [optional] 
**disk_limit** | **int** |  | [optional] 
**disk_size** | **int** |  | [optional] 
**display_message** | **str** |  | [optional] 
**download_error** | **str** |  | [optional] 
**driver_error** | **str** |  | [optional] 
**driver_message** | **str** |  | [optional] 
**exit_code** | **int** |  | [optional] 
**failed_sibling** | **str** |  | [optional] 
**fails_task** | **bool** |  | [optional] 
**generic_source** | **str** |  | [optional] 
**kill_error** | **str** |  | [optional] 
**kill_reason** | **str** |  | [optional] 
**kill_timeout** | **int** |  | [optional] 
**message** | **str** |  | [optional] 
**restart_reason** | **str** |  | [optional] 
**setup_error** | **str** |  | [optional] 
**signal** | **int** |  | [optional] 
**start_delay** | **int** |  | [optional] 
**task_signal** | **str** |  | [optional] 
**task_signal_reason** | **str** |  | [optional] 
**time** | **int** |  | [optional] 
**type** | **str** |  | [optional] 
**validation_error** | **str** |  | [optional] 
**vault_error** | **str** |  | [optional] 

## Example

```python
from nomad_generated.models.task_event import TaskEvent

# TODO update the JSON string below
json = "{}"
# create an instance of TaskEvent from a JSON string
task_event_instance = TaskEvent.from_json(json)
# print the JSON string representation of the object
print TaskEvent.to_json()

# convert the object into a dict
task_event_dict = task_event_instance.to_dict()
# create an instance of TaskEvent from a dict
task_event_form_dict = task_event.from_dict(task_event_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


