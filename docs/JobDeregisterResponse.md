# JobDeregisterResponse


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**eval_create_index** | **int** |  | [optional] 
**eval_id** | **str** |  | [optional] 
**job_modify_index** | **int** |  | [optional] 
**known_leader** | **bool** |  | [optional] 
**last_contact** | **int** |  | [optional] 
**last_index** | **int** |  | [optional] 
**next_token** | **str** |  | [optional] 
**request_time** | **int** |  | [optional] 

## Example

```python
from nomad_generated.models.job_deregister_response import JobDeregisterResponse

# TODO update the JSON string below
json = "{}"
# create an instance of JobDeregisterResponse from a JSON string
job_deregister_response_instance = JobDeregisterResponse.from_json(json)
# print the JSON string representation of the object
print JobDeregisterResponse.to_json()

# convert the object into a dict
job_deregister_response_dict = job_deregister_response_instance.to_dict()
# create an instance of JobDeregisterResponse from a dict
job_deregister_response_form_dict = job_deregister_response.from_dict(job_deregister_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


