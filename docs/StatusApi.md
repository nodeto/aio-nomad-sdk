# nomad_generated.StatusApi

All URIs are relative to *http://127.0.0.1:4646/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_status_leader**](StatusApi.md#get_status_leader) | **GET** /status/leader | 
[**get_status_peers**](StatusApi.md#get_status_peers) | **GET** /status/peers | 


# **get_status_leader**
> str get_status_leader(region=region, namespace=namespace, index=index, wait=wait, stale=stale, prefix=prefix, x_nomad_token=x_nomad_token, per_page=per_page, next_token=next_token)



### Example

* Api Key Authentication (X-Nomad-Token):
```python
import time
import os
import nomad_generated
from nomad_generated.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://127.0.0.1:4646/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = nomad_generated.Configuration(
    host = "http://127.0.0.1:4646/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: X-Nomad-Token
configuration.api_key['X-Nomad-Token'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['X-Nomad-Token'] = 'Bearer'

# Enter a context with an instance of the API client
async with nomad_generated.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = nomad_generated.StatusApi(api_client)
    region = 'region_example' # str | Filters results based on the specified region. (optional)
    namespace = 'namespace_example' # str | Filters results based on the specified namespace. (optional)
    index = 56 # int | If set, wait until query exceeds given index. Must be provided with WaitParam. (optional)
    wait = 'wait_example' # str | Provided with IndexParam to wait for change. (optional)
    stale = 'stale_example' # str | If present, results will include stale reads. (optional)
    prefix = 'prefix_example' # str | Constrains results to jobs that start with the defined prefix (optional)
    x_nomad_token = 'x_nomad_token_example' # str | A Nomad ACL token. (optional)
    per_page = 56 # int | Maximum number of results to return. (optional)
    next_token = 'next_token_example' # str | Indicates where to start paging for queries that support pagination. (optional)

    try:
        api_response = await api_instance.get_status_leader(region=region, namespace=namespace, index=index, wait=wait, stale=stale, prefix=prefix, x_nomad_token=x_nomad_token, per_page=per_page, next_token=next_token)
        print("The response of StatusApi->get_status_leader:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling StatusApi->get_status_leader: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **region** | **str**| Filters results based on the specified region. | [optional] 
 **namespace** | **str**| Filters results based on the specified namespace. | [optional] 
 **index** | **int**| If set, wait until query exceeds given index. Must be provided with WaitParam. | [optional] 
 **wait** | **str**| Provided with IndexParam to wait for change. | [optional] 
 **stale** | **str**| If present, results will include stale reads. | [optional] 
 **prefix** | **str**| Constrains results to jobs that start with the defined prefix | [optional] 
 **x_nomad_token** | **str**| A Nomad ACL token. | [optional] 
 **per_page** | **int**| Maximum number of results to return. | [optional] 
 **next_token** | **str**| Indicates where to start paging for queries that support pagination. | [optional] 

### Return type

**str**

### Authorization

[X-Nomad-Token](../README.md#X-Nomad-Token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |
**400** | Bad request |  -  |
**403** | Forbidden |  -  |
**405** | Method not allowed |  -  |
**500** | Internal server error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_status_peers**
> List[str] get_status_peers(region=region, namespace=namespace, index=index, wait=wait, stale=stale, prefix=prefix, x_nomad_token=x_nomad_token, per_page=per_page, next_token=next_token)



### Example

* Api Key Authentication (X-Nomad-Token):
```python
import time
import os
import nomad_generated
from nomad_generated.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://127.0.0.1:4646/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = nomad_generated.Configuration(
    host = "http://127.0.0.1:4646/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: X-Nomad-Token
configuration.api_key['X-Nomad-Token'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['X-Nomad-Token'] = 'Bearer'

# Enter a context with an instance of the API client
async with nomad_generated.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = nomad_generated.StatusApi(api_client)
    region = 'region_example' # str | Filters results based on the specified region. (optional)
    namespace = 'namespace_example' # str | Filters results based on the specified namespace. (optional)
    index = 56 # int | If set, wait until query exceeds given index. Must be provided with WaitParam. (optional)
    wait = 'wait_example' # str | Provided with IndexParam to wait for change. (optional)
    stale = 'stale_example' # str | If present, results will include stale reads. (optional)
    prefix = 'prefix_example' # str | Constrains results to jobs that start with the defined prefix (optional)
    x_nomad_token = 'x_nomad_token_example' # str | A Nomad ACL token. (optional)
    per_page = 56 # int | Maximum number of results to return. (optional)
    next_token = 'next_token_example' # str | Indicates where to start paging for queries that support pagination. (optional)

    try:
        api_response = await api_instance.get_status_peers(region=region, namespace=namespace, index=index, wait=wait, stale=stale, prefix=prefix, x_nomad_token=x_nomad_token, per_page=per_page, next_token=next_token)
        print("The response of StatusApi->get_status_peers:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling StatusApi->get_status_peers: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **region** | **str**| Filters results based on the specified region. | [optional] 
 **namespace** | **str**| Filters results based on the specified namespace. | [optional] 
 **index** | **int**| If set, wait until query exceeds given index. Must be provided with WaitParam. | [optional] 
 **wait** | **str**| Provided with IndexParam to wait for change. | [optional] 
 **stale** | **str**| If present, results will include stale reads. | [optional] 
 **prefix** | **str**| Constrains results to jobs that start with the defined prefix | [optional] 
 **x_nomad_token** | **str**| A Nomad ACL token. | [optional] 
 **per_page** | **int**| Maximum number of results to return. | [optional] 
 **next_token** | **str**| Indicates where to start paging for queries that support pagination. | [optional] 

### Return type

**List[str]**

### Authorization

[X-Nomad-Token](../README.md#X-Nomad-Token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |
**400** | Bad request |  -  |
**403** | Forbidden |  -  |
**405** | Method not allowed |  -  |
**500** | Internal server error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

