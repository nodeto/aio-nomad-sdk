# TaskGroupSummary


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**complete** | **int** |  | [optional] 
**failed** | **int** |  | [optional] 
**lost** | **int** |  | [optional] 
**queued** | **int** |  | [optional] 
**running** | **int** |  | [optional] 
**starting** | **int** |  | [optional] 
**unknown** | **int** |  | [optional] 

## Example

```python
from nomad_generated.models.task_group_summary import TaskGroupSummary

# TODO update the JSON string below
json = "{}"
# create an instance of TaskGroupSummary from a JSON string
task_group_summary_instance = TaskGroupSummary.from_json(json)
# print the JSON string representation of the object
print TaskGroupSummary.to_json()

# convert the object into a dict
task_group_summary_dict = task_group_summary_instance.to_dict()
# create an instance of TaskGroupSummary from a dict
task_group_summary_form_dict = task_group_summary.from_dict(task_group_summary_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


