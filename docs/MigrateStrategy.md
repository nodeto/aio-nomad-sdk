# MigrateStrategy


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**health_check** | **str** |  | [optional] 
**healthy_deadline** | **int** |  | [optional] 
**max_parallel** | **int** |  | [optional] 
**min_healthy_time** | **int** |  | [optional] 

## Example

```python
from nomad_generated.models.migrate_strategy import MigrateStrategy

# TODO update the JSON string below
json = "{}"
# create an instance of MigrateStrategy from a JSON string
migrate_strategy_instance = MigrateStrategy.from_json(json)
# print the JSON string representation of the object
print MigrateStrategy.to_json()

# convert the object into a dict
migrate_strategy_dict = migrate_strategy_instance.to_dict()
# create an instance of MigrateStrategy from a dict
migrate_strategy_form_dict = migrate_strategy.from_dict(migrate_strategy_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


