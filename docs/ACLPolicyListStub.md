# ACLPolicyListStub


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**create_index** | **int** |  | [optional] 
**description** | **str** |  | [optional] 
**modify_index** | **int** |  | [optional] 
**name** | **str** |  | [optional] 

## Example

```python
from nomad_generated.models.acl_policy_list_stub import ACLPolicyListStub

# TODO update the JSON string below
json = "{}"
# create an instance of ACLPolicyListStub from a JSON string
acl_policy_list_stub_instance = ACLPolicyListStub.from_json(json)
# print the JSON string representation of the object
print ACLPolicyListStub.to_json()

# convert the object into a dict
acl_policy_list_stub_dict = acl_policy_list_stub_instance.to_dict()
# create an instance of ACLPolicyListStub from a dict
acl_policy_list_stub_form_dict = acl_policy_list_stub.from_dict(acl_policy_list_stub_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


