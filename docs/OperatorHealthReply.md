# OperatorHealthReply


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**failure_tolerance** | **int** |  | [optional] 
**healthy** | **bool** |  | [optional] 
**servers** | [**List[ServerHealth]**](ServerHealth.md) |  | [optional] 

## Example

```python
from nomad_generated.models.operator_health_reply import OperatorHealthReply

# TODO update the JSON string below
json = "{}"
# create an instance of OperatorHealthReply from a JSON string
operator_health_reply_instance = OperatorHealthReply.from_json(json)
# print the JSON string representation of the object
print OperatorHealthReply.to_json()

# convert the object into a dict
operator_health_reply_dict = operator_health_reply_instance.to_dict()
# create an instance of OperatorHealthReply from a dict
operator_health_reply_form_dict = operator_health_reply.from_dict(operator_health_reply_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


