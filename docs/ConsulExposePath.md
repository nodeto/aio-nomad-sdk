# ConsulExposePath


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**listener_port** | **str** |  | [optional] 
**local_path_port** | **int** |  | [optional] 
**path** | **str** |  | [optional] 
**protocol** | **str** |  | [optional] 

## Example

```python
from nomad_generated.models.consul_expose_path import ConsulExposePath

# TODO update the JSON string below
json = "{}"
# create an instance of ConsulExposePath from a JSON string
consul_expose_path_instance = ConsulExposePath.from_json(json)
# print the JSON string representation of the object
print ConsulExposePath.to_json()

# convert the object into a dict
consul_expose_path_dict = consul_expose_path_instance.to_dict()
# create an instance of ConsulExposePath from a dict
consul_expose_path_form_dict = consul_expose_path.from_dict(consul_expose_path_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


