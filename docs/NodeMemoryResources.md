# NodeMemoryResources


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**memory_mb** | **int** |  | [optional] 

## Example

```python
from nomad_generated.models.node_memory_resources import NodeMemoryResources

# TODO update the JSON string below
json = "{}"
# create an instance of NodeMemoryResources from a JSON string
node_memory_resources_instance = NodeMemoryResources.from_json(json)
# print the JSON string representation of the object
print NodeMemoryResources.to_json()

# convert the object into a dict
node_memory_resources_dict = node_memory_resources_instance.to_dict()
# create an instance of NodeMemoryResources from a dict
node_memory_resources_form_dict = node_memory_resources.from_dict(node_memory_resources_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


