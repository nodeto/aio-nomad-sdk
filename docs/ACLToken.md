# ACLToken


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**accessor_id** | **str** |  | [optional] 
**create_index** | **int** |  | [optional] 
**create_time** | **datetime** |  | [optional] 
**expiration_ttl** | **int** |  | [optional] 
**expiration_time** | **datetime** |  | [optional] 
**var_global** | **bool** |  | [optional] 
**modify_index** | **int** |  | [optional] 
**name** | **str** |  | [optional] 
**policies** | **List[str]** |  | [optional] 
**roles** | [**List[ACLTokenRoleLink]**](ACLTokenRoleLink.md) |  | [optional] 
**secret_id** | **str** |  | [optional] 
**type** | **str** |  | [optional] 

## Example

```python
from nomad_generated.models.acl_token import ACLToken

# TODO update the JSON string below
json = "{}"
# create an instance of ACLToken from a JSON string
acl_token_instance = ACLToken.from_json(json)
# print the JSON string representation of the object
print ACLToken.to_json()

# convert the object into a dict
acl_token_dict = acl_token_instance.to_dict()
# create an instance of ACLToken from a dict
acl_token_form_dict = acl_token.from_dict(acl_token_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


