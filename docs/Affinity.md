# Affinity


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**l_target** | **str** |  | [optional] 
**operand** | **str** |  | [optional] 
**r_target** | **str** |  | [optional] 
**weight** | **int** |  | [optional] 

## Example

```python
from nomad_generated.models.affinity import Affinity

# TODO update the JSON string below
json = "{}"
# create an instance of Affinity from a JSON string
affinity_instance = Affinity.from_json(json)
# print the JSON string representation of the object
print Affinity.to_json()

# convert the object into a dict
affinity_dict = affinity_instance.to_dict()
# create an instance of Affinity from a dict
affinity_form_dict = affinity.from_dict(affinity_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


