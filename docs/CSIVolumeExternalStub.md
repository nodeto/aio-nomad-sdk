# CSIVolumeExternalStub


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**capacity_bytes** | **int** |  | [optional] 
**clone_id** | **str** |  | [optional] 
**external_id** | **str** |  | [optional] 
**is_abnormal** | **bool** |  | [optional] 
**published_external_node_ids** | **List[str]** |  | [optional] 
**snapshot_id** | **str** |  | [optional] 
**status** | **str** |  | [optional] 
**volume_context** | **Dict[str, str]** |  | [optional] 

## Example

```python
from nomad_generated.models.csi_volume_external_stub import CSIVolumeExternalStub

# TODO update the JSON string below
json = "{}"
# create an instance of CSIVolumeExternalStub from a JSON string
csi_volume_external_stub_instance = CSIVolumeExternalStub.from_json(json)
# print the JSON string representation of the object
print CSIVolumeExternalStub.to_json()

# convert the object into a dict
csi_volume_external_stub_dict = csi_volume_external_stub_instance.to_dict()
# create an instance of CSIVolumeExternalStub from a dict
csi_volume_external_stub_form_dict = csi_volume_external_stub.from_dict(csi_volume_external_stub_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


