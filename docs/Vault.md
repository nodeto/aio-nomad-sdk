# Vault


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**change_mode** | **str** |  | [optional] 
**change_signal** | **str** |  | [optional] 
**env** | **bool** |  | [optional] 
**namespace** | **str** |  | [optional] 
**policies** | **List[str]** |  | [optional] 

## Example

```python
from nomad_generated.models.vault import Vault

# TODO update the JSON string below
json = "{}"
# create an instance of Vault from a JSON string
vault_instance = Vault.from_json(json)
# print the JSON string representation of the object
print Vault.to_json()

# convert the object into a dict
vault_dict = vault_instance.to_dict()
# create an instance of Vault from a dict
vault_form_dict = vault.from_dict(vault_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


