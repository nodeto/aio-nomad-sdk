# ConsulIngressListener


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**port** | **int** |  | [optional] 
**protocol** | **str** |  | [optional] 
**services** | [**List[ConsulIngressService]**](ConsulIngressService.md) |  | [optional] 

## Example

```python
from nomad_generated.models.consul_ingress_listener import ConsulIngressListener

# TODO update the JSON string below
json = "{}"
# create an instance of ConsulIngressListener from a JSON string
consul_ingress_listener_instance = ConsulIngressListener.from_json(json)
# print the JSON string representation of the object
print ConsulIngressListener.to_json()

# convert the object into a dict
consul_ingress_listener_dict = consul_ingress_listener_instance.to_dict()
# create an instance of ConsulIngressListener from a dict
consul_ingress_listener_form_dict = consul_ingress_listener.from_dict(consul_ingress_listener_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


