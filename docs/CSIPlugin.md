# CSIPlugin


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**allocations** | [**List[AllocationListStub]**](AllocationListStub.md) |  | [optional] 
**controller_required** | **bool** |  | [optional] 
**controllers** | [**Dict[str, CSIInfo]**](CSIInfo.md) |  | [optional] 
**controllers_expected** | **int** |  | [optional] 
**controllers_healthy** | **int** |  | [optional] 
**create_index** | **int** |  | [optional] 
**id** | **str** |  | [optional] 
**modify_index** | **int** |  | [optional] 
**nodes** | [**Dict[str, CSIInfo]**](CSIInfo.md) |  | [optional] 
**nodes_expected** | **int** |  | [optional] 
**nodes_healthy** | **int** |  | [optional] 
**provider** | **str** |  | [optional] 
**version** | **str** |  | [optional] 

## Example

```python
from nomad_generated.models.csi_plugin import CSIPlugin

# TODO update the JSON string below
json = "{}"
# create an instance of CSIPlugin from a JSON string
csi_plugin_instance = CSIPlugin.from_json(json)
# print the JSON string representation of the object
print CSIPlugin.to_json()

# convert the object into a dict
csi_plugin_dict = csi_plugin_instance.to_dict()
# create an instance of CSIPlugin from a dict
csi_plugin_form_dict = csi_plugin.from_dict(csi_plugin_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


