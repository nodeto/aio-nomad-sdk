# ConsulSidecarService


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**disable_default_tcp_check** | **bool** |  | [optional] 
**port** | **str** |  | [optional] 
**proxy** | [**ConsulProxy**](ConsulProxy.md) |  | [optional] 
**tags** | **List[str]** |  | [optional] 

## Example

```python
from nomad_generated.models.consul_sidecar_service import ConsulSidecarService

# TODO update the JSON string below
json = "{}"
# create an instance of ConsulSidecarService from a JSON string
consul_sidecar_service_instance = ConsulSidecarService.from_json(json)
# print the JSON string representation of the object
print ConsulSidecarService.to_json()

# convert the object into a dict
consul_sidecar_service_dict = consul_sidecar_service_instance.to_dict()
# create an instance of ConsulSidecarService from a dict
consul_sidecar_service_form_dict = consul_sidecar_service.from_dict(consul_sidecar_service_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


