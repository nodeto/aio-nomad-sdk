# NodeDeviceResource


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**attributes** | [**Dict[str, Attribute]**](Attribute.md) |  | [optional] 
**instances** | [**List[NodeDevice]**](NodeDevice.md) |  | [optional] 
**name** | **str** |  | [optional] 
**type** | **str** |  | [optional] 
**vendor** | **str** |  | [optional] 

## Example

```python
from nomad_generated.models.node_device_resource import NodeDeviceResource

# TODO update the JSON string below
json = "{}"
# create an instance of NodeDeviceResource from a JSON string
node_device_resource_instance = NodeDeviceResource.from_json(json)
# print the JSON string representation of the object
print NodeDeviceResource.to_json()

# convert the object into a dict
node_device_resource_dict = node_device_resource_instance.to_dict()
# create an instance of NodeDeviceResource from a dict
node_device_resource_form_dict = node_device_resource.from_dict(node_device_resource_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


