# DesiredTransition


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**migrate** | **bool** |  | [optional] 
**reschedule** | **bool** |  | [optional] 

## Example

```python
from nomad_generated.models.desired_transition import DesiredTransition

# TODO update the JSON string below
json = "{}"
# create an instance of DesiredTransition from a JSON string
desired_transition_instance = DesiredTransition.from_json(json)
# print the JSON string representation of the object
print DesiredTransition.to_json()

# convert the object into a dict
desired_transition_dict = desired_transition_instance.to_dict()
# create an instance of DesiredTransition from a dict
desired_transition_form_dict = desired_transition.from_dict(desired_transition_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


