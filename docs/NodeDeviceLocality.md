# NodeDeviceLocality


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**pci_bus_id** | **str** |  | [optional] 

## Example

```python
from nomad_generated.models.node_device_locality import NodeDeviceLocality

# TODO update the JSON string below
json = "{}"
# create an instance of NodeDeviceLocality from a JSON string
node_device_locality_instance = NodeDeviceLocality.from_json(json)
# print the JSON string representation of the object
print NodeDeviceLocality.to_json()

# convert the object into a dict
node_device_locality_dict = node_device_locality_instance.to_dict()
# create an instance of NodeDeviceLocality from a dict
node_device_locality_form_dict = node_device_locality.from_dict(node_device_locality_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


