# DeploymentAllocHealthRequest


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**deployment_id** | **str** |  | [optional] 
**healthy_allocation_ids** | **List[str]** |  | [optional] 
**namespace** | **str** |  | [optional] 
**region** | **str** |  | [optional] 
**secret_id** | **str** |  | [optional] 
**unhealthy_allocation_ids** | **List[str]** |  | [optional] 

## Example

```python
from nomad_generated.models.deployment_alloc_health_request import DeploymentAllocHealthRequest

# TODO update the JSON string below
json = "{}"
# create an instance of DeploymentAllocHealthRequest from a JSON string
deployment_alloc_health_request_instance = DeploymentAllocHealthRequest.from_json(json)
# print the JSON string representation of the object
print DeploymentAllocHealthRequest.to_json()

# convert the object into a dict
deployment_alloc_health_request_dict = deployment_alloc_health_request_instance.to_dict()
# create an instance of DeploymentAllocHealthRequest from a dict
deployment_alloc_health_request_form_dict = deployment_alloc_health_request.from_dict(deployment_alloc_health_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


