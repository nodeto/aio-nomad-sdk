# PeriodicConfig


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**enabled** | **bool** |  | [optional] 
**prohibit_overlap** | **bool** |  | [optional] 
**spec** | **str** |  | [optional] 
**spec_type** | **str** |  | [optional] 
**time_zone** | **str** |  | [optional] 

## Example

```python
from nomad_generated.models.periodic_config import PeriodicConfig

# TODO update the JSON string below
json = "{}"
# create an instance of PeriodicConfig from a JSON string
periodic_config_instance = PeriodicConfig.from_json(json)
# print the JSON string representation of the object
print PeriodicConfig.to_json()

# convert the object into a dict
periodic_config_dict = periodic_config_instance.to_dict()
# create an instance of PeriodicConfig from a dict
periodic_config_form_dict = periodic_config.from_dict(periodic_config_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


