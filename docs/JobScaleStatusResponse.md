# JobScaleStatusResponse


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**job_create_index** | **int** |  | [optional] 
**job_id** | **str** |  | [optional] 
**job_modify_index** | **int** |  | [optional] 
**job_stopped** | **bool** |  | [optional] 
**namespace** | **str** |  | [optional] 
**task_groups** | [**Dict[str, TaskGroupScaleStatus]**](TaskGroupScaleStatus.md) |  | [optional] 

## Example

```python
from nomad_generated.models.job_scale_status_response import JobScaleStatusResponse

# TODO update the JSON string below
json = "{}"
# create an instance of JobScaleStatusResponse from a JSON string
job_scale_status_response_instance = JobScaleStatusResponse.from_json(json)
# print the JSON string representation of the object
print JobScaleStatusResponse.to_json()

# convert the object into a dict
job_scale_status_response_dict = job_scale_status_response_instance.to_dict()
# create an instance of JobScaleStatusResponse from a dict
job_scale_status_response_form_dict = job_scale_status_response.from_dict(job_scale_status_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


