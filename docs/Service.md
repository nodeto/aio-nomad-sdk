# Service


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**address** | **str** |  | [optional] 
**address_mode** | **str** |  | [optional] 
**canary_meta** | **Dict[str, str]** |  | [optional] 
**canary_tags** | **List[str]** |  | [optional] 
**check_restart** | [**CheckRestart**](CheckRestart.md) |  | [optional] 
**checks** | [**List[ServiceCheck]**](ServiceCheck.md) |  | [optional] 
**connect** | [**ConsulConnect**](ConsulConnect.md) |  | [optional] 
**enable_tag_override** | **bool** |  | [optional] 
**meta** | **Dict[str, str]** |  | [optional] 
**name** | **str** |  | [optional] 
**on_update** | **str** |  | [optional] 
**port_label** | **str** |  | [optional] 
**provider** | **str** |  | [optional] 
**tagged_addresses** | **Dict[str, str]** |  | [optional] 
**tags** | **List[str]** |  | [optional] 
**task_name** | **str** |  | [optional] 

## Example

```python
from nomad_generated.models.service import Service

# TODO update the JSON string below
json = "{}"
# create an instance of Service from a JSON string
service_instance = Service.from_json(json)
# print the JSON string representation of the object
print Service.to_json()

# convert the object into a dict
service_dict = service_instance.to_dict()
# create an instance of Service from a dict
service_form_dict = service.from_dict(service_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


