# ScalingEvent


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**count** | **int** |  | [optional] 
**create_index** | **int** |  | [optional] 
**error** | **bool** |  | [optional] 
**eval_id** | **str** |  | [optional] 
**message** | **str** |  | [optional] 
**meta** | **Dict[str, object]** |  | [optional] 
**previous_count** | **int** |  | [optional] 
**time** | **int** |  | [optional] 

## Example

```python
from nomad_generated.models.scaling_event import ScalingEvent

# TODO update the JSON string below
json = "{}"
# create an instance of ScalingEvent from a JSON string
scaling_event_instance = ScalingEvent.from_json(json)
# print the JSON string representation of the object
print ScalingEvent.to_json()

# convert the object into a dict
scaling_event_dict = scaling_event_instance.to_dict()
# create an instance of ScalingEvent from a dict
scaling_event_form_dict = scaling_event.from_dict(scaling_event_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


