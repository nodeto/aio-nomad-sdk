# SampledValue


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**count** | **int** |  | [optional] 
**labels** | **Dict[str, str]** |  | [optional] 
**max** | **float** |  | [optional] 
**mean** | **float** |  | [optional] 
**min** | **float** |  | [optional] 
**name** | **str** |  | [optional] 
**rate** | **float** |  | [optional] 
**stddev** | **float** |  | [optional] 
**sum** | **float** |  | [optional] 

## Example

```python
from nomad_generated.models.sampled_value import SampledValue

# TODO update the JSON string below
json = "{}"
# create an instance of SampledValue from a JSON string
sampled_value_instance = SampledValue.from_json(json)
# print the JSON string representation of the object
print SampledValue.to_json()

# convert the object into a dict
sampled_value_dict = sampled_value_instance.to_dict()
# create an instance of SampledValue from a dict
sampled_value_form_dict = sampled_value.from_dict(sampled_value_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


