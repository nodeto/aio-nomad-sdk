# AllocatedSharedResources


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**disk_mb** | **int** |  | [optional] 
**networks** | [**List[NetworkResource]**](NetworkResource.md) |  | [optional] 
**ports** | [**List[PortMapping]**](PortMapping.md) |  | [optional] 

## Example

```python
from nomad_generated.models.allocated_shared_resources import AllocatedSharedResources

# TODO update the JSON string below
json = "{}"
# create an instance of AllocatedSharedResources from a JSON string
allocated_shared_resources_instance = AllocatedSharedResources.from_json(json)
# print the JSON string representation of the object
print AllocatedSharedResources.to_json()

# convert the object into a dict
allocated_shared_resources_dict = allocated_shared_resources_instance.to_dict()
# create an instance of AllocatedSharedResources from a dict
allocated_shared_resources_form_dict = allocated_shared_resources.from_dict(allocated_shared_resources_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


