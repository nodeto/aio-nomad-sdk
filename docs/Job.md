# Job


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**affinities** | [**List[Affinity]**](Affinity.md) |  | [optional] 
**all_at_once** | **bool** |  | [optional] 
**constraints** | [**List[Constraint]**](Constraint.md) |  | [optional] 
**consul_namespace** | **str** |  | [optional] 
**consul_token** | **str** |  | [optional] 
**create_index** | **int** |  | [optional] 
**datacenters** | **List[str]** |  | [optional] 
**dispatch_idempotency_token** | **str** |  | [optional] 
**dispatched** | **bool** |  | [optional] 
**id** | **str** |  | [optional] 
**job_modify_index** | **int** |  | [optional] 
**meta** | **Dict[str, str]** |  | [optional] 
**migrate** | [**MigrateStrategy**](MigrateStrategy.md) |  | [optional] 
**modify_index** | **int** |  | [optional] 
**multiregion** | [**Multiregion**](Multiregion.md) |  | [optional] 
**name** | **str** |  | [optional] 
**namespace** | **str** |  | [optional] 
**nomad_token_id** | **str** |  | [optional] 
**parameterized_job** | [**ParameterizedJobConfig**](ParameterizedJobConfig.md) |  | [optional] 
**parent_id** | **str** |  | [optional] 
**payload** | **bytearray** |  | [optional] 
**periodic** | [**PeriodicConfig**](PeriodicConfig.md) |  | [optional] 
**priority** | **int** |  | [optional] 
**region** | **str** |  | [optional] 
**reschedule** | [**ReschedulePolicy**](ReschedulePolicy.md) |  | [optional] 
**spreads** | [**List[Spread]**](Spread.md) |  | [optional] 
**stable** | **bool** |  | [optional] 
**status** | **str** |  | [optional] 
**status_description** | **str** |  | [optional] 
**stop** | **bool** |  | [optional] 
**submit_time** | **int** |  | [optional] 
**task_groups** | [**List[TaskGroup]**](TaskGroup.md) |  | [optional] 
**type** | **str** |  | [optional] 
**update** | [**UpdateStrategy**](UpdateStrategy.md) |  | [optional] 
**vault_namespace** | **str** |  | [optional] 
**vault_token** | **str** |  | [optional] 
**version** | **int** |  | [optional] 

## Example

```python
from nomad_generated.models.job import Job

# TODO update the JSON string below
json = "{}"
# create an instance of Job from a JSON string
job_instance = Job.from_json(json)
# print the JSON string representation of the object
print Job.to_json()

# convert the object into a dict
job_dict = job_instance.to_dict()
# create an instance of Job from a dict
job_form_dict = job.from_dict(job_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


