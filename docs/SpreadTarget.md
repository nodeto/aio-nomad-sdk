# SpreadTarget


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**percent** | **int** |  | [optional] 
**value** | **str** |  | [optional] 

## Example

```python
from nomad_generated.models.spread_target import SpreadTarget

# TODO update the JSON string below
json = "{}"
# create an instance of SpreadTarget from a JSON string
spread_target_instance = SpreadTarget.from_json(json)
# print the JSON string representation of the object
print SpreadTarget.to_json()

# convert the object into a dict
spread_target_dict = spread_target_instance.to_dict()
# create an instance of SpreadTarget from a dict
spread_target_form_dict = spread_target.from_dict(spread_target_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


