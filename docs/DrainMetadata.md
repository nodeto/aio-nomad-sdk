# DrainMetadata


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**accessor_id** | **str** |  | [optional] 
**meta** | **Dict[str, str]** |  | [optional] 
**started_at** | **datetime** |  | [optional] 
**status** | **str** |  | [optional] 
**updated_at** | **datetime** |  | [optional] 

## Example

```python
from nomad_generated.models.drain_metadata import DrainMetadata

# TODO update the JSON string below
json = "{}"
# create an instance of DrainMetadata from a JSON string
drain_metadata_instance = DrainMetadata.from_json(json)
# print the JSON string representation of the object
print DrainMetadata.to_json()

# convert the object into a dict
drain_metadata_dict = drain_metadata_instance.to_dict()
# create an instance of DrainMetadata from a dict
drain_metadata_form_dict = drain_metadata.from_dict(drain_metadata_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


