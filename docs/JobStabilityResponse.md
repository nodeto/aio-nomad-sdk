# JobStabilityResponse


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**index** | **int** |  | [optional] 

## Example

```python
from nomad_generated.models.job_stability_response import JobStabilityResponse

# TODO update the JSON string below
json = "{}"
# create an instance of JobStabilityResponse from a JSON string
job_stability_response_instance = JobStabilityResponse.from_json(json)
# print the JSON string representation of the object
print JobStabilityResponse.to_json()

# convert the object into a dict
job_stability_response_dict = job_stability_response_instance.to_dict()
# create an instance of JobStabilityResponse from a dict
job_stability_response_form_dict = job_stability_response.from_dict(job_stability_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


