# JobRegisterRequest


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**enforce_index** | **bool** |  | [optional] 
**eval_priority** | **int** |  | [optional] 
**job** | [**Job**](Job.md) |  | [optional] 
**job_modify_index** | **int** |  | [optional] 
**namespace** | **str** |  | [optional] 
**policy_override** | **bool** |  | [optional] 
**preserve_counts** | **bool** |  | [optional] 
**region** | **str** |  | [optional] 
**secret_id** | **str** |  | [optional] 

## Example

```python
from nomad_generated.models.job_register_request import JobRegisterRequest

# TODO update the JSON string below
json = "{}"
# create an instance of JobRegisterRequest from a JSON string
job_register_request_instance = JobRegisterRequest.from_json(json)
# print the JSON string representation of the object
print JobRegisterRequest.to_json()

# convert the object into a dict
job_register_request_dict = job_register_request_instance.to_dict()
# create an instance of JobRegisterRequest from a dict
job_register_request_form_dict = job_register_request.from_dict(job_register_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


