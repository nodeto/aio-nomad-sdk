# Node


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**attributes** | **Dict[str, str]** |  | [optional] 
**csi_controller_plugins** | [**Dict[str, CSIInfo]**](CSIInfo.md) |  | [optional] 
**csi_node_plugins** | [**Dict[str, CSIInfo]**](CSIInfo.md) |  | [optional] 
**cgroup_parent** | **str** |  | [optional] 
**create_index** | **int** |  | [optional] 
**datacenter** | **str** |  | [optional] 
**drain** | **bool** |  | [optional] 
**drain_strategy** | [**DrainStrategy**](DrainStrategy.md) |  | [optional] 
**drivers** | [**Dict[str, DriverInfo]**](DriverInfo.md) |  | [optional] 
**events** | [**List[NodeEvent]**](NodeEvent.md) |  | [optional] 
**http_addr** | **str** |  | [optional] 
**host_networks** | [**Dict[str, HostNetworkInfo]**](HostNetworkInfo.md) |  | [optional] 
**host_volumes** | [**Dict[str, HostVolumeInfo]**](HostVolumeInfo.md) |  | [optional] 
**id** | **str** |  | [optional] 
**last_drain** | [**DrainMetadata**](DrainMetadata.md) |  | [optional] 
**links** | **Dict[str, str]** |  | [optional] 
**meta** | **Dict[str, str]** |  | [optional] 
**modify_index** | **int** |  | [optional] 
**name** | **str** |  | [optional] 
**node_class** | **str** |  | [optional] 
**node_resources** | [**NodeResources**](NodeResources.md) |  | [optional] 
**reserved** | [**Resources**](Resources.md) |  | [optional] 
**reserved_resources** | [**NodeReservedResources**](NodeReservedResources.md) |  | [optional] 
**resources** | [**Resources**](Resources.md) |  | [optional] 
**scheduling_eligibility** | **str** |  | [optional] 
**status** | **str** |  | [optional] 
**status_description** | **str** |  | [optional] 
**status_updated_at** | **int** |  | [optional] 
**tls_enabled** | **bool** |  | [optional] 

## Example

```python
from nomad_generated.models.node import Node

# TODO update the JSON string below
json = "{}"
# create an instance of Node from a JSON string
node_instance = Node.from_json(json)
# print the JSON string representation of the object
print Node.to_json()

# convert the object into a dict
node_dict = node_instance.to_dict()
# create an instance of Node from a dict
node_form_dict = node.from_dict(node_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


