# TaskDiff


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**annotations** | **List[str]** |  | [optional] 
**fields** | [**List[FieldDiff]**](FieldDiff.md) |  | [optional] 
**name** | **str** |  | [optional] 
**objects** | [**List[ObjectDiff]**](ObjectDiff.md) |  | [optional] 
**type** | **str** |  | [optional] 

## Example

```python
from nomad_generated.models.task_diff import TaskDiff

# TODO update the JSON string below
json = "{}"
# create an instance of TaskDiff from a JSON string
task_diff_instance = TaskDiff.from_json(json)
# print the JSON string representation of the object
print TaskDiff.to_json()

# convert the object into a dict
task_diff_dict = task_diff_instance.to_dict()
# create an instance of TaskDiff from a dict
task_diff_form_dict = task_diff.from_dict(task_diff_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


