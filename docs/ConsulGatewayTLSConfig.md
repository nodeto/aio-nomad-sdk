# ConsulGatewayTLSConfig


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cipher_suites** | **List[str]** |  | [optional] 
**enabled** | **bool** |  | [optional] 
**tls_max_version** | **str** |  | [optional] 
**tls_min_version** | **str** |  | [optional] 

## Example

```python
from nomad_generated.models.consul_gateway_tls_config import ConsulGatewayTLSConfig

# TODO update the JSON string below
json = "{}"
# create an instance of ConsulGatewayTLSConfig from a JSON string
consul_gateway_tls_config_instance = ConsulGatewayTLSConfig.from_json(json)
# print the JSON string representation of the object
print ConsulGatewayTLSConfig.to_json()

# convert the object into a dict
consul_gateway_tls_config_dict = consul_gateway_tls_config_instance.to_dict()
# create an instance of ConsulGatewayTLSConfig from a dict
consul_gateway_tls_config_form_dict = consul_gateway_tls_config.from_dict(consul_gateway_tls_config_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


