# ObjectDiff


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**fields** | [**List[FieldDiff]**](FieldDiff.md) |  | [optional] 
**name** | **str** |  | [optional] 
**objects** | [**List[ObjectDiff]**](ObjectDiff.md) |  | [optional] 
**type** | **str** |  | [optional] 

## Example

```python
from nomad_generated.models.object_diff import ObjectDiff

# TODO update the JSON string below
json = "{}"
# create an instance of ObjectDiff from a JSON string
object_diff_instance = ObjectDiff.from_json(json)
# print the JSON string representation of the object
print ObjectDiff.to_json()

# convert the object into a dict
object_diff_dict = object_diff_instance.to_dict()
# create an instance of ObjectDiff from a dict
object_diff_form_dict = object_diff.from_dict(object_diff_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


