# EvaluationStub


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**blocked_eval** | **str** |  | [optional] 
**create_index** | **int** |  | [optional] 
**create_time** | **int** |  | [optional] 
**deployment_id** | **str** |  | [optional] 
**id** | **str** |  | [optional] 
**job_id** | **str** |  | [optional] 
**modify_index** | **int** |  | [optional] 
**modify_time** | **int** |  | [optional] 
**namespace** | **str** |  | [optional] 
**next_eval** | **str** |  | [optional] 
**node_id** | **str** |  | [optional] 
**previous_eval** | **str** |  | [optional] 
**priority** | **int** |  | [optional] 
**status** | **str** |  | [optional] 
**status_description** | **str** |  | [optional] 
**triggered_by** | **str** |  | [optional] 
**type** | **str** |  | [optional] 
**wait_until** | **datetime** |  | [optional] 

## Example

```python
from nomad_generated.models.evaluation_stub import EvaluationStub

# TODO update the JSON string below
json = "{}"
# create an instance of EvaluationStub from a JSON string
evaluation_stub_instance = EvaluationStub.from_json(json)
# print the JSON string representation of the object
print EvaluationStub.to_json()

# convert the object into a dict
evaluation_stub_dict = evaluation_stub_instance.to_dict()
# create an instance of EvaluationStub from a dict
evaluation_stub_form_dict = evaluation_stub.from_dict(evaluation_stub_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


