# ConsulProxy


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**config** | **Dict[str, object]** |  | [optional] 
**expose_config** | [**ConsulExposeConfig**](ConsulExposeConfig.md) |  | [optional] 
**local_service_address** | **str** |  | [optional] 
**local_service_port** | **int** |  | [optional] 
**upstreams** | [**List[ConsulUpstream]**](ConsulUpstream.md) |  | [optional] 

## Example

```python
from nomad_generated.models.consul_proxy import ConsulProxy

# TODO update the JSON string below
json = "{}"
# create an instance of ConsulProxy from a JSON string
consul_proxy_instance = ConsulProxy.from_json(json)
# print the JSON string representation of the object
print ConsulProxy.to_json()

# convert the object into a dict
consul_proxy_dict = consul_proxy_instance.to_dict()
# create an instance of ConsulProxy from a dict
consul_proxy_form_dict = consul_proxy.from_dict(consul_proxy_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


