# FuzzyMatch


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** |  | [optional] 
**scope** | **List[str]** |  | [optional] 

## Example

```python
from nomad_generated.models.fuzzy_match import FuzzyMatch

# TODO update the JSON string below
json = "{}"
# create an instance of FuzzyMatch from a JSON string
fuzzy_match_instance = FuzzyMatch.from_json(json)
# print the JSON string representation of the object
print FuzzyMatch.to_json()

# convert the object into a dict
fuzzy_match_dict = fuzzy_match_instance.to_dict()
# create an instance of FuzzyMatch from a dict
fuzzy_match_form_dict = fuzzy_match.from_dict(fuzzy_match_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


