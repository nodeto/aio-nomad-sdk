# AllocatedTaskResources


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cpu** | [**AllocatedCpuResources**](AllocatedCpuResources.md) |  | [optional] 
**devices** | [**List[AllocatedDeviceResource]**](AllocatedDeviceResource.md) |  | [optional] 
**memory** | [**AllocatedMemoryResources**](AllocatedMemoryResources.md) |  | [optional] 
**networks** | [**List[NetworkResource]**](NetworkResource.md) |  | [optional] 

## Example

```python
from nomad_generated.models.allocated_task_resources import AllocatedTaskResources

# TODO update the JSON string below
json = "{}"
# create an instance of AllocatedTaskResources from a JSON string
allocated_task_resources_instance = AllocatedTaskResources.from_json(json)
# print the JSON string representation of the object
print AllocatedTaskResources.to_json()

# convert the object into a dict
allocated_task_resources_dict = allocated_task_resources_instance.to_dict()
# create an instance of AllocatedTaskResources from a dict
allocated_task_resources_form_dict = allocated_task_resources.from_dict(allocated_task_resources_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


