# ConsulGateway


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ingress** | [**ConsulIngressConfigEntry**](ConsulIngressConfigEntry.md) |  | [optional] 
**mesh** | **object** |  | [optional] 
**proxy** | [**ConsulGatewayProxy**](ConsulGatewayProxy.md) |  | [optional] 
**terminating** | [**ConsulTerminatingConfigEntry**](ConsulTerminatingConfigEntry.md) |  | [optional] 

## Example

```python
from nomad_generated.models.consul_gateway import ConsulGateway

# TODO update the JSON string below
json = "{}"
# create an instance of ConsulGateway from a JSON string
consul_gateway_instance = ConsulGateway.from_json(json)
# print the JSON string representation of the object
print ConsulGateway.to_json()

# convert the object into a dict
consul_gateway_dict = consul_gateway_instance.to_dict()
# create an instance of ConsulGateway from a dict
consul_gateway_form_dict = consul_gateway.from_dict(consul_gateway_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


