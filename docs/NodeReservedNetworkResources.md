# NodeReservedNetworkResources


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**reserved_host_ports** | **str** |  | [optional] 

## Example

```python
from nomad_generated.models.node_reserved_network_resources import NodeReservedNetworkResources

# TODO update the JSON string below
json = "{}"
# create an instance of NodeReservedNetworkResources from a JSON string
node_reserved_network_resources_instance = NodeReservedNetworkResources.from_json(json)
# print the JSON string representation of the object
print NodeReservedNetworkResources.to_json()

# convert the object into a dict
node_reserved_network_resources_dict = node_reserved_network_resources_instance.to_dict()
# create an instance of NodeReservedNetworkResources from a dict
node_reserved_network_resources_form_dict = node_reserved_network_resources.from_dict(node_reserved_network_resources_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


