# OneTimeTokenExchangeRequest


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**one_time_secret_id** | **str** |  | [optional] 

## Example

```python
from nomad_generated.models.one_time_token_exchange_request import OneTimeTokenExchangeRequest

# TODO update the JSON string below
json = "{}"
# create an instance of OneTimeTokenExchangeRequest from a JSON string
one_time_token_exchange_request_instance = OneTimeTokenExchangeRequest.from_json(json)
# print the JSON string representation of the object
print OneTimeTokenExchangeRequest.to_json()

# convert the object into a dict
one_time_token_exchange_request_dict = one_time_token_exchange_request_instance.to_dict()
# create an instance of OneTimeTokenExchangeRequest from a dict
one_time_token_exchange_request_form_dict = one_time_token_exchange_request.from_dict(one_time_token_exchange_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


