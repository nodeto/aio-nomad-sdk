# ReschedulePolicy


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**attempts** | **int** |  | [optional] 
**delay** | **int** |  | [optional] 
**delay_function** | **str** |  | [optional] 
**interval** | **int** |  | [optional] 
**max_delay** | **int** |  | [optional] 
**unlimited** | **bool** |  | [optional] 

## Example

```python
from nomad_generated.models.reschedule_policy import ReschedulePolicy

# TODO update the JSON string below
json = "{}"
# create an instance of ReschedulePolicy from a JSON string
reschedule_policy_instance = ReschedulePolicy.from_json(json)
# print the JSON string representation of the object
print ReschedulePolicy.to_json()

# convert the object into a dict
reschedule_policy_dict = reschedule_policy_instance.to_dict()
# create an instance of ReschedulePolicy from a dict
reschedule_policy_form_dict = reschedule_policy.from_dict(reschedule_policy_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


