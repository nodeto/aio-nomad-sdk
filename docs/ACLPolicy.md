# ACLPolicy


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**create_index** | **int** |  | [optional] 
**description** | **str** |  | [optional] 
**job_acl** | [**JobACL**](JobACL.md) |  | [optional] 
**modify_index** | **int** |  | [optional] 
**name** | **str** |  | [optional] 
**rules** | **str** |  | [optional] 

## Example

```python
from nomad_generated.models.acl_policy import ACLPolicy

# TODO update the JSON string below
json = "{}"
# create an instance of ACLPolicy from a JSON string
acl_policy_instance = ACLPolicy.from_json(json)
# print the JSON string representation of the object
print ACLPolicy.to_json()

# convert the object into a dict
acl_policy_dict = acl_policy_instance.to_dict()
# create an instance of ACLPolicy from a dict
acl_policy_form_dict = acl_policy.from_dict(acl_policy_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


