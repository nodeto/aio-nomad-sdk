# CSIVolume


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**access_mode** | **str** |  | [optional] 
**allocations** | [**List[AllocationListStub]**](AllocationListStub.md) |  | [optional] 
**attachment_mode** | **str** |  | [optional] 
**capacity** | **int** |  | [optional] 
**clone_id** | **str** |  | [optional] 
**context** | **Dict[str, str]** |  | [optional] 
**controller_required** | **bool** |  | [optional] 
**controllers_expected** | **int** |  | [optional] 
**controllers_healthy** | **int** |  | [optional] 
**create_index** | **int** |  | [optional] 
**external_id** | **str** |  | [optional] 
**id** | **str** |  | [optional] 
**modify_index** | **int** |  | [optional] 
**mount_options** | [**CSIMountOptions**](CSIMountOptions.md) |  | [optional] 
**name** | **str** |  | [optional] 
**namespace** | **str** |  | [optional] 
**nodes_expected** | **int** |  | [optional] 
**nodes_healthy** | **int** |  | [optional] 
**parameters** | **Dict[str, str]** |  | [optional] 
**plugin_id** | **str** |  | [optional] 
**provider** | **str** |  | [optional] 
**provider_version** | **str** |  | [optional] 
**read_allocs** | [**Dict[str, Allocation]**](Allocation.md) |  | [optional] 
**requested_capabilities** | [**List[CSIVolumeCapability]**](CSIVolumeCapability.md) |  | [optional] 
**requested_capacity_max** | **int** |  | [optional] 
**requested_capacity_min** | **int** |  | [optional] 
**requested_topologies** | [**CSITopologyRequest**](CSITopologyRequest.md) |  | [optional] 
**resource_exhausted** | **datetime** |  | [optional] 
**schedulable** | **bool** |  | [optional] 
**secrets** | **Dict[str, str]** |  | [optional] 
**snapshot_id** | **str** |  | [optional] 
**topologies** | [**List[CSITopology]**](CSITopology.md) |  | [optional] 
**write_allocs** | [**Dict[str, Allocation]**](Allocation.md) |  | [optional] 

## Example

```python
from nomad_generated.models.csi_volume import CSIVolume

# TODO update the JSON string below
json = "{}"
# create an instance of CSIVolume from a JSON string
csi_volume_instance = CSIVolume.from_json(json)
# print the JSON string representation of the object
print CSIVolume.to_json()

# convert the object into a dict
csi_volume_dict = csi_volume_instance.to_dict()
# create an instance of CSIVolume from a dict
csi_volume_form_dict = csi_volume.from_dict(csi_volume_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


