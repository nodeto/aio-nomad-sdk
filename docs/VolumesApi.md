# nomad_generated.VolumesApi

All URIs are relative to *http://127.0.0.1:4646/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**create_volume**](VolumesApi.md#create_volume) | **POST** /volume/csi/{volumeId}/{action} | 
[**delete_snapshot**](VolumesApi.md#delete_snapshot) | **DELETE** /volumes/snapshot | 
[**delete_volume_registration**](VolumesApi.md#delete_volume_registration) | **DELETE** /volume/csi/{volumeId} | 
[**detach_or_delete_volume**](VolumesApi.md#detach_or_delete_volume) | **DELETE** /volume/csi/{volumeId}/{action} | 
[**get_external_volumes**](VolumesApi.md#get_external_volumes) | **GET** /volumes/external | 
[**get_snapshots**](VolumesApi.md#get_snapshots) | **GET** /volumes/snapshot | 
[**get_volume**](VolumesApi.md#get_volume) | **GET** /volume/csi/{volumeId} | 
[**get_volumes**](VolumesApi.md#get_volumes) | **GET** /volumes | 
[**post_snapshot**](VolumesApi.md#post_snapshot) | **POST** /volumes/snapshot | 
[**post_volume**](VolumesApi.md#post_volume) | **POST** /volumes | 
[**post_volume_registration**](VolumesApi.md#post_volume_registration) | **POST** /volume/csi/{volumeId} | 


# **create_volume**
> create_volume(volume_id, action, csi_volume_create_request, region=region, namespace=namespace, x_nomad_token=x_nomad_token, idempotency_token=idempotency_token)



### Example

* Api Key Authentication (X-Nomad-Token):
```python
import time
import os
import nomad_generated
from nomad_generated.models.csi_volume_create_request import CSIVolumeCreateRequest
from nomad_generated.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://127.0.0.1:4646/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = nomad_generated.Configuration(
    host = "http://127.0.0.1:4646/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: X-Nomad-Token
configuration.api_key['X-Nomad-Token'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['X-Nomad-Token'] = 'Bearer'

# Enter a context with an instance of the API client
async with nomad_generated.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = nomad_generated.VolumesApi(api_client)
    volume_id = 'volume_id_example' # str | Volume unique identifier.
    action = 'action_example' # str | The action to perform on the Volume (create, detach, delete).
    csi_volume_create_request = nomad_generated.CSIVolumeCreateRequest() # CSIVolumeCreateRequest | 
    region = 'region_example' # str | Filters results based on the specified region. (optional)
    namespace = 'namespace_example' # str | Filters results based on the specified namespace. (optional)
    x_nomad_token = 'x_nomad_token_example' # str | A Nomad ACL token. (optional)
    idempotency_token = 'idempotency_token_example' # str | Can be used to ensure operations are only run once. (optional)

    try:
        await api_instance.create_volume(volume_id, action, csi_volume_create_request, region=region, namespace=namespace, x_nomad_token=x_nomad_token, idempotency_token=idempotency_token)
    except Exception as e:
        print("Exception when calling VolumesApi->create_volume: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **volume_id** | **str**| Volume unique identifier. | 
 **action** | **str**| The action to perform on the Volume (create, detach, delete). | 
 **csi_volume_create_request** | [**CSIVolumeCreateRequest**](CSIVolumeCreateRequest.md)|  | 
 **region** | **str**| Filters results based on the specified region. | [optional] 
 **namespace** | **str**| Filters results based on the specified namespace. | [optional] 
 **x_nomad_token** | **str**| A Nomad ACL token. | [optional] 
 **idempotency_token** | **str**| Can be used to ensure operations are only run once. | [optional] 

### Return type

void (empty response body)

### Authorization

[X-Nomad-Token](../README.md#X-Nomad-Token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  * X-Nomad-Index - A unique identifier representing the current state of the requested resource. On a new Nomad cluster the value of this index starts at 1. <br>  * X-Nomad-KnownLeader - Boolean indicating if there is a known cluster leader. <br>  * X-Nomad-LastContact - The time in milliseconds that a server was last contacted by the leader node. <br>  |
**400** | Bad request |  -  |
**403** | Forbidden |  -  |
**405** | Method not allowed |  -  |
**500** | Internal server error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_snapshot**
> delete_snapshot(region=region, namespace=namespace, x_nomad_token=x_nomad_token, idempotency_token=idempotency_token, plugin_id=plugin_id, snapshot_id=snapshot_id)



### Example

* Api Key Authentication (X-Nomad-Token):
```python
import time
import os
import nomad_generated
from nomad_generated.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://127.0.0.1:4646/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = nomad_generated.Configuration(
    host = "http://127.0.0.1:4646/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: X-Nomad-Token
configuration.api_key['X-Nomad-Token'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['X-Nomad-Token'] = 'Bearer'

# Enter a context with an instance of the API client
async with nomad_generated.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = nomad_generated.VolumesApi(api_client)
    region = 'region_example' # str | Filters results based on the specified region. (optional)
    namespace = 'namespace_example' # str | Filters results based on the specified namespace. (optional)
    x_nomad_token = 'x_nomad_token_example' # str | A Nomad ACL token. (optional)
    idempotency_token = 'idempotency_token_example' # str | Can be used to ensure operations are only run once. (optional)
    plugin_id = 'plugin_id_example' # str | Filters volume lists by plugin ID. (optional)
    snapshot_id = 'snapshot_id_example' # str | The ID of the snapshot to target. (optional)

    try:
        await api_instance.delete_snapshot(region=region, namespace=namespace, x_nomad_token=x_nomad_token, idempotency_token=idempotency_token, plugin_id=plugin_id, snapshot_id=snapshot_id)
    except Exception as e:
        print("Exception when calling VolumesApi->delete_snapshot: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **region** | **str**| Filters results based on the specified region. | [optional] 
 **namespace** | **str**| Filters results based on the specified namespace. | [optional] 
 **x_nomad_token** | **str**| A Nomad ACL token. | [optional] 
 **idempotency_token** | **str**| Can be used to ensure operations are only run once. | [optional] 
 **plugin_id** | **str**| Filters volume lists by plugin ID. | [optional] 
 **snapshot_id** | **str**| The ID of the snapshot to target. | [optional] 

### Return type

void (empty response body)

### Authorization

[X-Nomad-Token](../README.md#X-Nomad-Token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  * X-Nomad-Index - A unique identifier representing the current state of the requested resource. On a new Nomad cluster the value of this index starts at 1. <br>  |
**400** | Bad request |  -  |
**403** | Forbidden |  -  |
**405** | Method not allowed |  -  |
**500** | Internal server error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_volume_registration**
> delete_volume_registration(volume_id, region=region, namespace=namespace, x_nomad_token=x_nomad_token, idempotency_token=idempotency_token, force=force)



### Example

* Api Key Authentication (X-Nomad-Token):
```python
import time
import os
import nomad_generated
from nomad_generated.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://127.0.0.1:4646/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = nomad_generated.Configuration(
    host = "http://127.0.0.1:4646/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: X-Nomad-Token
configuration.api_key['X-Nomad-Token'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['X-Nomad-Token'] = 'Bearer'

# Enter a context with an instance of the API client
async with nomad_generated.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = nomad_generated.VolumesApi(api_client)
    volume_id = 'volume_id_example' # str | Volume unique identifier.
    region = 'region_example' # str | Filters results based on the specified region. (optional)
    namespace = 'namespace_example' # str | Filters results based on the specified namespace. (optional)
    x_nomad_token = 'x_nomad_token_example' # str | A Nomad ACL token. (optional)
    idempotency_token = 'idempotency_token_example' # str | Can be used to ensure operations are only run once. (optional)
    force = 'force_example' # str | Used to force the de-registration of a volume. (optional)

    try:
        await api_instance.delete_volume_registration(volume_id, region=region, namespace=namespace, x_nomad_token=x_nomad_token, idempotency_token=idempotency_token, force=force)
    except Exception as e:
        print("Exception when calling VolumesApi->delete_volume_registration: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **volume_id** | **str**| Volume unique identifier. | 
 **region** | **str**| Filters results based on the specified region. | [optional] 
 **namespace** | **str**| Filters results based on the specified namespace. | [optional] 
 **x_nomad_token** | **str**| A Nomad ACL token. | [optional] 
 **idempotency_token** | **str**| Can be used to ensure operations are only run once. | [optional] 
 **force** | **str**| Used to force the de-registration of a volume. | [optional] 

### Return type

void (empty response body)

### Authorization

[X-Nomad-Token](../README.md#X-Nomad-Token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  * X-Nomad-Index - A unique identifier representing the current state of the requested resource. On a new Nomad cluster the value of this index starts at 1. <br>  |
**400** | Bad request |  -  |
**403** | Forbidden |  -  |
**405** | Method not allowed |  -  |
**500** | Internal server error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **detach_or_delete_volume**
> detach_or_delete_volume(volume_id, action, region=region, namespace=namespace, x_nomad_token=x_nomad_token, idempotency_token=idempotency_token, node=node)



### Example

* Api Key Authentication (X-Nomad-Token):
```python
import time
import os
import nomad_generated
from nomad_generated.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://127.0.0.1:4646/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = nomad_generated.Configuration(
    host = "http://127.0.0.1:4646/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: X-Nomad-Token
configuration.api_key['X-Nomad-Token'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['X-Nomad-Token'] = 'Bearer'

# Enter a context with an instance of the API client
async with nomad_generated.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = nomad_generated.VolumesApi(api_client)
    volume_id = 'volume_id_example' # str | Volume unique identifier.
    action = 'action_example' # str | The action to perform on the Volume (create, detach, delete).
    region = 'region_example' # str | Filters results based on the specified region. (optional)
    namespace = 'namespace_example' # str | Filters results based on the specified namespace. (optional)
    x_nomad_token = 'x_nomad_token_example' # str | A Nomad ACL token. (optional)
    idempotency_token = 'idempotency_token_example' # str | Can be used to ensure operations are only run once. (optional)
    node = 'node_example' # str | Specifies node to target volume operation for. (optional)

    try:
        await api_instance.detach_or_delete_volume(volume_id, action, region=region, namespace=namespace, x_nomad_token=x_nomad_token, idempotency_token=idempotency_token, node=node)
    except Exception as e:
        print("Exception when calling VolumesApi->detach_or_delete_volume: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **volume_id** | **str**| Volume unique identifier. | 
 **action** | **str**| The action to perform on the Volume (create, detach, delete). | 
 **region** | **str**| Filters results based on the specified region. | [optional] 
 **namespace** | **str**| Filters results based on the specified namespace. | [optional] 
 **x_nomad_token** | **str**| A Nomad ACL token. | [optional] 
 **idempotency_token** | **str**| Can be used to ensure operations are only run once. | [optional] 
 **node** | **str**| Specifies node to target volume operation for. | [optional] 

### Return type

void (empty response body)

### Authorization

[X-Nomad-Token](../README.md#X-Nomad-Token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  * X-Nomad-Index - A unique identifier representing the current state of the requested resource. On a new Nomad cluster the value of this index starts at 1. <br>  * X-Nomad-KnownLeader - Boolean indicating if there is a known cluster leader. <br>  * X-Nomad-LastContact - The time in milliseconds that a server was last contacted by the leader node. <br>  |
**400** | Bad request |  -  |
**403** | Forbidden |  -  |
**405** | Method not allowed |  -  |
**500** | Internal server error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_external_volumes**
> CSIVolumeListExternalResponse get_external_volumes(region=region, namespace=namespace, index=index, wait=wait, stale=stale, prefix=prefix, x_nomad_token=x_nomad_token, per_page=per_page, next_token=next_token, plugin_id=plugin_id)



### Example

* Api Key Authentication (X-Nomad-Token):
```python
import time
import os
import nomad_generated
from nomad_generated.models.csi_volume_list_external_response import CSIVolumeListExternalResponse
from nomad_generated.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://127.0.0.1:4646/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = nomad_generated.Configuration(
    host = "http://127.0.0.1:4646/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: X-Nomad-Token
configuration.api_key['X-Nomad-Token'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['X-Nomad-Token'] = 'Bearer'

# Enter a context with an instance of the API client
async with nomad_generated.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = nomad_generated.VolumesApi(api_client)
    region = 'region_example' # str | Filters results based on the specified region. (optional)
    namespace = 'namespace_example' # str | Filters results based on the specified namespace. (optional)
    index = 56 # int | If set, wait until query exceeds given index. Must be provided with WaitParam. (optional)
    wait = 'wait_example' # str | Provided with IndexParam to wait for change. (optional)
    stale = 'stale_example' # str | If present, results will include stale reads. (optional)
    prefix = 'prefix_example' # str | Constrains results to jobs that start with the defined prefix (optional)
    x_nomad_token = 'x_nomad_token_example' # str | A Nomad ACL token. (optional)
    per_page = 56 # int | Maximum number of results to return. (optional)
    next_token = 'next_token_example' # str | Indicates where to start paging for queries that support pagination. (optional)
    plugin_id = 'plugin_id_example' # str | Filters volume lists by plugin ID. (optional)

    try:
        api_response = await api_instance.get_external_volumes(region=region, namespace=namespace, index=index, wait=wait, stale=stale, prefix=prefix, x_nomad_token=x_nomad_token, per_page=per_page, next_token=next_token, plugin_id=plugin_id)
        print("The response of VolumesApi->get_external_volumes:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling VolumesApi->get_external_volumes: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **region** | **str**| Filters results based on the specified region. | [optional] 
 **namespace** | **str**| Filters results based on the specified namespace. | [optional] 
 **index** | **int**| If set, wait until query exceeds given index. Must be provided with WaitParam. | [optional] 
 **wait** | **str**| Provided with IndexParam to wait for change. | [optional] 
 **stale** | **str**| If present, results will include stale reads. | [optional] 
 **prefix** | **str**| Constrains results to jobs that start with the defined prefix | [optional] 
 **x_nomad_token** | **str**| A Nomad ACL token. | [optional] 
 **per_page** | **int**| Maximum number of results to return. | [optional] 
 **next_token** | **str**| Indicates where to start paging for queries that support pagination. | [optional] 
 **plugin_id** | **str**| Filters volume lists by plugin ID. | [optional] 

### Return type

[**CSIVolumeListExternalResponse**](CSIVolumeListExternalResponse.md)

### Authorization

[X-Nomad-Token](../README.md#X-Nomad-Token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  * X-Nomad-Index - A unique identifier representing the current state of the requested resource. On a new Nomad cluster the value of this index starts at 1. <br>  * X-Nomad-KnownLeader - Boolean indicating if there is a known cluster leader. <br>  * X-Nomad-LastContact - The time in milliseconds that a server was last contacted by the leader node. <br>  |
**400** | Bad request |  -  |
**403** | Forbidden |  -  |
**405** | Method not allowed |  -  |
**500** | Internal server error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_snapshots**
> CSISnapshotListResponse get_snapshots(region=region, namespace=namespace, index=index, wait=wait, stale=stale, prefix=prefix, x_nomad_token=x_nomad_token, per_page=per_page, next_token=next_token, plugin_id=plugin_id)



### Example

* Api Key Authentication (X-Nomad-Token):
```python
import time
import os
import nomad_generated
from nomad_generated.models.csi_snapshot_list_response import CSISnapshotListResponse
from nomad_generated.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://127.0.0.1:4646/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = nomad_generated.Configuration(
    host = "http://127.0.0.1:4646/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: X-Nomad-Token
configuration.api_key['X-Nomad-Token'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['X-Nomad-Token'] = 'Bearer'

# Enter a context with an instance of the API client
async with nomad_generated.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = nomad_generated.VolumesApi(api_client)
    region = 'region_example' # str | Filters results based on the specified region. (optional)
    namespace = 'namespace_example' # str | Filters results based on the specified namespace. (optional)
    index = 56 # int | If set, wait until query exceeds given index. Must be provided with WaitParam. (optional)
    wait = 'wait_example' # str | Provided with IndexParam to wait for change. (optional)
    stale = 'stale_example' # str | If present, results will include stale reads. (optional)
    prefix = 'prefix_example' # str | Constrains results to jobs that start with the defined prefix (optional)
    x_nomad_token = 'x_nomad_token_example' # str | A Nomad ACL token. (optional)
    per_page = 56 # int | Maximum number of results to return. (optional)
    next_token = 'next_token_example' # str | Indicates where to start paging for queries that support pagination. (optional)
    plugin_id = 'plugin_id_example' # str | Filters volume lists by plugin ID. (optional)

    try:
        api_response = await api_instance.get_snapshots(region=region, namespace=namespace, index=index, wait=wait, stale=stale, prefix=prefix, x_nomad_token=x_nomad_token, per_page=per_page, next_token=next_token, plugin_id=plugin_id)
        print("The response of VolumesApi->get_snapshots:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling VolumesApi->get_snapshots: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **region** | **str**| Filters results based on the specified region. | [optional] 
 **namespace** | **str**| Filters results based on the specified namespace. | [optional] 
 **index** | **int**| If set, wait until query exceeds given index. Must be provided with WaitParam. | [optional] 
 **wait** | **str**| Provided with IndexParam to wait for change. | [optional] 
 **stale** | **str**| If present, results will include stale reads. | [optional] 
 **prefix** | **str**| Constrains results to jobs that start with the defined prefix | [optional] 
 **x_nomad_token** | **str**| A Nomad ACL token. | [optional] 
 **per_page** | **int**| Maximum number of results to return. | [optional] 
 **next_token** | **str**| Indicates where to start paging for queries that support pagination. | [optional] 
 **plugin_id** | **str**| Filters volume lists by plugin ID. | [optional] 

### Return type

[**CSISnapshotListResponse**](CSISnapshotListResponse.md)

### Authorization

[X-Nomad-Token](../README.md#X-Nomad-Token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  * X-Nomad-Index - A unique identifier representing the current state of the requested resource. On a new Nomad cluster the value of this index starts at 1. <br>  * X-Nomad-KnownLeader - Boolean indicating if there is a known cluster leader. <br>  * X-Nomad-LastContact - The time in milliseconds that a server was last contacted by the leader node. <br>  |
**400** | Bad request |  -  |
**403** | Forbidden |  -  |
**405** | Method not allowed |  -  |
**500** | Internal server error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_volume**
> CSIVolume get_volume(volume_id, region=region, namespace=namespace, index=index, wait=wait, stale=stale, prefix=prefix, x_nomad_token=x_nomad_token, per_page=per_page, next_token=next_token)



### Example

* Api Key Authentication (X-Nomad-Token):
```python
import time
import os
import nomad_generated
from nomad_generated.models.csi_volume import CSIVolume
from nomad_generated.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://127.0.0.1:4646/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = nomad_generated.Configuration(
    host = "http://127.0.0.1:4646/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: X-Nomad-Token
configuration.api_key['X-Nomad-Token'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['X-Nomad-Token'] = 'Bearer'

# Enter a context with an instance of the API client
async with nomad_generated.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = nomad_generated.VolumesApi(api_client)
    volume_id = 'volume_id_example' # str | Volume unique identifier.
    region = 'region_example' # str | Filters results based on the specified region. (optional)
    namespace = 'namespace_example' # str | Filters results based on the specified namespace. (optional)
    index = 56 # int | If set, wait until query exceeds given index. Must be provided with WaitParam. (optional)
    wait = 'wait_example' # str | Provided with IndexParam to wait for change. (optional)
    stale = 'stale_example' # str | If present, results will include stale reads. (optional)
    prefix = 'prefix_example' # str | Constrains results to jobs that start with the defined prefix (optional)
    x_nomad_token = 'x_nomad_token_example' # str | A Nomad ACL token. (optional)
    per_page = 56 # int | Maximum number of results to return. (optional)
    next_token = 'next_token_example' # str | Indicates where to start paging for queries that support pagination. (optional)

    try:
        api_response = await api_instance.get_volume(volume_id, region=region, namespace=namespace, index=index, wait=wait, stale=stale, prefix=prefix, x_nomad_token=x_nomad_token, per_page=per_page, next_token=next_token)
        print("The response of VolumesApi->get_volume:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling VolumesApi->get_volume: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **volume_id** | **str**| Volume unique identifier. | 
 **region** | **str**| Filters results based on the specified region. | [optional] 
 **namespace** | **str**| Filters results based on the specified namespace. | [optional] 
 **index** | **int**| If set, wait until query exceeds given index. Must be provided with WaitParam. | [optional] 
 **wait** | **str**| Provided with IndexParam to wait for change. | [optional] 
 **stale** | **str**| If present, results will include stale reads. | [optional] 
 **prefix** | **str**| Constrains results to jobs that start with the defined prefix | [optional] 
 **x_nomad_token** | **str**| A Nomad ACL token. | [optional] 
 **per_page** | **int**| Maximum number of results to return. | [optional] 
 **next_token** | **str**| Indicates where to start paging for queries that support pagination. | [optional] 

### Return type

[**CSIVolume**](CSIVolume.md)

### Authorization

[X-Nomad-Token](../README.md#X-Nomad-Token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  * X-Nomad-Index - A unique identifier representing the current state of the requested resource. On a new Nomad cluster the value of this index starts at 1. <br>  * X-Nomad-KnownLeader - Boolean indicating if there is a known cluster leader. <br>  * X-Nomad-LastContact - The time in milliseconds that a server was last contacted by the leader node. <br>  |
**400** | Bad request |  -  |
**403** | Forbidden |  -  |
**405** | Method not allowed |  -  |
**500** | Internal server error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_volumes**
> List[CSIVolumeListStub] get_volumes(region=region, namespace=namespace, index=index, wait=wait, stale=stale, prefix=prefix, x_nomad_token=x_nomad_token, per_page=per_page, next_token=next_token, node_id=node_id, plugin_id=plugin_id, type=type)



### Example

* Api Key Authentication (X-Nomad-Token):
```python
import time
import os
import nomad_generated
from nomad_generated.models.csi_volume_list_stub import CSIVolumeListStub
from nomad_generated.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://127.0.0.1:4646/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = nomad_generated.Configuration(
    host = "http://127.0.0.1:4646/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: X-Nomad-Token
configuration.api_key['X-Nomad-Token'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['X-Nomad-Token'] = 'Bearer'

# Enter a context with an instance of the API client
async with nomad_generated.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = nomad_generated.VolumesApi(api_client)
    region = 'region_example' # str | Filters results based on the specified region. (optional)
    namespace = 'namespace_example' # str | Filters results based on the specified namespace. (optional)
    index = 56 # int | If set, wait until query exceeds given index. Must be provided with WaitParam. (optional)
    wait = 'wait_example' # str | Provided with IndexParam to wait for change. (optional)
    stale = 'stale_example' # str | If present, results will include stale reads. (optional)
    prefix = 'prefix_example' # str | Constrains results to jobs that start with the defined prefix (optional)
    x_nomad_token = 'x_nomad_token_example' # str | A Nomad ACL token. (optional)
    per_page = 56 # int | Maximum number of results to return. (optional)
    next_token = 'next_token_example' # str | Indicates where to start paging for queries that support pagination. (optional)
    node_id = 'node_id_example' # str | Filters volume lists by node ID. (optional)
    plugin_id = 'plugin_id_example' # str | Filters volume lists by plugin ID. (optional)
    type = 'type_example' # str | Filters volume lists to a specific type. (optional)

    try:
        api_response = await api_instance.get_volumes(region=region, namespace=namespace, index=index, wait=wait, stale=stale, prefix=prefix, x_nomad_token=x_nomad_token, per_page=per_page, next_token=next_token, node_id=node_id, plugin_id=plugin_id, type=type)
        print("The response of VolumesApi->get_volumes:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling VolumesApi->get_volumes: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **region** | **str**| Filters results based on the specified region. | [optional] 
 **namespace** | **str**| Filters results based on the specified namespace. | [optional] 
 **index** | **int**| If set, wait until query exceeds given index. Must be provided with WaitParam. | [optional] 
 **wait** | **str**| Provided with IndexParam to wait for change. | [optional] 
 **stale** | **str**| If present, results will include stale reads. | [optional] 
 **prefix** | **str**| Constrains results to jobs that start with the defined prefix | [optional] 
 **x_nomad_token** | **str**| A Nomad ACL token. | [optional] 
 **per_page** | **int**| Maximum number of results to return. | [optional] 
 **next_token** | **str**| Indicates where to start paging for queries that support pagination. | [optional] 
 **node_id** | **str**| Filters volume lists by node ID. | [optional] 
 **plugin_id** | **str**| Filters volume lists by plugin ID. | [optional] 
 **type** | **str**| Filters volume lists to a specific type. | [optional] 

### Return type

[**List[CSIVolumeListStub]**](CSIVolumeListStub.md)

### Authorization

[X-Nomad-Token](../README.md#X-Nomad-Token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  * X-Nomad-Index - A unique identifier representing the current state of the requested resource. On a new Nomad cluster the value of this index starts at 1. <br>  * X-Nomad-KnownLeader - Boolean indicating if there is a known cluster leader. <br>  * X-Nomad-LastContact - The time in milliseconds that a server was last contacted by the leader node. <br>  |
**400** | Bad request |  -  |
**403** | Forbidden |  -  |
**405** | Method not allowed |  -  |
**500** | Internal server error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_snapshot**
> CSISnapshotCreateResponse post_snapshot(csi_snapshot_create_request, region=region, namespace=namespace, x_nomad_token=x_nomad_token, idempotency_token=idempotency_token)



### Example

* Api Key Authentication (X-Nomad-Token):
```python
import time
import os
import nomad_generated
from nomad_generated.models.csi_snapshot_create_request import CSISnapshotCreateRequest
from nomad_generated.models.csi_snapshot_create_response import CSISnapshotCreateResponse
from nomad_generated.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://127.0.0.1:4646/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = nomad_generated.Configuration(
    host = "http://127.0.0.1:4646/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: X-Nomad-Token
configuration.api_key['X-Nomad-Token'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['X-Nomad-Token'] = 'Bearer'

# Enter a context with an instance of the API client
async with nomad_generated.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = nomad_generated.VolumesApi(api_client)
    csi_snapshot_create_request = nomad_generated.CSISnapshotCreateRequest() # CSISnapshotCreateRequest | 
    region = 'region_example' # str | Filters results based on the specified region. (optional)
    namespace = 'namespace_example' # str | Filters results based on the specified namespace. (optional)
    x_nomad_token = 'x_nomad_token_example' # str | A Nomad ACL token. (optional)
    idempotency_token = 'idempotency_token_example' # str | Can be used to ensure operations are only run once. (optional)

    try:
        api_response = await api_instance.post_snapshot(csi_snapshot_create_request, region=region, namespace=namespace, x_nomad_token=x_nomad_token, idempotency_token=idempotency_token)
        print("The response of VolumesApi->post_snapshot:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling VolumesApi->post_snapshot: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **csi_snapshot_create_request** | [**CSISnapshotCreateRequest**](CSISnapshotCreateRequest.md)|  | 
 **region** | **str**| Filters results based on the specified region. | [optional] 
 **namespace** | **str**| Filters results based on the specified namespace. | [optional] 
 **x_nomad_token** | **str**| A Nomad ACL token. | [optional] 
 **idempotency_token** | **str**| Can be used to ensure operations are only run once. | [optional] 

### Return type

[**CSISnapshotCreateResponse**](CSISnapshotCreateResponse.md)

### Authorization

[X-Nomad-Token](../README.md#X-Nomad-Token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  * X-Nomad-Index - A unique identifier representing the current state of the requested resource. On a new Nomad cluster the value of this index starts at 1. <br>  |
**400** | Bad request |  -  |
**403** | Forbidden |  -  |
**405** | Method not allowed |  -  |
**500** | Internal server error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_volume**
> post_volume(csi_volume_register_request, region=region, namespace=namespace, x_nomad_token=x_nomad_token, idempotency_token=idempotency_token)



### Example

* Api Key Authentication (X-Nomad-Token):
```python
import time
import os
import nomad_generated
from nomad_generated.models.csi_volume_register_request import CSIVolumeRegisterRequest
from nomad_generated.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://127.0.0.1:4646/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = nomad_generated.Configuration(
    host = "http://127.0.0.1:4646/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: X-Nomad-Token
configuration.api_key['X-Nomad-Token'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['X-Nomad-Token'] = 'Bearer'

# Enter a context with an instance of the API client
async with nomad_generated.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = nomad_generated.VolumesApi(api_client)
    csi_volume_register_request = nomad_generated.CSIVolumeRegisterRequest() # CSIVolumeRegisterRequest | 
    region = 'region_example' # str | Filters results based on the specified region. (optional)
    namespace = 'namespace_example' # str | Filters results based on the specified namespace. (optional)
    x_nomad_token = 'x_nomad_token_example' # str | A Nomad ACL token. (optional)
    idempotency_token = 'idempotency_token_example' # str | Can be used to ensure operations are only run once. (optional)

    try:
        await api_instance.post_volume(csi_volume_register_request, region=region, namespace=namespace, x_nomad_token=x_nomad_token, idempotency_token=idempotency_token)
    except Exception as e:
        print("Exception when calling VolumesApi->post_volume: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **csi_volume_register_request** | [**CSIVolumeRegisterRequest**](CSIVolumeRegisterRequest.md)|  | 
 **region** | **str**| Filters results based on the specified region. | [optional] 
 **namespace** | **str**| Filters results based on the specified namespace. | [optional] 
 **x_nomad_token** | **str**| A Nomad ACL token. | [optional] 
 **idempotency_token** | **str**| Can be used to ensure operations are only run once. | [optional] 

### Return type

void (empty response body)

### Authorization

[X-Nomad-Token](../README.md#X-Nomad-Token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  * X-Nomad-Index - A unique identifier representing the current state of the requested resource. On a new Nomad cluster the value of this index starts at 1. <br>  * X-Nomad-KnownLeader - Boolean indicating if there is a known cluster leader. <br>  * X-Nomad-LastContact - The time in milliseconds that a server was last contacted by the leader node. <br>  |
**400** | Bad request |  -  |
**403** | Forbidden |  -  |
**405** | Method not allowed |  -  |
**500** | Internal server error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_volume_registration**
> post_volume_registration(volume_id, csi_volume_register_request, region=region, namespace=namespace, x_nomad_token=x_nomad_token, idempotency_token=idempotency_token)



### Example

* Api Key Authentication (X-Nomad-Token):
```python
import time
import os
import nomad_generated
from nomad_generated.models.csi_volume_register_request import CSIVolumeRegisterRequest
from nomad_generated.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://127.0.0.1:4646/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = nomad_generated.Configuration(
    host = "http://127.0.0.1:4646/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: X-Nomad-Token
configuration.api_key['X-Nomad-Token'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['X-Nomad-Token'] = 'Bearer'

# Enter a context with an instance of the API client
async with nomad_generated.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = nomad_generated.VolumesApi(api_client)
    volume_id = 'volume_id_example' # str | Volume unique identifier.
    csi_volume_register_request = nomad_generated.CSIVolumeRegisterRequest() # CSIVolumeRegisterRequest | 
    region = 'region_example' # str | Filters results based on the specified region. (optional)
    namespace = 'namespace_example' # str | Filters results based on the specified namespace. (optional)
    x_nomad_token = 'x_nomad_token_example' # str | A Nomad ACL token. (optional)
    idempotency_token = 'idempotency_token_example' # str | Can be used to ensure operations are only run once. (optional)

    try:
        await api_instance.post_volume_registration(volume_id, csi_volume_register_request, region=region, namespace=namespace, x_nomad_token=x_nomad_token, idempotency_token=idempotency_token)
    except Exception as e:
        print("Exception when calling VolumesApi->post_volume_registration: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **volume_id** | **str**| Volume unique identifier. | 
 **csi_volume_register_request** | [**CSIVolumeRegisterRequest**](CSIVolumeRegisterRequest.md)|  | 
 **region** | **str**| Filters results based on the specified region. | [optional] 
 **namespace** | **str**| Filters results based on the specified namespace. | [optional] 
 **x_nomad_token** | **str**| A Nomad ACL token. | [optional] 
 **idempotency_token** | **str**| Can be used to ensure operations are only run once. | [optional] 

### Return type

void (empty response body)

### Authorization

[X-Nomad-Token](../README.md#X-Nomad-Token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  * X-Nomad-Index - A unique identifier representing the current state of the requested resource. On a new Nomad cluster the value of this index starts at 1. <br>  |
**400** | Bad request |  -  |
**403** | Forbidden |  -  |
**405** | Method not allowed |  -  |
**500** | Internal server error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

