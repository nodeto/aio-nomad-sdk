# AllocatedResources


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**shared** | [**AllocatedSharedResources**](AllocatedSharedResources.md) |  | [optional] 
**tasks** | [**Dict[str, AllocatedTaskResources]**](AllocatedTaskResources.md) |  | [optional] 

## Example

```python
from nomad_generated.models.allocated_resources import AllocatedResources

# TODO update the JSON string below
json = "{}"
# create an instance of AllocatedResources from a JSON string
allocated_resources_instance = AllocatedResources.from_json(json)
# print the JSON string representation of the object
print AllocatedResources.to_json()

# convert the object into a dict
allocated_resources_dict = allocated_resources_instance.to_dict()
# create an instance of AllocatedResources from a dict
allocated_resources_form_dict = allocated_resources.from_dict(allocated_resources_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


