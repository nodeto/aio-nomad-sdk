# DeploymentState


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**auto_revert** | **bool** |  | [optional] 
**desired_canaries** | **int** |  | [optional] 
**desired_total** | **int** |  | [optional] 
**healthy_allocs** | **int** |  | [optional] 
**placed_allocs** | **int** |  | [optional] 
**placed_canaries** | **List[str]** |  | [optional] 
**progress_deadline** | **int** |  | [optional] 
**promoted** | **bool** |  | [optional] 
**require_progress_by** | **datetime** |  | [optional] 
**unhealthy_allocs** | **int** |  | [optional] 

## Example

```python
from nomad_generated.models.deployment_state import DeploymentState

# TODO update the JSON string below
json = "{}"
# create an instance of DeploymentState from a JSON string
deployment_state_instance = DeploymentState.from_json(json)
# print the JSON string representation of the object
print DeploymentState.to_json()

# convert the object into a dict
deployment_state_dict = deployment_state_instance.to_dict()
# create an instance of DeploymentState from a dict
deployment_state_form_dict = deployment_state.from_dict(deployment_state_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


