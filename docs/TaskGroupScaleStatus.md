# TaskGroupScaleStatus


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**desired** | **int** |  | [optional] 
**events** | [**List[ScalingEvent]**](ScalingEvent.md) |  | [optional] 
**healthy** | **int** |  | [optional] 
**placed** | **int** |  | [optional] 
**running** | **int** |  | [optional] 
**unhealthy** | **int** |  | [optional] 

## Example

```python
from nomad_generated.models.task_group_scale_status import TaskGroupScaleStatus

# TODO update the JSON string below
json = "{}"
# create an instance of TaskGroupScaleStatus from a JSON string
task_group_scale_status_instance = TaskGroupScaleStatus.from_json(json)
# print the JSON string representation of the object
print TaskGroupScaleStatus.to_json()

# convert the object into a dict
task_group_scale_status_dict = task_group_scale_status_instance.to_dict()
# create an instance of TaskGroupScaleStatus from a dict
task_group_scale_status_form_dict = task_group_scale_status.from_dict(task_group_scale_status_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


