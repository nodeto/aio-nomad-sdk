# DispatchPayloadConfig


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**file** | **str** |  | [optional] 

## Example

```python
from nomad_generated.models.dispatch_payload_config import DispatchPayloadConfig

# TODO update the JSON string below
json = "{}"
# create an instance of DispatchPayloadConfig from a JSON string
dispatch_payload_config_instance = DispatchPayloadConfig.from_json(json)
# print the JSON string representation of the object
print DispatchPayloadConfig.to_json()

# convert the object into a dict
dispatch_payload_config_dict = dispatch_payload_config_instance.to_dict()
# create an instance of DispatchPayloadConfig from a dict
dispatch_payload_config_form_dict = dispatch_payload_config.from_dict(dispatch_payload_config_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


