# JobsParseRequest


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**canonicalize** | **bool** |  | [optional] 
**job_hcl** | **str** |  | [optional] 
**hclv1** | **bool** |  | [optional] 

## Example

```python
from nomad_generated.models.jobs_parse_request import JobsParseRequest

# TODO update the JSON string below
json = "{}"
# create an instance of JobsParseRequest from a JSON string
jobs_parse_request_instance = JobsParseRequest.from_json(json)
# print the JSON string representation of the object
print JobsParseRequest.to_json()

# convert the object into a dict
jobs_parse_request_dict = jobs_parse_request_instance.to_dict()
# create an instance of JobsParseRequest from a dict
jobs_parse_request_form_dict = jobs_parse_request.from_dict(jobs_parse_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


