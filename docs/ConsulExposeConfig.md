# ConsulExposeConfig


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**path** | [**List[ConsulExposePath]**](ConsulExposePath.md) |  | [optional] 

## Example

```python
from nomad_generated.models.consul_expose_config import ConsulExposeConfig

# TODO update the JSON string below
json = "{}"
# create an instance of ConsulExposeConfig from a JSON string
consul_expose_config_instance = ConsulExposeConfig.from_json(json)
# print the JSON string representation of the object
print ConsulExposeConfig.to_json()

# convert the object into a dict
consul_expose_config_dict = consul_expose_config_instance.to_dict()
# create an instance of ConsulExposeConfig from a dict
consul_expose_config_form_dict = consul_expose_config.from_dict(consul_expose_config_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


