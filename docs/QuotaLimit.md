# QuotaLimit


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**hash** | **bytearray** |  | [optional] 
**region** | **str** |  | [optional] 
**region_limit** | [**Resources**](Resources.md) |  | [optional] 
**variables_limit** | **int** |  | [optional] 

## Example

```python
from nomad_generated.models.quota_limit import QuotaLimit

# TODO update the JSON string below
json = "{}"
# create an instance of QuotaLimit from a JSON string
quota_limit_instance = QuotaLimit.from_json(json)
# print the JSON string representation of the object
print QuotaLimit.to_json()

# convert the object into a dict
quota_limit_dict = quota_limit_instance.to_dict()
# create an instance of QuotaLimit from a dict
quota_limit_form_dict = quota_limit.from_dict(quota_limit_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


