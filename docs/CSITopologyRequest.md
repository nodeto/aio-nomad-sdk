# CSITopologyRequest


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**preferred** | [**List[CSITopology]**](CSITopology.md) |  | [optional] 
**required** | [**List[CSITopology]**](CSITopology.md) |  | [optional] 

## Example

```python
from nomad_generated.models.csi_topology_request import CSITopologyRequest

# TODO update the JSON string below
json = "{}"
# create an instance of CSITopologyRequest from a JSON string
csi_topology_request_instance = CSITopologyRequest.from_json(json)
# print the JSON string representation of the object
print CSITopologyRequest.to_json()

# convert the object into a dict
csi_topology_request_dict = csi_topology_request_instance.to_dict()
# create an instance of CSITopologyRequest from a dict
csi_topology_request_form_dict = csi_topology_request.from_dict(csi_topology_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


