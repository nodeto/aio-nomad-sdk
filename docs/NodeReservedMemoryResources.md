# NodeReservedMemoryResources


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**memory_mb** | **int** |  | [optional] 

## Example

```python
from nomad_generated.models.node_reserved_memory_resources import NodeReservedMemoryResources

# TODO update the JSON string below
json = "{}"
# create an instance of NodeReservedMemoryResources from a JSON string
node_reserved_memory_resources_instance = NodeReservedMemoryResources.from_json(json)
# print the JSON string representation of the object
print NodeReservedMemoryResources.to_json()

# convert the object into a dict
node_reserved_memory_resources_dict = node_reserved_memory_resources_instance.to_dict()
# create an instance of NodeReservedMemoryResources from a dict
node_reserved_memory_resources_form_dict = node_reserved_memory_resources.from_dict(node_reserved_memory_resources_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


