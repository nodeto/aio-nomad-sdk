import asyncio
import aio_nomad

async def main():

   async with aio_nomad.NomadApi("http://torto:4646") as nomad:
        print(await nomad.jobs.get_jobs())

asyncio.run(main())
