from typing import TypedDict


class Configuration(TypedDict):
    nexus_url: str
    nexus_user: str
    nexus_password: str
