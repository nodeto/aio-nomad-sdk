import nomad_generated.api

from generate.functions import parse_and_generate_code

for item in parse_and_generate_code(nomad_generated.api.__file__):
    print(item)
