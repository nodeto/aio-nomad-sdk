import os

from generate.functions import generate_and_publish_openapi_client
from generate.models import Configuration


def main():
    config: Configuration = {
        "nexus_url": "https://nexus.nodeto.site/repository/nodeto-py/",
        "nexus_user": "admin",
        "nexus_password": os.environ["NEXUS_PASSWORD"],
    }
    generate_and_publish_openapi_client(config)


if __name__ == "__main__":
    main()
