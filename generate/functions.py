import ast
import os
import shutil
import subprocess
import tempfile
from pathlib import Path

from generate.models import Configuration


def parse_and_generate_code(init_file_path: str) -> str:
    """
    Parse an __init__.py file and generate import statements and __init__ function lines.

    Args:
        init_file_path: Path to the __init__.py file.

    Returns:
        A tuple containing two strings:
        - The import statements
        - The lines to be added to the __init__ function of the generated class.
    """
    with open(init_file_path, "r") as f:
        # Read the file contents
        file_contents = f.read()

    # Parse the Python code into an AST
    tree = ast.parse(file_contents)

    # Initialize result strings
    import_str = ""
    init_str = ""

    # Loop through the AST nodes to find imported API classes
    for node in ast.walk(tree):
        # Filter out ImportFrom nodes
        if isinstance(node, ast.ImportFrom):
            for name in node.names:
                # Create import statements
                import_str += f"from {node.module} import {name.name}\n"
                # Create the line for the __init__ function
                init_str += f"self.{name.name.lower()[:-3]} = {name.name}(api_client)\n"

    return import_str, init_str


def generate_and_publish_openapi_client(config: Configuration) -> None:
    """
    Function to generate a Python client using OpenAPI Generator and Docker.

    Runs OpenAPI Generator in a Docker container to generate a Python client
    based on a provided OpenAPI specification and config file.
    """
    # Create the output directory if it doesn't exist
    with tempfile.TemporaryDirectory() as output_dir:
        # Set up Docker command
        docker_cmd = [
            "docker",
            "run",
            "--user",
            f"{os.getuid()}:{os.getgid()}",
            "--rm",
            "-v",
            f"{Path.cwd()}:/local",
            "-v",
            f"{output_dir}:/output",
            "openapitools/openapi-generator-cli:v7.0.0",
            "generate",
            "-i",
            "/local/openapi.yaml",
            "-c",
            "/local/config.json",
            "-g",
            "python",
            "-o",
            "/output/",
        ]

        # Execute the Docker command
        subprocess.run(docker_cmd)

        shutil.rmtree("./docs")
        shutil.copytree(os.path.join(output_dir, "docs"), "./docs")
        # Navigate to the project folder
        os.chdir(output_dir)

        # Package the project
        subprocess.run(["python", "setup.py", "sdist", "bdist_wheel"])

        # Upload using twine
        subprocess.run(
            [
                "twine",
                "upload",
                "--repository-url",
                config["nexus_url"],
                "--username",
                config["nexus_user"],
                "--password",
                config["nexus_password"],
                "dist/*",
            ]
        )
